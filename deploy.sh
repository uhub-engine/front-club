# !/bin/bash 

echo '#UHUB EXPERIENCE'
aws s3 sync webapp/. s3://core.institution.v2.uhub.com/  --delete --exclude="*.git/*" --exclude="*.DS_Store*" --profile=UHUB
aws cloudfront create-invalidation --profile=UHUB --distribution-id E1XKPF4EYQIG6D  --paths /\* 
# | jq .Invalidation -r