"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Array.prototype.unique = function () {
  var a = this.concat();

  for (var i = 0; i < a.length; ++i) {
    for (var j = i + 1; j < a.length; ++j) {
      if (a[i] === a[j]) a.splice(j--, 1);
    }
  }

  return a;
};
/*
 * Frontend Logic for application
 *
 */


var endpoint = 'https://bapi.uhub.team';
var app = {
  encryption_key: endpoint.indexOf('localhost') > -1 ? 'ek_test_lF0W00Ds6hN45ZwAdQZQy2VZXSHHNU' : 'ek_live_MSc1djH6K72YHrVQW4guhnFm8wIugu'
};
app.lists = {};
app.session = {};
app.institution = {
  membership: {}
};
app.ckeditor = {}; // Get data from query string

app.getFromQuery = function (param) {
  var urlParams = new URLSearchParams(window.location.search);
  return urlParams.get(param);
};

app.printDiv = function (divName) {
  var documentPrintable = document.getElementById(divName);
  var documentChield = documentPrintable.children[0];
  documentChield.classList.add("w-100");
  var printContents = document.getElementById(divName).innerHTML;
  var originalContents = document.body.innerHTML;
  document.body.innerHTML = "\n       <div class=\"d-flex flex-column h-100 w-100 p-3\">\n        <div class=\"container d-flex align-content-center flex-row mb-3\">\n            <img src=\"".concat(app.getData('applicationLogo'), "\" class=\"printableLogo\" width=\"64\"/>\n        </div>\n        <h1 class=\"text-center\">\n            Comprovante de pagamento ").concat(app.getData('applicationName'), "\n        </h1>\n        <div class=\"mt-3 mb-5\">\n            <p class=\"d-flex justify-content-center\">A ").concat(app.getData('applicationName'), " comprova que&nbsp;<strong>").concat(app.user.name, "</strong>&nbsp;realizou o pagamento dos itens abaixo por meio da plataforma da Uhub</p>\n        </div>\n        ").concat(printContents, "\n        CNPJ: 51.201.093/0001-53\n       </div>\n    ");
  window.print();
  document.body.innerHTML = originalContents;
  documentChield.classList.remove("w-100");
};

app.sendMessageAvaliators = function (modalId) {
  $("#".concat(modalId)).modal('hide');
  var congressId = document.getElementById('sendMessageAvaliatorButton').getAttribute('congress');
  var oCongress = app.lists.congresses[congressId];
  app.lists.categories = app.user.cordinatored[congressId].categs;
  var dataHtml = "\n        <div class=\"form-group\">\n            <label for=\"textarea-message-avalitor\">Mensagem que ser\xE1 enviada para os avaliadores.</label>\n            <textarea class=\"form-control\" id=\"textarea-message-avalitor\" rows=\"3\"></textarea>\n        </div>\n       \n    ";

  if (typeof oCongress.type == 'string' && oCongress.type == 'regional' && typeof oCongress.subdivision == 'boolean' && oCongress.subdivision && _typeof(oCongress.subdivs) == 'object' && oCongress.subdivs instanceof Array && oCongress.subdivs.length > 0) {
    dataHtml += '<hr><span>Região</span>';
    dataHtml += "<select name=\"select-region\" id=\"select-region\" class=\"select-region custom-select mb-3\" >";
    dataHtml += "<option selected>Selecione...</option>";

    for (var key in oCongress.subdivs) {
      if (Object.hasOwnProperty.call(oCongress.subdivs, key)) {
        var region = oCongress.subdivs[key];
        dataHtml += "<option value=\"".concat(region, "\">").concat(region, "</option>");
      }
    }

    dataHtml += "</select>";
  }

  dataHtml += '<hr><div class="form-group"><span>Modalidade</span>';
  dataHtml += '<div class="d-flex-row justify-content-between mt-3">';

  for (var index = 0; index < app.lists.categories.length; index++) {
    var modalidade = app.lists.categories[index].split(' ');
    var category = "".concat(modalidade[0], " ").concat(modalidade[1]);
    dataHtml += "\n            <div class=\"form-check form-check-inline col-3\">\n                <input class=\"form-check-input checkbox-categ-avaliator\" type=\"checkbox\" id=\"".concat(category.replace(" ", ""), "\" value=\"").concat(category, "\" >\n                <label class=\"form-check-label\" for=\"").concat(category.replace(" ", ""), "\">").concat(category, "</label>\n            </div>\n        ");
  }

  dataHtml += '</div></div>';
  Swal.fire({
    title: 'Atenção',
    html: "\n            <div class=\"col-lg-12\">\n            ".concat(dataHtml, "\n            </div>\n        "),
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Enviar Comunicado',
    cancelButtonText: 'Cancelar'
  }).then(function (result) {
    if (result.value) {
      var categCheck = [];
      var categories = $('.checkbox-categ-avaliator:checkbox:checked');
      var region = $('#select-region').val() || '';
      var message = $('#textarea-message-avalitor').val();

      for (var _index = 0; _index < categories.length; _index++) {
        var categ = categories[_index];
        categCheck.push(categ.value);
      }

      var payloadObj = {
        access_token: app.getData('token'),
        host: window.location.host,
        congressId: congressId,
        message: message,
        categs: categCheck,
        region: region
      };
      app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Congress/Cordinator/Notification', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
        if (statusCode == 200 && responsePayload) {
          Swal.fire('', 'Comunicado foi enviado com sucesso.', 'success');
        } else {
          Swal.fire('', 'Houve um problema para enviar o comunicado, tente novamente mais tarde.', 'warning');
        }
      });
    }

    $("#".concat(modalId)).modal('show');
  });
};

app.sendMessageAvaliatorsNacional = function (modalId, workId, congressId) {
  $("#".concat(modalId)).modal('hide');
  var dataHtml = "\n        <div class=\"form-group\">\n            <label for=\"textarea-message-avalitor\">Mensagem que ser\xE1 enviada para os avaliadores.</label>\n            <textarea class=\"form-control\" id=\"textarea-message-avalitor\" rows=\"3\"></textarea>\n        </div>\n       \n    ";
  Swal.fire({
    title: 'Atenção',
    html: "\n            <div class=\"col-lg-12\">\n            ".concat(dataHtml, "\n            </div>\n        "),
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Enviar Comunicado',
    cancelButtonText: 'Cancelar'
  }).then(function (result) {
    if (result.value) {
      var message = $('#textarea-message-avalitor').val();
      var payloadObj = {
        access_token: app.getData('token'),
        host: window.location.host,
        message: message,
        congressId: congressId,
        workId: workId
      };
      app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Congress/Cordinator/Notification/Work', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
        if (statusCode == 200 && responsePayload) {
          Swal.fire('', 'Comunicado foi enviado com sucesso.', 'success');
        } else {
          Swal.fire('', 'Houve um problema para enviar o comunicado, tente novamente mais tarde.', 'warning');
        }
      });
    }

    $("#".concat(modalId)).modal('show');
  });
};

app.inactiveWork = function (modalId, workid) {
  $("#".concat(modalId)).modal('hide');
  var congressId = document.getElementById('sendMessageAvaliatorButton').getAttribute('congress');
  app.lists.categories = app.user.cordinatored[congressId].categs;
  var dataHtml = "\n        <div class=\"form-group\">\n            <label for=\"textarea-message-avalitor\">Mensagem que ser\xE1 enviada para os estudante.</label>\n            <textarea class=\"form-control\" id=\"textarea-message-avalitor\" rows=\"3\"></textarea>\n        </div>\n       \n    ";
  Swal.fire({
    title: 'Atenção',
    html: "\n            <div class=\"col-lg-12\">\n                ".concat(dataHtml, "\n            </div>\n        "),
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Desclassificar',
    cancelButtonText: 'Cancelar'
  }).then(function (result) {
    if (result.value) {
      var message = $('#textarea-message-avalitor').val();
      var payloadObj = {
        access_token: app.getData('token'),
        host: window.location.host,
        congressId: congressId,
        message: message,
        workId: workid
      };
      app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Congress/Work/Declassified', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
        if (statusCode == 200 && responsePayload) {
          Swal.fire('', 'Trabalho desclassificado com sucesso.', 'success');
        } else {
          Swal.fire('', 'Houve um problema para enviar o comunicado, tente novamente mais tarde.', 'warning');
        }
      });
    }

    $("#".concat(modalId)).modal('show');
  });
};

app.sendAvaliations = function (modalId) {
  var congressId = document.getElementById('sendAvaliationButton').getAttribute('congress');
  app.lists.categories = app.user.cordinatored[congressId].categs;
  var oCongress = app.lists.congresses[congressId];
  var dataHtml = "<span>Após você enviar os trabalhos os algoritimos da Intercom vão processar todas os trabalhos de sua modalidade, aprovando e reprovando automaticamente de acordo com as notas que você aplicou aos trabalhos.<span><hr>";
  dataHtml += '<div class="d-flex-row justify-content-between mt-3">';

  for (var index = 0; index < app.lists.categories.length; index++) {
    var modalidade = app.lists.categories[index].split(' ');
    var category = "".concat(modalidade[0], " ").concat(modalidade[1]);
    dataHtml += "\n            <div class=\"form-check form-check-inline col-3\">\n                <input class=\"form-check-input checkbox-categ-avaliator\" type=\"checkbox\" id=\"".concat(category.replace(" ", ""), "\" value=\"").concat(category, "\" >\n                <label class=\"form-check-label\" for=\"").concat(category.replace(" ", ""), "\">").concat(category, "</label>\n            </div>\n        ");
  }

  dataHtml += '</div>';

  if (typeof oCongress.subdivision == 'boolean' && oCongress.subdivision && _typeof(oCongress.subdivs) == 'object' && oCongress.subdivs instanceof Array && oCongress.subdivs.length > 0) {
    dataHtml += '<hr><span>Região</span>';
    dataHtml += "<select name=\"select-region\" id=\"select-region\" class=\"select-region custom-select mb-3\" >";
    dataHtml += "<option selected>Selecione...</option>";

    for (var key in oCongress.subdivs) {
      if (Object.hasOwnProperty.call(oCongress.subdivs, key)) {
        var region = oCongress.subdivs[key];
        dataHtml += "<option value=\"".concat(region, "\">").concat(region, "</option>");
      }
    }

    dataHtml += "</select>";
  }

  Swal.fire({
    title: 'Atenção',
    html: "\n            <div class=\"col-lg-12\">\n            ".concat(dataHtml, "\n            </div>\n        "),
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Vamos lá, pode processar as avaliações',
    cancelButtonText: 'Cancelar'
  }).then(function (result) {
    if (result.value) {
      var categCheck = [];
      var categories = $('.checkbox-categ-avaliator:checkbox:checked');

      for (var _index2 = 0; _index2 < categories.length; _index2++) {
        var categ = categories[_index2];
        categCheck.push(categ.value);
      }

      var region = $('#select-region').val();
      var payloadObj = {
        access_token: app.getData('token'),
        host: window.location.host,
        congressId: congressId,
        categs: categCheck,
        region: region
      };
      app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Works/Finish', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
        if (statusCode == 200 && responsePayload) {
          Swal.fire('', 'Trabalhos enviados com sucesso.', 'success');
        }

        if (statusCode == 411 && responsePayload) {
          Swal.fire('', 'Os trabalhos já foram enviados, caso seja necessário reenviar ou modificar classificações entre em contato com o suporte.', 'error');
        } else {
          Swal.fire('', 'Houve um problema para enviar os trabalhos, tente novamente mais tarde.', 'warning');
        }
      });
    }
  });
};

app.bindLoadHistory = function () {
  var oStatus = {
    paid: 'Pago',
    waiting_payment: "Aguardando Pagamento",
    platform_include: "Pagamento Antecipado"
  };
  var colorTheme = {
    "waiting_payment": "bg-warning text-white",
    "paid": "bg-success text-white"
  };
  var iconTheme = {
    platform_include: "<i class=\"fad fa-cash-register\" ></i>",
    boleto: "<i class=\"fad fa-file-invoice-dollar\"></i>",
    credit_card: "<i class=\"fad fa-credit-card\"></i>",
    pix: "<i class=\"fab fa-xing\"></i>"
  };
  var queryStringObj = {
    host: window.location.host,
    access_token: app.getData('token')
  };
  app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/User/Transactions', 'GET', queryStringObj, undefined, function (statusCode, responsePayload) {
    if (statusCode == 200) {
      var sHtmlTemplate = '';
      responsePayload.forEach(function (row) {
        //console.log(row.type, row.status)
        var buttonBoleto = '';
        var buttonHtml = '';

        if (row.type == 'boleto' && row.status == 'waiting_payment') {
          buttonBoleto = "<a href=\"".concat(row.tx.boleto_url, "\" target=\"_blank\" class=\"btn btn-primary\"><i class=\"fad fa-file-invoice-dollar\"></i> Visualizar Boleto</a> ");
        }

        if (row.type == 'pix' && row.status == 'waiting_payment') {
          //console.log(row.tx.pix_expiration_date)
          buttonBoleto = "<button onClick=\"app.loadPixPayment('".concat(row.tx.pix_qr_code, "')\" class=\"btn btn-primary\"><i class=\"fad fa-qrcode\"></i> Visualizar QRCode</a>");
        }

        var historyContentIcon = "\n                    <div class=\"tracking-icon status-inforeceived ".concat(colorTheme[row.status], "\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Transa\xE7\xE3o anterior a Uhub\">\n                        ").concat(iconTheme[row.type], "\n                    </div>\n                ");
        var historyContentBody = "\n                    <div class=\"tracking-date\">".concat(new Date(row.date).toLocaleString(), "</div>\n                    <div class=\"tracking-content\">\n                        <div class=\"card\">\n                            <div class=\"card-header d-flex flex-row justify-content-between\">\n                                <strong><i><small>#").concat(row.id, "</small></i></strong>\n                                <button type=\"button\" class=\"btn btn-secondary\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Transa\xE7\xF5es anteriores a Uhub servem apenas como hist\xF3rico e n\xE3o possuem comprovante.\"/><i class=\"fad fa-print\"></i> Imprimir Comprovante</button>\n                            </div>\n                            <div class=\"card-body\">\n                                <table class=\"table table-hover table-bordered\">\n                                    <thead>\n                                        <tr>\n                                            <th scope=\"col\">Item</th>\n                                            <th scope=\"col\">Data</th>\n                                            <th scope=\"col\">Valor</th>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr>\n                                            <td>Anuidade da associa\xE7\xE3o referente a ").concat(new Date(row.date).getFullYear(), "</td>\n                                            <td>").concat(new Date(row.date).toLocaleString(), "</td>\n                                            <td>R$ ").concat((row.amount / 100).toLocaleString('pt-BR', {
          minimumFractionDigits: 2,
          maximumFractionDigits: 3
        }), "</td>\n                                        </tr>\n                                    </tbody>\n                                </table>\n                            </div>\n                            <div class=\"card-footer d-flex flex-row justify-content-between\">\n                                <p>\n                                    <strong>").concat(buttonBoleto, " Status:</strong> ").concat(oStatus[row.status], "<br>\n                                </p>\n                                <p>\n                                    <strong>Valor:</strong> R$ ").concat((row.amount / 100).toLocaleString('pt-BR', {
          minimumFractionDigits: 2,
          maximumFractionDigits: 3
        }), "<br>\n                                </p>\n                            </div>\n                        </div>\n                    </div>\n                    \n                ");

        if (row.type != 'platform_include') {
          historyContentIcon = "\n                        <div class=\"tracking-icon status-inforeceived ".concat(colorTheme[row.status], "\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Transa\xE7\xE3o realizada com a Uhub\">\n                            ").concat(iconTheme[row.type], "\n                        </div>\n                    ");
          var itemContent = '';

          for (var index = 0; index < row.tx.items.length; index++) {
            var oItem = row.tx.items[index];
            itemContent += "\n                            <tr>\n                                <td>".concat(oItem.title, "</td>\n                                <td>").concat(new Date(row.date).toLocaleString(), "</td>\n                                <td>R$ ").concat((oItem.unit_price / 100).toLocaleString('pt-BR', {
              minimumFractionDigits: 2,
              maximumFractionDigits: 3
            }), "</td>\n                            </tr>\n                        ");
          }

          buttonHtml = buttonBoleto;

          if (buttonBoleto.length < 1) {
            buttonHtml = "<button onclick=\"app.printDiv('printable-".concat(row.id, "')\" class=\"btn btn-primary\"/><i class=\"fad fa-print\"></i> Imprimir Comprovante</button>");
          }

          historyContentBody = "\n                        <div class=\"tracking-date\">".concat(new Date(row.date).toLocaleString(), "</div>\n                        <div class=\"tracking-content\">\n                            <div id=\"printable-").concat(row.id, "\">\n                                \n                                <div class=\"card mb-3\">\n                                    <div class=\"card-header d-flex flex-row justify-content-between\">\n                                        <strong><i><small>#").concat(row.id, "</small></i></strong>\n                                        ").concat(buttonHtml, "\n                                    </div>\n                                    <div class=\"card-body\">\n                                        <table class=\"table table-hover table-bordered\">\n                                            <thead>\n                                                <tr>\n                                                    <th scope=\"col\">Item</th>\n                                                    <th scope=\"col\">Data</th>\n                                                    <th scope=\"col\">Valor</th>\n                                                </tr>\n                                            </thead>\n                                            <tbody>\n                                                ").concat(itemContent, "\n                                            </tbody>\n                                        </table>\n                                    </div>\n                                    <div class=\"card-footer d-flex flex-row justify-content-between\">\n                                        <p>\n                                            <strong>Status:</strong> ").concat(oStatus[row.status], "<br>\n                                        </p>\n                                        <p>\n                                            <strong>Valor:</strong> R$ ").concat((row.amount / 100).toLocaleString('pt-BR', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 3
          }), "<br>\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                           \n                        </div>\n                        \n                    "); // Exibir o comprovante pois já o deve possuir.
          //console.log()
        }

        sHtmlTemplate += "\n                    <div class=\"tracking-item\">\n                        ".concat(historyContentIcon, "\n                        ").concat(historyContentBody, "\n                    </div>");
      });
      document.getElementById('historyTransaction').innerHTML = sHtmlTemplate;
      $('[data-toggle="tooltip"]').tooltip();
      $('.page-hover').fadeOut();
    }
  });
};

app.loadModalCongress = function (index) {
  if (app.institution.congresses) {
    document.getElementById('eventModalTitle').innerHTML = app.institution.congresses[index].name;
    document.getElementById('eventModalContent').innerHTML = app.institution.congresses[index].resume;
    $('#eventModal').modal('show');
  }
};

app.resetElement = function (elm) {
  var select = elm;
  var length = select.options.length;

  for (var i = length - 1; i >= 0; i--) {
    select.options[i] = null;
  }
};

app.modalConcentActionDiff = function (congressId, modalPrefix, type) {
  document.querySelector('#inscriptionWorkAlumn').style.display = 'block';
  document.querySelectorAll('.regional').forEach(function (elm) {
    elm.remove();
  });
  document.getElementById('myTypeSubmition').value = type;
  $("#".concat(modalPrefix, "Modal")).modal('show'); // 

  if (app.institution && app.institution.congresses) {
    var _loop = function _loop(index) {
      var oCongress = app.institution.congresses[index];

      if (oCongress.id == congressId) {
        if (_typeof(oCongress.categories) == 'object' && oCongress.categories instanceof Array && oCongress.categories.length > 0) {
          categSelect = document.querySelector("#".concat(modalPrefix, "Modal #category"));
          app.resetElement(categSelect);
          categSelect.options[categSelect.options.length] = new Option("Selecione...", "");

          for (var _index3 = 0; _index3 < oCongress.categories.length; _index3++) {
            var oCategory = oCongress.categories[_index3];
            var prefixCategory = oCategory.substring(0, 2);

            if (type == 'research' && prefixCategory == 'GP') {
              categSelect.options[categSelect.options.length] = new Option(oCategory, oCategory);
            } else if (type == 'intercomjr' && prefixCategory == 'IJ') {
              categSelect.options[categSelect.options.length] = new Option(oCategory, oCategory);
            }
          }
        }

        if (_typeof(oCongress.iesList) == 'object' && oCongress.iesList instanceof Array && oCongress.iesList.length > 0) {
          ieSelect = document.querySelector("#".concat(modalPrefix, "Modal #ies"));
          app.resetElement(ieSelect);

          for (var _index4 = 0; _index4 < oCongress.iesList.length; _index4++) {
            var ies = oCongress.iesList[_index4];
            ieSelect.options[ieSelect.options.length] = new Option(ies.name, ies.name);
          }
        }

        aSubtype = [];

        if (type == 'research') {
          document.querySelectorAll('.intercomjr').forEach(function (elm) {
            elm.remove();
          });
          aSubtype.push('Doutores(as)');
          aSubtype.push('Doutorandos(as)');
          aSubtype.push('Mestres');
          aSubtype.push('Mestrandos(as)');
        } else if (type == 'intercomjr') {
          document.querySelectorAll('.research').forEach(function (elm) {
            elm.remove();
          });
          aSubtype.push('Graduação (recém-graduado e graduando)');
        }

        subtypeSelect = document.querySelector("#".concat(modalPrefix, "Modal #subtype"));
        app.resetElement(subtypeSelect);

        for (var _index5 = 0; _index5 < aSubtype.length; _index5++) {
          var subtype = aSubtype[_index5];
          subtypeSelect.options[subtypeSelect.options.length] = new Option(subtype, subtype);
        }

        if (modalPrefix == 'inscription') {
          priceSelect = document.getElementById('priceSelect');

          for (var _index6 = 0; _index6 < oCongress.products.length; _index6++) {
            var oProduct = oCongress.products[_index6];
            priceSelect.options[priceSelect.options.length] = new Option(oProduct.text, oProduct.value);
          }

          document.querySelector('.payme-inscription-btn').addEventListener('click', function () {
            $('.page-hover').fadeIn();
            $("#".concat(modalPrefix, "Modal")).modal('hide');
            var membership = app.getData('membership');
            membership = typeof membership == 'string' && membership.length > 0 ? JSON.parse(membership) : false;

            if (_typeof(membership) == 'object' && typeof membership.active == 'boolean' && membership.active) {
              var payloadObj = {
                access_token: app.getData('token'),
                host: window.location.host,
                congressId: oCongress.id,
                products: app.institution.congress,
                associated: app.user.id,
                oPrice: {
                  label: document.getElementById('priceSelect').options[document.getElementById('priceSelect').selectedIndex].text,
                  value: document.getElementById('priceSelect').value
                }
              };
              app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Congress/Inscription', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
                if (statusCode == 200 && responsePayload) {
                  Swal.fire('Sucesso', 'A sua inscrição foi registrada com sucesso.', 'success').then(function (e) {
                    document.location = '/pages/account-home.html?upd=evn';
                  });
                }

                $('.page-hover').fadeOut();
              });
              return true;
            } else {
              var points = 0;
              points += app.user.name ? 1 : 0;
              points += app.user.birthday ? 1 : 0;
              points += app.user.cpf ? 1 : 0;
              points += app.user.phone ? 1 : 0;
              points += app.user.mobile ? 1 : 0;
              points += app.user.email ? 1 : 0;
              points += app.user.address ? 1 : 0;
              points += app.user.address_number ? 1 : 0;
              points += app.user.address_complement ? 1 : 0;
              points += app.user.address_neighborhood ? 1 : 0;
              points += app.user.address_city ? 1 : 0;
              points += app.user.address_state ? 1 : 0;
              points += app.user.address_country ? 1 : 0;
              points += app.user.postal_code ? 1 : 0;

              if (points >= 13) {
                $('.page-hover').fadeIn();
                var value = document.getElementById('priceSelect').value;
                app.institution.congress = [{
                  id: oCongress.id,
                  name: document.getElementById('priceSelect').options[document.getElementById('priceSelect').selectedIndex].text,
                  price: +value
                }];
                var checkout = new PagarMeCheckout.Checkout({
                  encryption_key: app.encryption_key,
                  success: function success(data) {
                    var payloadObj = _objectSpread({
                      access_token: app.getData('token'),
                      host: window.location.host,
                      congressId: oCongress.id,
                      products: app.institution.congress,
                      oPrice: {
                        label: document.getElementById('priceSelect').options[document.getElementById('priceSelect').selectedIndex].text,
                        value: document.getElementById('priceSelect').value
                      }
                    }, data);

                    app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Congress/Inscription', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
                      if (statusCode == 200 && responsePayload) {
                        if (responsePayload.status == 'paid') {
                          Swal.fire('Sucesso', 'O seu pagamento foi processado com sucesso.', 'success').then(function (e) {
                            document.location = '/pages/account-home.html?upd=evn';
                          });
                        }

                        if (responsePayload.type == 'boleto') {
                          var boleto_url = responsePayload.boleto_url;
                          var boleto_barcode = responsePayload.boleto_barcode;
                          document.querySelector('.barcode-copy').innerText = boleto_barcode;
                          document.querySelector('.pdf-file').href = boleto_url;
                          $('#applicationModal').modal('show');
                        }

                        if (responsePayload.type == 'pix') {
                          document.getElementById('qrcode').innerHTML = '';
                          document.querySelector('.pixcode-copy').innerText = '';
                          $('#qrcode').qrcode(responsePayload.tx.pix_qr_code);
                          $('#pixModal').modal('show');
                          document.querySelector('.pixcode-copy').innerText = responsePayload.tx.pix_qr_code;
                        }
                      }

                      $('.page-hover').fadeOut();
                    });
                  },
                  error: function error(err) {//console.log(err);
                  },
                  close: function close() {//console.log('The modal has been closed.');
                  }
                });
                var aPhoneNumbers = new Array();
                aPhoneNumbers.push("+55".concat(app.user.mobile.replace(/[^0-9]/g, "")));
                var customerData = {
                  name: app.user.name,
                  email: app.user.email,
                  country: "br",
                  external_id: app.user.email,
                  documents: [{
                    type: "cpf",
                    number: app.user.cpf.replace(/[^0-9]/g, "")
                  }],
                  type: "individual",
                  phone_numbers: aPhoneNumbers
                };
                var billingData = {
                  name: app.user.name,
                  address: {
                    country: "br",
                    state: app.user.address_state,
                    city: app.user.address_city,
                    neighborhood: app.user.address_neighborhood,
                    complementary: app.user.address_complement,
                    street: app.user.address,
                    street_number: app.user.address_number,
                    zipcode: app.user.postal_code.replace(/\D/gm, "")
                  }
                };
                var iAmountPrice = 0;
                var aItems = [];
                var aPixItems = [];

                for (var _index7 = 0; _index7 < app.institution.congress.length; _index7++) {
                  var _oProduct = app.institution.congress[_index7];
                  var oProductTemplate = {
                    id: _oProduct.id,
                    title: _oProduct.name,
                    unit_price: _oProduct.price,
                    quantity: 1,
                    tangible: 'false'
                  };
                  iAmountPrice += _oProduct.price;
                  aItems.push(oProductTemplate);
                  aPixItems.push({
                    name: _oProduct.name,
                    value: "".concat(_oProduct.price)
                  });
                }

                var date = new Date();
                date.setDate(date.getDate() + 1);
                var pixExpirationDate = "".concat(date.getFullYear(), "-").concat(("0" + (date.getMonth() + 1)).slice(-2), "-").concat(("0" + date.getDate()).slice(-2)); //console.log(aPixItems)

                var pagarmeTemp = {
                  amount: iAmountPrice,
                  customerData: 'false',
                  createToken: 'true',
                  paymentMethods: 'credit_card,boleto,pix',
                  pix_expiration_date: pixExpirationDate,
                  pix_additional_fields: aPixItems,
                  boletoDiscountPercentage: 0,
                  items: aItems,
                  customer: customerData,
                  billing: billingData
                }; //console.log(pagarmeTemp)

                checkout.open(pagarmeTemp);
              } else {
                $('#inscriptionModal').modal('hide');
                Swal.fire('Importante', 'Para realizar a inscrição no evento, você primeiro precisa terminar de preencher os dados necessários em seu cadastro.', 'info').then(function (e) {
                  $('#myTab a[href="#profile"]').tab('show');
                  $('.page-hover').fadeOut();
                  document.querySelector('.formInfo').style.display = 'block';
                  document.querySelector('.formInfo').innerHTML = "<i class=\"fad fa-check-circle\"></i> Preencha os dados abaixo para poder realizar a inscri\xE7\xE3o";
                });
              }
            }
          });
          text = oCongress.resume;
          regex = /(<([^>]+)>)/ig;
          text = text.replace(regex, "");
          document.querySelector('.inscription-event-name').innerText = oCongress.name;
          document.querySelector('.inscription-event-desc').innerHTML = text.slice(0, 150);
        } else if (modalPrefix == 'avl') {
          //console.log(`#${modalPrefix}Modal`, oCongress)
          if (_typeof(oCongress.categories) == 'object' && oCongress.categories instanceof Array && oCongress.categories.length > 0) {
            $('.las-categories').html('');

            for (var _index8 = 0; _index8 < oCongress.categories.length; _index8++) {
              var _oCategory = oCongress.categories[_index8];
              $('.las-categories').append("\n                                <div class=\"form-check\">\n                                    <input class=\"form-check-input\" type=\"checkbox\" id=\"dynamicCategCheck".concat(_index8, "\" name=\"").concat(_oCategory, "\">\n                                    <label class=\"form-check-label\" for=\"dynamicCategCheck").concat(_index8, "\">").concat(_oCategory, "</label>\n                                </div>\n                            "));
            }
          }

          if (_typeof(oCongress.iesList) == 'object' && oCongress.iesList instanceof Array && oCongress.iesList.length > 0) {
            ieSelect = document.getElementById('las-ies');

            for (var _index9 = 0; _index9 < oCongress.iesList.length; _index9++) {
              var _ies = oCongress.iesList[_index9];
              ieSelect.options[ieSelect.options.length] = new Option(_ies.name, _ies.name);
            }
          }

          document.getElementById('congressIdAvaliation').value = oCongress.id;
          document.getElementById('eventHostModalCongressAvaliator').value = window.location.host;
        } else if (modalPrefix == 'workinscription') {
          document.getElementById('congressIdWorkSub').value = oCongress.id; // document.getElementById('lectiveYearLabel').innerText = `O trabalho foi realizado no ano letivo de ${(new Date().getFullYear() - 1)}?`

          document.getElementById('workIdWorkSub').value = '';
          if (document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners')) document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners').forEach(function (e) {
            e.remove();
          });
          ClassicEditor.create(document.querySelector('#resumeWork'), {
            removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
            cloudServices: {
              tokenUrl: 'https://79675.cke-cs.com/token/dev/05f801120cf7cccdb540ea25a7068d7d1c8108b02232eb1bfec6b89be13c',
              uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
            }
          }).then(function (editor) {
            app.ckeditor.resumeWork = editor;
          })["catch"](function (error) {
            console.error(error);
          });
          ClassicEditor.create(document.querySelector('#studyObject'), {
            removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
            cloudServices: {
              tokenUrl: 'https://79675.cke-cs.com/token/dev/05f801120cf7cccdb540ea25a7068d7d1c8108b02232eb1bfec6b89be13c',
              uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
            }
          }).then(function (editor) {
            app.ckeditor.studyObject = editor;
          })["catch"](function (error) {
            console.error(error);
          });
          ClassicEditor.create(document.querySelector('#researchRealized'), {
            removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
            cloudServices: {
              tokenUrl: 'https://79675.cke-cs.com/token/dev/05f801120cf7cccdb540ea25a7068d7d1c8108b02232eb1bfec6b89be13c',
              uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
            }
          }).then(function (editor) {
            app.ckeditor.researchRealized = editor;
          })["catch"](function (error) {
            console.error(error);
          });
          ClassicEditor.create(document.querySelector('#productionDescription'), {
            removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
            cloudServices: {
              tokenUrl: 'https://79675.cke-cs.com/token/dev/05f801120cf7cccdb540ea25a7068d7d1c8108b02232eb1bfec6b89be13c',
              uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
            }
          }).then(function (editor) {
            app.ckeditor.productionDescription = editor;
          })["catch"](function (error) {
            console.error(error);
          });
        }
      }
    };

    for (var index = 0; index < app.institution.congresses.length; index++) {
      var categSelect;
      var ieSelect;
      var aSubtype;
      var subtypeSelect;
      var priceSelect;
      var text;
      var regex;
      var ieSelect;

      _loop(index);
    }
  }
};

app.loadModalActionDiff = function (congressId, modalPrefix) {
  document.querySelector('.type-picker').style.display = 'block';
  Swal.fire({
    title: '<strong>Submissão de Trabalho</strong>',
    icon: 'info',
    html: 'Para prosseguir por favor selecione para qual tipo você deseja submeter o trabalho',
    showCloseButton: true,
    showCancelButton: true,
    confirmButtonColor: '#ff0000',
    cancelButtonColor: '#0000ff',
    focusConfirm: false,
    confirmButtonText: '<i class="fad fa-laptop-house"></i> Grupo de Pesquisa',
    confirmButtonAriaLabel: 'Grupo de Pesquisa',
    cancelButtonText: '<i class="fas fa-user-graduate"></i> Intercom Jr',
    cancelButtonAriaLabel: 'Intercom Jr'
  }).then(function (e) {
    if (e.value) {
      app.modalConcentActionDiff(congressId, modalPrefix, 'research');
    } else if (e.dismiss == 'cancel') {
      app.modalConcentActionDiff(congressId, modalPrefix, 'intercomjr');
    }
  });
};

app.loadModalAction = function (key, modalPrefix) {
  document.querySelector('.type-picker').style.display = 'none';
  document.querySelector('#inscriptionWorkAlumn').style.display = 'block';
  $("#".concat(modalPrefix, "Modal")).modal('show');

  if (app.institution && app.institution.congresses) {
    var oCongress = app.institution.congresses[key];

    if (modalPrefix == 'inscription') {
      var priceSelect = document.getElementById('priceSelect');

      for (var index = 0; index < oCongress.products.length; index++) {
        var oProduct = oCongress.products[index];
        priceSelect.options[priceSelect.options.length] = new Option(oProduct.text, oProduct.value);
      }

      document.querySelector('.payme-inscription-btn').addEventListener('click', function () {
        $('.page-hover').fadeIn();
        $("#".concat(modalPrefix, "Modal")).modal('hide');
        var membership = app.getData('membership');
        membership = typeof membership == 'string' && membership.length > 0 ? JSON.parse(membership) : false;

        if (_typeof(membership) == 'object' && typeof membership.active == 'boolean' && membership.active) {
          var payloadObj = {
            access_token: app.getData('token'),
            host: window.location.host,
            congressId: oCongress.id,
            products: app.institution.congress,
            associated: app.user.id,
            oPrice: {
              label: document.getElementById('priceSelect').options[document.getElementById('priceSelect').selectedIndex].text,
              value: document.getElementById('priceSelect').value
            }
          };
          app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Congress/Inscription', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
            if (statusCode == 200 && responsePayload) {
              Swal.fire('Sucesso', 'A sua inscrição foi registrada com sucesso.', 'success').then(function (e) {
                document.location = '/pages/account-home.html?upd=evn';
              });
            }

            $('.page-hover').fadeOut();
          });
          return true;
        } else {
          var points = 0;
          points += app.user.name ? 1 : 0;
          points += app.user.birthday ? 1 : 0;
          points += app.user.cpf ? 1 : 0;
          points += app.user.phone ? 1 : 0;
          points += app.user.mobile ? 1 : 0;
          points += app.user.email ? 1 : 0;
          points += app.user.address ? 1 : 0;
          points += app.user.address_number ? 1 : 0;
          points += app.user.address_complement ? 1 : 0;
          points += app.user.address_neighborhood ? 1 : 0;
          points += app.user.address_city ? 1 : 0;
          points += app.user.address_state ? 1 : 0;
          points += app.user.address_country ? 1 : 0;
          points += app.user.postal_code ? 1 : 0;

          if (points >= 13) {
            $('.page-hover').fadeIn();
            var value = document.getElementById('priceSelect').value;
            app.institution.congress = [{
              id: oCongress.id,
              name: document.getElementById('priceSelect').options[document.getElementById('priceSelect').selectedIndex].text,
              price: +value
            }];
            var checkout = new PagarMeCheckout.Checkout({
              encryption_key: app.encryption_key,
              success: function success(data) {
                var payloadObj = _objectSpread({
                  access_token: app.getData('token'),
                  host: window.location.host,
                  congressId: oCongress.id,
                  products: app.institution.congress,
                  oPrice: {
                    label: document.getElementById('priceSelect').options[document.getElementById('priceSelect').selectedIndex].text,
                    value: document.getElementById('priceSelect').value
                  }
                }, data);

                app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Congress/Inscription', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
                  if (statusCode == 200 && responsePayload) {
                    if (responsePayload.status == 'paid') {
                      Swal.fire('Sucesso', 'O seu pagamento foi processado com sucesso.', 'success').then(function (e) {
                        document.location = '/pages/account-home.html?upd=evn';
                      });
                    }

                    if (responsePayload.type == 'boleto') {
                      var boleto_url = responsePayload.boleto_url;
                      var boleto_barcode = responsePayload.boleto_barcode;
                      document.querySelector('.barcode-copy').innerText = boleto_barcode;
                      document.querySelector('.pdf-file').href = boleto_url;
                      $('#applicationModal').modal('show');
                    }

                    if (responsePayload.type == 'pix') {
                      document.getElementById('qrcode').innerHTML = '';
                      document.querySelector('.pixcode-copy').innerText = '';
                      $('#qrcode').qrcode(responsePayload.tx.pix_qr_code);
                      $('#pixModal').modal('show');
                      document.querySelector('.pixcode-copy').innerText = responsePayload.tx.pix_qr_code;
                    }
                  }

                  $('.page-hover').fadeOut();
                });
              },
              error: function error(err) {//console.log(err);
              },
              close: function close() {//console.log('The modal has been closed.');
              }
            });
            var aPhoneNumbers = new Array();
            aPhoneNumbers.push("+55".concat(app.user.mobile.replace(/[^0-9]/g, "")));
            var customerData = {
              name: app.user.name,
              email: app.user.email,
              country: "br",
              external_id: app.user.email,
              documents: [{
                type: "cpf",
                number: app.user.cpf.replace(/[^0-9]/g, "")
              }],
              type: "individual",
              phone_numbers: aPhoneNumbers
            };
            var billingData = {
              name: app.user.name,
              address: {
                country: "br",
                state: app.user.address_state,
                city: app.user.address_city,
                neighborhood: app.user.address_neighborhood,
                complementary: app.user.address_complement,
                street: app.user.address,
                street_number: app.user.address_number,
                zipcode: app.user.postal_code.replace(/\D/gm, "")
              }
            };
            var iAmountPrice = 0;
            var aItems = [];
            var aPixItems = [];

            for (var _index10 = 0; _index10 < app.institution.congress.length; _index10++) {
              var _oProduct2 = app.institution.congress[_index10];
              var oProductTemplate = {
                id: _oProduct2.id,
                title: _oProduct2.name,
                unit_price: _oProduct2.price,
                quantity: 1,
                tangible: 'false'
              };
              iAmountPrice += _oProduct2.price;
              aItems.push(oProductTemplate);
              aPixItems.push({
                name: _oProduct2.name,
                value: "".concat(_oProduct2.price)
              });
            }

            var date = new Date();
            date.setDate(date.getDate() + 1);
            var pixExpirationDate = "".concat(date.getFullYear(), "-").concat(("0" + (date.getMonth() + 1)).slice(-2), "-").concat(("0" + date.getDate()).slice(-2)); //console.log(aPixItems)

            var pagarmeTemp = {
              amount: iAmountPrice,
              customerData: 'false',
              createToken: 'true',
              paymentMethods: 'credit_card,boleto,pix',
              pix_expiration_date: pixExpirationDate,
              pix_additional_fields: aPixItems,
              boletoDiscountPercentage: 0,
              items: aItems,
              customer: customerData,
              billing: billingData
            }; //console.log(pagarmeTemp)

            checkout.open(pagarmeTemp);
          } else {
            $('#inscriptionModal').modal('hide');
            Swal.fire('Importante', 'Para realizar a inscrição no evento, você primeiro precisa terminar de preencher os dados necessários em seu cadastro.', 'info').then(function (e) {
              $('#myTab a[href="#profile"]').tab('show');
              $('.page-hover').fadeOut();
              document.querySelector('.formInfo').style.display = 'block';
              document.querySelector('.formInfo').innerHTML = "<i class=\"fad fa-check-circle\"></i> Preencha os dados abaixo para poder realizar a inscri\xE7\xE3o";
            });
          }
        }
      });
      var text = oCongress.resume;
      var regex = /(<([^>]+)>)/ig;
      text = text.replace(regex, "");
      document.querySelector('.inscription-event-name').innerText = oCongress.name;
      document.querySelector('.inscription-event-desc').innerHTML = text.slice(0, 150);
    } else if (modalPrefix == 'avl') {
      //console.log(`#${modalPrefix}Modal`, oCongress)
      if (_typeof(oCongress.categories) == 'object' && oCongress.categories instanceof Array && oCongress.categories.length > 0) {
        $('.las-categories').html('');

        for (var _index11 = 0; _index11 < oCongress.categories.length; _index11++) {
          var oCategory = oCongress.categories[_index11];
          $('.las-categories').append("\n                        <div class=\"form-check\">\n                            <input class=\"form-check-input\" type=\"checkbox\" id=\"dynamicCategCheck".concat(_index11, "\" name=\"").concat(oCategory, "\">\n                            <label class=\"form-check-label\" for=\"dynamicCategCheck").concat(_index11, "\">").concat(oCategory, "</label>\n                        </div>\n                    "));
        }
      }

      if (_typeof(oCongress.iesList) == 'object' && oCongress.iesList instanceof Array && oCongress.iesList.length > 0) {
        var ieSelect = document.getElementById('las-ies');

        for (var _index12 = 0; _index12 < oCongress.iesList.length; _index12++) {
          var ies = oCongress.iesList[_index12];
          ieSelect.options[ieSelect.options.length] = new Option(ies.name, ies.name);
        }
      }

      document.getElementById('congressIdAvaliation').value = oCongress.id;
      document.getElementById('eventHostModalCongressAvaliator').value = window.location.host;
    } else if (modalPrefix == 'workinscription') {
      document.getElementById('congressIdWorkSub').value = oCongress.id;
      document.getElementById('lectiveYearLabel').innerText = "O trabalho foi realizado no ano letivo de ".concat(new Date().getFullYear() - 1, "?");
      document.getElementById('workIdWorkSub').value = app.user["congress_".concat(oCongress.id)].submitedWorkId;
      if (document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners')) document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners').forEach(function (e) {
        e.remove();
      });
      ClassicEditor.create(document.querySelector('#resumeWork'), {
        removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
        cloudServices: {
          tokenUrl: 'https://79675.cke-cs.com/token/dev/05f801120cf7cccdb540ea25a7068d7d1c8108b02232eb1bfec6b89be13c',
          uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
        }
      }).then(function (editor) {
        app.ckeditor.resumeWork = editor;
      })["catch"](function (error) {
        console.error(error);
      });
      ClassicEditor.create(document.querySelector('#studyObject'), {
        removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
        cloudServices: {
          tokenUrl: 'https://79675.cke-cs.com/token/dev/05f801120cf7cccdb540ea25a7068d7d1c8108b02232eb1bfec6b89be13c',
          uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
        }
      }).then(function (editor) {
        app.ckeditor.studyObject = editor;
      })["catch"](function (error) {
        console.error(error);
      });
      ClassicEditor.create(document.querySelector('#researchRealized'), {
        removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
        cloudServices: {
          tokenUrl: 'https://79675.cke-cs.com/token/dev/05f801120cf7cccdb540ea25a7068d7d1c8108b02232eb1bfec6b89be13c',
          uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
        }
      }).then(function (editor) {
        app.ckeditor.researchRealized = editor;
      })["catch"](function (error) {
        console.error(error);
      });
      ClassicEditor.create(document.querySelector('#productionDescription'), {
        removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
        cloudServices: {
          tokenUrl: 'https://79675.cke-cs.com/token/dev/05f801120cf7cccdb540ea25a7068d7d1c8108b02232eb1bfec6b89be13c',
          uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
        }
      }).then(function (editor) {
        app.ckeditor.productionDescription = editor;
      })["catch"](function (error) {
        console.error(error);
      });
    }
  }
};

app.loadWorkDataModal = function (modalId, workId) {
  if (document.querySelector('.ck.ck-reset.ck-editor.ck-rounded-corners')) {
    document.querySelector('.ck.ck-reset.ck-editor.ck-rounded-corners').remove();
  }

  for (var index = 0; index < 11; index++) {
    var ele = document.getElementsByName("defaultAvaliatorOption[".concat(index, "]"));

    for (var i = 0; i < ele.length; i++) {
      ele[i].checked = false;
    }
  }

  var el = document.getElementsByName("defaultWorkOf");

  for (var idx = 0; idx < el.length; idx++) {
    el[idx].checked = false;
  }

  for (var _index13 = 0; _index13 < app.lists.workList.length; _index13++) {
    var oWork = app.lists.workList[_index13];

    if (oWork.id == workId) {
      document.getElementById('experimentalismOfProduct').value = 0;
      document.getElementById('productQuality').value = 0;
      document.getElementById('coerenceOfContent').value = 0;
      var categoryAndModule = oWork.category.split(' ');
      document.getElementById('discipline-2').innerHTML = oWork.submitionWork.discipline;
      document.getElementById('currentYear').innerHTML = oWork.submitionWork.lectiveYear == 'yes' ? 'Sim' : 'Não';
      document.getElementById('congressIdWorkAvaliator').value = oWork.congressId;
      document.getElementById('workIdWorkAvaliator').value = oWork.id;
      document.getElementById('workNameAvaliator').innerHTML = oWork.workName;
      document.getElementById('iesWorkAvaliator').innerHTML = oWork.ies;
      document.getElementById('leaderName').innerHTML = oWork.leaderName;
      document.getElementById('responsableTeatcherWorkAvaliator').innerHTML = oWork.submitionWork.responsableTeatcher;
      document.getElementById('coAuthorsWorkAvaliator').innerHTML = oWork.submitionWork.coAuthors;
      document.getElementById('studyObjectWorkAvaliator').innerHTML = oWork.submitionWork.studyObject;
      document.getElementById('researchRealizedWorkAvaliator').innerHTML = oWork.submitionWork.researchRealized;
      document.getElementById('productionDescriptionWorkAvaliator').innerHTML = oWork.submitionWork.productionDescription;

      if (typeof oWork.submitionWork.linkCloudWork == 'string' && oWork.submitionWork.linkCloudWork.length > 0) {
        document.getElementById('buttonDownloadMaterialWorkAvaliator').style.display = 'block';
        document.getElementById('buttonDownloadMaterialWorkAvaliator').href = oWork.submitionWork.linkCloudWork;
      } else {
        document.getElementById('buttonDownloadMaterialWorkAvaliator').style.display = 'none';
      }

      document.getElementById('category-1').innerHTML = categoryAndModule[0];
      document.getElementById('category-2').innerHTML = "".concat(categoryAndModule[0], " ").concat(categoryAndModule[1]);
      document.getElementById('videos-from-work').innerHTML = '';

      if (typeof oWork.correctionVideo == 'string' && oWork.correctionVideo.length > 0) {
        document.getElementById('videos-from-work').innerHTML += "\n                    <hr>\n                    <a href=\"".concat(oWork.correctionVideo, "\" class=\"btn btn-primary\" target=\"_blank\">\n                        <i class=\"fad fa-download\"></i> Link de Corre\xE7\xE3o\n                    </a>");
      }

      if (_typeof(oWork.correctionHistories) == 'object' && oWork.correctionHistories.length > 0) {
        oWork.correctionHistories.reverse();

        for (var _index14 = 0; _index14 < oWork.correctionHistories.length; _index14++) {
          var historyVid = oWork.correctionHistories[_index14];
          document.getElementById('videos-from-work').innerHTML += "\n                        <hr>\n                        <a href=\"".concat(historyVid, "\" class=\"btn btn-primary\" target=\"_blank\">\n                            <i class=\"fad fa-download\"></i> Link de Corre\xE7\xE3o\n                        </a>");
        }
      } // console.log('Avaliator',app.lists.congresses[oWork.congressId].type)


      if (_typeof(app.lists.congresses[oWork.congressId]) == 'object' && typeof app.lists.congresses[oWork.congressId].type == 'string' && app.lists.congresses[oWork.congressId].type == 'nacional' && typeof oWork.oldCongressId == 'undefined') {
        document.querySelectorAll('.avaliator-regional').forEach(function (e) {
          e.style.display = 'none';
        });
        document.querySelectorAll('.avaliator-nacional').forEach(function (e) {
          e.style.display = 'block';
        });
      } else {
        document.querySelectorAll('.avaliator-nacional').forEach(function (e) {
          e.style.display = 'none';
        });
        document.querySelectorAll('.avaliator-regional').forEach(function (e) {
          e.style.display = 'block';
        }); // document.querySelectorAll('.avaliator-resumes').style.display = 'none';
      }

      ClassicEditor.create(document.querySelector('#descriptionAvaliationWorkAvaliator'), {
        removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
        cloudServices: {
          tokenUrl: 'https://79675.cke-cs.com/token/dev/05f801120cf7cccdb540ea25a7068d7d1c8108b02232eb1bfec6b89be13c',
          uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
        }
      }).then(function (editor) {
        app.ckeditor.descriptionAvaliationWorkAvaliator = editor;
        app.ckeditor.descriptionAvaliationWorkAvaliator.setData('');
      })["catch"](function (error) {
        console.error(error);
      });
      var formName = modalId.replace('Modal', '');
      $("#".concat(formName, " .formError")).hide();
      $("#".concat(formName, " .formSuccess")).hide();
      $("#".concat(modalId)).modal('show');
    }
  }
};

app.getReferenceAvalition = function _callee(data) {
  var avaliatorsCollection, avaliationsByAvaliator, index, _avaliation, key, avaliator, avaliation, response;

  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          avaliatorsCollection = [];
          avaliationsByAvaliator = {};

          for (index = 0; index < data.length; index++) {
            _avaliation = data[index];

            if (_avaliation.customerId == app.user.id) {
              if (_typeof(avaliationsByAvaliator[_avaliation.customerId]) != 'object') {
                avaliationsByAvaliator[_avaliation.customerId] = [];
              }

              avaliationsByAvaliator[_avaliation.customerId].push(_avaliation);
            }
          }

          _context.t0 = regeneratorRuntime.keys(avaliationsByAvaliator);

        case 4:
          if ((_context.t1 = _context.t0()).done) {
            _context.next = 14;
            break;
          }

          key = _context.t1.value;

          if (!Object.hasOwnProperty.call(avaliationsByAvaliator, key)) {
            _context.next = 12;
            break;
          }

          avaliator = avaliationsByAvaliator[key];
          _context.next = 10;
          return regeneratorRuntime.awrap(avaliator.reduce(function (a, b) {
            return new Date(a.date) > new Date(b.date) ? a : b;
          }));

        case 10:
          avaliation = _context.sent;
          avaliatorsCollection.push(avaliation);

        case 12:
          _context.next = 4;
          break;

        case 14:
          response = _typeof(avaliatorsCollection) == 'object' && avaliatorsCollection.length > 0 ? avaliatorsCollection[0] : false;
          return _context.abrupt("return", response);

        case 16:
        case "end":
          return _context.stop();
      }
    }
  });
};

app.loadWorksForAvaliation = function _callee2(id) {
  var index, oCongress, dateCert, today, dataSet, _index15, element, aAvalations, categoryAndModule, child, localTable, tble;

  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          //console.log('Carregando Lista de Trabalhos', id)
          app.lists.workList = [];
          document.querySelector('.lo-footer').style.display = 'none';
          $('#workAvaliationModal').modal('show');
          index = 0;

        case 4:
          if (!(index < app.institution.congresses.length)) {
            _context2.next = 46;
            break;
          }

          oCongress = app.institution.congresses[index];

          if (!(oCongress.id == id)) {
            _context2.next = 43;
            break;
          }

          dateCert = new Date(oCongress.dateCert);
          today = new Date();

          if (today > dateCert) {
            document.querySelector('.lo-footer-congressman').style.display = 'none';
            document.querySelector('.lo-footer-avaliator').style.display = 'block';
            document.getElementById('certificateAvaliator').setAttribute('congress', oCongress.id);
            document.querySelector('.lo-footer-coordinator').style.display = 'none';
          } // Show each created check as a new row in the table


          dataSet = [];
          _index15 = 0;

        case 12:
          if (!(_index15 < oCongress.works.length)) {
            _context2.next = 36;
            break;
          }

          element = oCongress.works[_index15];

          if (!(_typeof(element.submitionWork) == 'object' && element.submitionWork && typeof element.submitionWork.workId == 'string' && element.avaliators && _typeof(element.avaliators) == 'object' && element.avaliators.indexOf(app.user.id) > -1)) {
            _context2.next = 33;
            break;
          }

          aAvalations = void 0;

          if (!(_typeof(element.avaliations) == 'object' && element.avaliations instanceof Array && element.avaliations.length > 0)) {
            _context2.next = 20;
            break;
          }

          _context2.next = 19;
          return regeneratorRuntime.awrap(app.getReferenceAvalition(element.avaliations));

        case 19:
          aAvalations = _context2.sent;

        case 20:
          categoryAndModule = element.category.split(' ');
          child = [];
          child[0] = app.lists.iesByName[element.ies] && app.lists.iesByName[element.ies].region ? app.lists.iesByName[element.ies].region : 'Não preenchido';
          child[1] = "".concat(categoryAndModule[0], " ").concat(categoryAndModule[1]);
          child[2] = element.workName.substring(0, 22) || '';
          child[3] = element.leaderName.substring(0, 18) || '';
          child[4] = app.lists.iesByName[element.ies] && app.lists.iesByName[element.ies].sigla ? app.lists.iesByName[element.ies].sigla : 'Não preenchido';

          if (typeof element.correctionVideo == 'string' && element.correctionVideo.length > 0 && typeof element.defaultCordinatorWorkOf == 'string' && element.defaultCordinatorWorkOf == 'alterationNeed') {
            child[5] = '<span class="text-primary"><i class="fas fa-sync"></i> Reavaliar</span>';
          } else {
            child[5] = aAvalations ? '<span class="text-success"><i class="fad fa-check-circle"></i> Avaliado</span>' : '<span class="text-warning"><i class="fad fa-exclamation-triangle"></i> Não avaliado</span>';
          }

          child[6] = '';
          child[7] = '';
          child[8] = "\n                        <button type=\"button\" class=\"open-modal btn btn-dark\" onClick=\"app.loadWorkDataModal('workAvaliationAvaliatorModal','".concat(element.id, "')\">\n                            Avaliar <i class=\"fad fa-clipboard-list-check\"></i>\n                        </button>\n                        \n                        ");
          app.lists.workList.push(element);
          dataSet.push(child);

        case 33:
          _index15++;
          _context2.next = 12;
          break;

        case 36:
          if ($.fn.dataTable.isDataTable('#tableWorks')) {
            localTable = $('#tableWorks').DataTable();
            localTable.destroy();
          }

          $('#tableWorks thead th').each(function () {
            var title = $(this).text();

            if (title != "Ações") {
              $(this).html("\n                            <label style=\"display: none\">".concat(title.trim(), "</label>\n                            <input type=\"text\" class=\"form-control\" placeholder=\"").concat(title.trim(), "\">\n                    "));
            }
          });
          tble = $('#tableWorks').DataTable({
            scrollX: true,
            lengthChange: true,
            buttons: ['excel', 'pdf'],
            data: dataSet,
            columnDefs: [{
              "targets": [6],
              "visible": false,
              "searchable": false
            }, {
              "targets": [7],
              "visible": false,
              "searchable": false
            }],
            order: [[1, "asc"]],
            language: {
              lengthMenu: "Exibindo _MENU_  ",
              zeroRecords: "Nenhum registro disponível.",
              info: "Exibindo pagina _PAGE_ de _PAGES_",
              search: "Filtro _INPUT_",
              paginate: {
                "next": ">",
                "previous": "<"
              }
            }
          });
          tble.columns().every(function () {
            var that = this;
            $('input', this.header()).on('keyup change', function () {
              if (that.search() !== this.value) {
                that.search(this.value).draw();
              }
            });
          });
          setTimeout(function () {
            window.dispatchEvent(new Event('resize'));
          }, 300);
          $(document).on('show.bs.modal', '.modal', function (event) {
            var zIndex = 1040 + 10 * $('.modal:visible').length;
            $(this).css('z-index', zIndex);
            setTimeout(function () {
              $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
          });
          $('.page-hover').fadeOut();

        case 43:
          index++;
          _context2.next = 4;
          break;

        case 46:
        case "end":
          return _context2.stop();
      }
    }
  });
};

app.getReferenceAvalitionForCordinator = function _callee3(data, typef) {
  var avaliatorsFinal, avaliationPoints, divider, avaliationsByAvaliator, index, _avaliation2, key, avaliator, avaliation, avaliatorStatus, typeOfRadio, _key;

  return regeneratorRuntime.async(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          if (document.querySelector('.ck.ck-reset.ck-editor.ck-rounded-corners')) {
            document.querySelector('.ck.ck-reset.ck-editor.ck-rounded-corners').remove();
          }

          document.getElementById('cordinator-avaliation-history').innerHTML = '';
          avaliatorsFinal = [];
          avaliationPoints = {
            coerenceOfContent: 0,
            experimentalismOfProduct: 0,
            productQuality: 0
          };
          divider = 0;
          avaliationsByAvaliator = {};

          for (index = 0; index < data.length; index++) {
            _avaliation2 = data[index];

            if (_typeof(avaliationsByAvaliator[_avaliation2.customerId]) != 'object') {
              avaliationsByAvaliator[_avaliation2.customerId] = [];
            }

            avaliationsByAvaliator[_avaliation2.customerId].push(_avaliation2);
          }

          _context3.t0 = regeneratorRuntime.keys(avaliationsByAvaliator);

        case 8:
          if ((_context3.t1 = _context3.t0()).done) {
            _context3.next = 18;
            break;
          }

          key = _context3.t1.value;

          if (!Object.hasOwnProperty.call(avaliationsByAvaliator, key)) {
            _context3.next = 16;
            break;
          }

          avaliator = avaliationsByAvaliator[key];
          _context3.next = 14;
          return regeneratorRuntime.awrap(avaliator.reduce(function (a, b) {
            return new Date(a.date) > new Date(b.date) ? a : b;
          }));

        case 14:
          avaliation = _context3.sent;

          if (typeof avaliation.defaultWorkOf == 'string' && avaliation.defaultWorkOf.length > 0 && typef == 'undefined') {
            avaliatorStatus = {
              accept: 'Aceito',
              alterationNeed: 'Alterações solicitadas',
              refused: 'Recusado'
            };
            typeOfRadio = {
              yes: 'Sim',
              not: 'Não'
            };
            document.getElementById('cordinator-avaliation-history').innerHTML += "\n                    <div class=\"list-group-item list-group-item-action flex-column align-items-start\">\n                        <div class=\"d-flex w-100 justify-content-between\">\n                            <h5 class=\"mb-1\"><i class=\"fad fa-clipboard-list-check\"></i> ".concat(avaliation.customer.name, "</h5>\n                            <small class=\"text-muted\">").concat(new Date(avaliation.date).toLocaleString(), "</small>\n                        </div>\n                        <hr>\n                        <small class=\"mb-1\">\n                            <strong>1 - O texto est\xE1 no modelo-padr\xE3o do congresso?</strong> ").concat(typeOfRadio[avaliation.nationalWorks[0]], "<br>\n                            <strong>2 - O texto tem entre 10 e 15 p\xE1ginas?</strong> ").concat(typeOfRadio[avaliation.nationalWorks[1]], "<br>\n                            <strong>3 - O texto apresenta, no in\xEDcio, t\xEDtulo, nome do(s) autor(es), institui\xE7\xE3o a que pertence(m), um resumo de at\xE9 10 linhas e palavra-chave?</strong> ").concat(typeOfRadio[avaliation.nationalWorks[2]], "<br>\n                            <strong>4 - O texto apresenta a primeira nota de rodap\xE9 de acordo com a forma\xE7\xE3o exigida no padr\xE3o do congresso?</strong> ").concat(typeOfRadio[avaliation.nationalWorks[3]], "<br>\n                            <strong>5 - Ao final do texto \xE9 apresentada a bibliografia de refer\xEAncia?</strong> ").concat(typeOfRadio[avaliation.nationalWorks[4]], "<br>\n                            <strong>6 - O texto est\xE1 rigorosamente dentro das exig\xEAncias acad\xEAmicas: solidez te\xF3rica, relev\xE2ncia do tema, corre\xE7\xE3o contextual e normas t\xE9cnicas ABNT?</strong> ").concat(typeOfRadio[avaliation.nationalWorks[5]], "<br>\n                            <strong>7 - O texto apresenta contribui\xE7\xE3o resultante de pesquisa cient\xEDfica?</strong> ").concat(typeOfRadio[avaliation.nationalWorks[6]], "<br>\n                            <strong>8 - O texto \xE9 pertinente \xE0 emenda do GP?</strong> ").concat(typeOfRadio[avaliation.nationalWorks[7]], "<br>\n                            <strong>9 - Os objetivos do texto est\xE3o bem definidos?</strong> ").concat(typeOfRadio[avaliation.nationalWorks[8]], "<br>\n                            <strong>10 - O texto apresenta fundamenta\xE7\xE3o te\xF3rica adequada e atualizada?</strong> ").concat(typeOfRadio[avaliation.nationalWorks[9]], "<br>\n                            <strong>11 - O texto est\xE1 metodicamente organizado?</strong> ").concat(typeOfRadio[avaliation.nationalWorks[10]], "<br>\n                        </small>\n                        <hr>\n                            <strong>O trabalho foi:</strong> ").concat(avaliatorStatus[avaliation.defaultWorkOf], "\n                        <hr>\n                        <small class=\"text-muted text-bold\">Observa\xE7\xF5es:</small>\n                        <div class=\"container\">\n                            ").concat(avaliation.descriptionAvaliationWorkAvaliator, "\n                        </div>\n                    </div>\n                ");
          } else {
            avaliationPoints.coerenceOfContent += +avaliation.coerenceOfContent;
            avaliationPoints.experimentalismOfProduct += +avaliation.experimentalismOfProduct;
            avaliationPoints.productQuality += +avaliation.productQuality;
            divider++;
            avaliatorsFinal.push(avaliation);
            document.getElementById('cordinator-avaliation-history').innerHTML += "\n                    <div class=\"list-group-item list-group-item-action flex-column align-items-start\">\n                        <div class=\"d-flex w-100 justify-content-between\">\n                            <h5 class=\"mb-1\"><i class=\"fad fa-clipboard-list-check\"></i> ".concat(avaliation.customer.name, "</h5>\n                            <small class=\"text-muted\">").concat(new Date(avaliation.date).toLocaleString(), "</small>\n                        </div>\n                        <hr>\n                        <small class=\"mb-1\">\n                            <strong>O experimentalismo do produto:</strong> ").concat(avaliation.experimentalismOfProduct, "<br>\n                            <strong>A qualidade t\xE9cnica do produto:</strong> ").concat(avaliation.productQuality, "<br>\n                            <strong>A consist\xEAncia te\xF3rica e coer\xEAncia do conte\xFAdo inserido no formul\xE1rio-padr\xE3o com o respectivo produto:</strong> ").concat(avaliation.coerenceOfContent, "<br>\n                        </small>\n                        <hr>\n                        <small class=\"text-muted text-bold\">Observa\xE7\xF5es:</small>\n                        <div class=\"container\">\n                            ").concat(avaliation.descriptionAvaliationWorkAvaliator, "\n                        </div>\n                    </div>\n                ");
          }

        case 16:
          _context3.next = 8;
          break;

        case 18:
          for (_key in avaliationPoints) {
            if (Object.hasOwnProperty.call(avaliationPoints, _key)) {
              avaliationPoints[_key] = avaliationPoints[_key] / divider;
            }
          }

          return _context3.abrupt("return", {
            notes: avaliationPoints,
            avalations: avaliatorsFinal
          });

        case 20:
        case "end":
          return _context3.stop();
      }
    }
  });
};

app.loadWorkCordinatorDataModal = function _callee4(modalId, workId) {
  var index, oWork, _oWork, el, idx, aAvalations, categoryAndModule, _index16, historyVid, formName;

  return regeneratorRuntime.async(function _callee4$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          index = 0;

        case 1:
          if (!(index < app.lists.workCordinationList.length)) {
            _context4.next = 41;
            break;
          }

          oWork = app.lists.workCordinationList[index];

          if (!(oWork.id == workId)) {
            _context4.next = 38;
            break;
          }

          _oWork = app.lists.workCordinationList[index];
          document.getElementById('cordinator-avaliation-history').innerHTML = 'Este trabalho ainda não possui avaliações.';
          el = document.getElementsByName("defaultCordinatorWorkOf");

          for (idx = 0; idx < el.length; idx++) {
            el[idx].checked = false;
          }

          if (!(_typeof(_oWork.avaliations) == 'object' && _oWork.avaliations instanceof Array && _oWork.avaliations.length > 0)) {
            _context4.next = 15;
            break;
          }

          _context4.next = 11;
          return regeneratorRuntime.awrap(app.getReferenceAvalitionForCordinator(_oWork.avaliations, _typeof(_oWork.oldCongressId)));

        case 11:
          aAvalations = _context4.sent;
          document.getElementById('experimentalismOfProductCordinator').value = aAvalations.notes.experimentalismOfProduct;
          document.getElementById('productQualityCordinator').value = aAvalations.notes.productQuality;
          document.getElementById('coerenceOfContentCordinator').value = aAvalations.notes.coerenceOfContent;

        case 15:
          categoryAndModule = _oWork.category.split(' ');
          document.getElementById('discipline-2Cordinator').innerHTML = _oWork.submitionWork.discipline;
          document.getElementById('currentYearCordinator').innerHTML = _oWork.submitionWork.lectiveYear == 'yes' ? 'Sim' : 'Não';
          document.getElementById('congressIdWorkCordinator').value = _oWork.congressId;
          document.getElementById('workIdWorkCordinator').value = _oWork.id;
          document.getElementById('workNameCordinator').innerHTML = _oWork.workName;
          document.getElementById('iesWorkCordinator').innerHTML = _oWork.ies;
          document.getElementById('leaderNameCordinator').innerHTML = _oWork.leaderName;
          document.getElementById('responsableTeatcherWorkCordinator').innerHTML = _oWork.submitionWork.responsableTeatcher;
          document.getElementById('coAuthorsWorkCordinator').innerHTML = _oWork.submitionWork.coAuthors;
          document.getElementById('studyObjectWorkCordinator').innerHTML = _oWork.submitionWork.studyObject;
          document.getElementById('researchRealizedWorkCordinator').innerHTML = _oWork.submitionWork.researchRealized;
          document.getElementById('productionDescriptionWorkCordinator').innerHTML = _oWork.submitionWork.productionDescription;

          if (typeof _oWork.submitionWork.linkCloudWork == 'string' && _oWork.submitionWork.linkCloudWork.length > 0) {
            document.getElementById('buttonDownloadMaterialWorkCordinator').style.display = 'block';
            document.getElementById('buttonDownloadMaterialWorkCordinator').href = _oWork.submitionWork.linkCloudWork;
          } else {
            document.getElementById('buttonDownloadMaterialWorkCordinator').style.display = 'none';
          }

          document.getElementById('category-1Cordinator').innerHTML = categoryAndModule[0];
          document.getElementById('category-2Cordinator').innerHTML = "".concat(categoryAndModule[0], " ").concat(categoryAndModule[1]); // console.log('Cordinator',app.lists.congresses[oWork.congressId].type)

          if (_typeof(app.lists.congresses[_oWork.congressId]) == 'object' && typeof app.lists.congresses[_oWork.congressId].type == 'string' && app.lists.congresses[_oWork.congressId].type == 'nacional' && typeof _oWork.oldCongressId == 'undefined') {
            if (typeof _oWork.descriptionAvaliationWorkCordinator == "string" && _oWork.descriptionAvaliationWorkCordinator.length > 0) {
              document.querySelector('#descriptionAvaliationWorkCordinator').value = _oWork.descriptionAvaliationWorkCordinator;
            } else {
              document.querySelector('#descriptionAvaliationWorkCordinator').value = '';
            }

            document.getElementById('videos-from-work-cordinator').innerHTML = '';

            if (typeof _oWork.correctionVideo == 'string' && _oWork.correctionVideo.length > 0) {
              document.getElementById('videos-from-work-cordinator').innerHTML += "\n                        <hr>\n                        <a href=\"".concat(_oWork.correctionVideo, "\" class=\"btn btn-primary\" target=\"_blank\">\n                            <i class=\"fad fa-download\"></i> Link de Corre\xE7\xE3o\n                        </a>");
            }

            if (_typeof(_oWork.correctionHistories) == 'object' && _oWork.correctionHistories.length > 0) {
              _oWork.correctionHistories.reverse();

              for (_index16 = 0; _index16 < _oWork.correctionHistories.length; _index16++) {
                historyVid = _oWork.correctionHistories[_index16];
                document.getElementById('videos-from-work-cordinator').innerHTML += "\n                            <hr>\n                            <a href=\"".concat(historyVid, "\" class=\"btn btn-primary\" target=\"_blank\">\n                                <i class=\"fad fa-download\"></i> Link de Corre\xE7\xE3o\n                            </a>");
              }
            }

            document.getElementById('submitButtonEnding').innerText = 'Salvar e Notificar';
            document.querySelectorAll('.cordinator-regional').forEach(function (e) {
              e.style.display = 'none';
            });
            document.querySelectorAll('.cordinator-nacional').forEach(function (e) {
              e.style.display = 'block';
            });
            document.getElementById("autorNameId").innerText = 'Autor(a):'; // document.querySelectorAll('.avaliator-regional')
          } else {
            document.getElementById("autorNameId").innerText = 'Estudante Lider:';
            document.getElementById('submitButtonEnding').innerText = 'Finaliar Trabalho';
            document.querySelectorAll('.cordinator-nacional').forEach(function (e) {
              e.style.display = 'none';
            });
            document.querySelectorAll('.cordinator-regional').forEach(function (e) {
              e.style.display = 'block';
            }); // document.querySelectorAll('.avaliator-resumes').style.display = 'none';
          }

          if (document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners')) document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners').forEach(function (e) {
            e.remove();
          });
          ClassicEditor.create(document.querySelector('#descriptionAvaliationWorkCordinator'), {
            removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
            cloudServices: {
              tokenUrl: 'https://79675.cke-cs.com/token/dev/05f801120cf7cccdb540ea25a7068d7d1c8108b02232eb1bfec6b89be13c',
              uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
            }
          }).then(function (editor) {
            app.ckeditor.descriptionAvaliationWorkCordinator = editor;
          })["catch"](function (error) {
            console.error(error);
          });
          formName = modalId.replace('Modal', '');
          $("#".concat(formName, " .formError")).hide();
          $("#".concat(formName, " .formSuccess")).hide();
          $("#".concat(modalId)).modal('show');

        case 38:
          index++;
          _context4.next = 1;
          break;

        case 41:
        case "end":
          return _context4.stop();
      }
    }
  });
};

app.loadUserDetailDataModal = function _callee5(modalId, workId) {
  var index, oWork, _oWork2, categoryAndModule, formName;

  return regeneratorRuntime.async(function _callee5$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          for (index = 0; index < app.lists.workCordinationList.length; index++) {
            oWork = app.lists.workCordinationList[index];

            if (oWork.id == workId) {
              _oWork2 = app.lists.workCordinationList[index];
              categoryAndModule = _oWork2.category.split(' ');
              console.log(categoryAndModule, _oWork2);
              document.getElementById('discipline-2Detail').innerHTML = _oWork2.submitionWork.discipline;
              document.getElementById('currentYearDetail').innerHTML = _oWork2.submitionWork.lectiveYear == 'yes' ? 'Sim' : 'Não'; // document.getElementById('congressIdWorkDetail').value = oWork.congressId;
              // document.getElementById('workIdWorkDetail').value = oWork.id;

              document.getElementById('workNameDetail').innerHTML = _oWork2.workName;
              document.getElementById('iesWorkDetail').innerHTML = _oWork2.ies;
              document.getElementById('leaderNameDetail').innerHTML = _oWork2.leaderName;
              document.getElementById('responsableTeatcherWorkDetail').innerHTML = _oWork2.submitionWork.responsableTeatcher;
              document.getElementById('coAuthorsWorkDetail').innerHTML = _oWork2.submitionWork.coAuthors; // document.getElementById('studyObjectWorkDetail').innerHTML = oWork.submitionWork.studyObject;
              // document.getElementById('researchRealizedWorkDetail').innerHTML = oWork.submitionWork.researchRealized;
              // document.getElementById('productionDescriptionWorkDetail').innerHTML = oWork.submitionWork.productionDescription;

              if (typeof _oWork2.submitionWork.linkCloudWork == 'string' && _oWork2.submitionWork.linkCloudWork.length > 0) {
                document.getElementById('buttonDownloadMaterialWorkDetail').href = _oWork2.submitionWork.linkCloudWork;
              } else {
                document.getElementById('buttonDownloadMaterialWorkDetail').style.display = 'none';
              }

              document.getElementById('category-1Detail').innerHTML = categoryAndModule[0];
              document.getElementById('category-2Detail').innerHTML = "".concat(categoryAndModule[0], " ").concat(categoryAndModule[1]);
              formName = modalId.replace('Modal', '');
              $("#".concat(formName, " .formError")).hide();
              $("#".concat(formName, " .formSuccess")).hide();
              $("#".concat(modalId)).modal('show');
            }
          }

        case 1:
        case "end":
          return _context5.stop();
      }
    }
  });
};

app.showAvaliators = function (congressId, workId) {
  $('.page-hover').fadeIn();
  $('.page-hover').css('zIndex', '99999999');

  var _loop2 = function _loop2(index) {
    var work = app.lists.workCordinationList[index];

    if (work.id == workId) {
      token = typeof app.user.token == 'string' ? app.user.token : app.getData('token');

      if (token) {
        var pld = {
          'host': window.location.host,
          'access_token': token,
          'congressId': congressId,
          'category': work.category
        };
        app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Congress/Work/Cordinator/List/Avaliators', 'POST', undefined, pld, function (statusCode, rspObj) {
          if (statusCode == 200 && _typeof(rspObj) == 'object' && rspObj) {
            $('.page-hover').fadeOut();
            $('.page-hover').css('zIndex', '2');
            var showAvaliatorsHtml = '';

            if (_typeof(work.avaliators) == 'object' && work.avaliators instanceof Array && work.avaliators.length > 0) {
              showAvaliatorsHtml += '<ul class="list-group">';

              for (var _index17 = 0; _index17 < rspObj.length; _index17++) {
                var oAvaliator = rspObj[_index17];

                if (_typeof(oAvaliator) == 'object' && _typeof(oAvaliator.customer) == 'object') {
                  if (work.avaliators.indexOf(oAvaliator.customer.id) > -1) {
                    showAvaliatorsHtml += "<li class=\"list-group-item\"><strong>".concat(oAvaliator.customer.name, "</strong></li>");
                  }
                }
              }

              showAvaliatorsHtml += '</ul>';
              Swal.fire({
                title: 'Avaliadores',
                icon: '',
                html: showAvaliatorsHtml,
                showCloseButton: false,
                showCancelButton: false,
                showConfirmButton: false,
                focusConfirm: false
              });
            } else {
              showAvaliatorsHtml = 'No momento este trabalho não possui avaliadores.';
              Swal.fire({
                icon: 'warning',
                html: showAvaliatorsHtml,
                showCloseButton: false,
                showCancelButton: false,
                showConfirmButton: false,
                focusConfirm: false
              });
            }
          }
        });
      }
    }
  };

  for (var index = 0; index < app.lists.workCordinationList.length; index++) {
    var token;

    _loop2(index);
  }
};

app.compare = function (a, b) {
  if (_typeof(a.customer) == 'object' && typeof a.customer.name == 'string' && _typeof(b.customer) == 'object' && typeof b.customer.name == 'string') {
    if (a.customer.name < b.customer.name) {
      return -1;
    }

    if (a.customer.name > b.customer.name) {
      return 1;
    }

    return 0;
  }

  return 0;
};

app.loadCordinatorSetAvaliation = function (congressId, workId) {
  $('.page-hover').fadeIn();
  $('.page-hover').css('zIndex', '99999999');

  for (var index = 0; index < app.lists.workCordinationList.length; index++) {
    var work = app.lists.workCordinationList[index];

    if (work.id == workId) {
      var token = typeof app.user.token == 'string' ? app.user.token : app.getData('token');

      if (token) {
        var pld = {
          'host': window.location.host,
          'access_token': token,
          'congressId': congressId,
          'category': work.category
        };
        app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Congress/Work/Cordinator/List/Avaliators', 'POST', undefined, pld, function (statusCode, rspObj) {
          if (statusCode == 200 && rspObj) {
            var dataHtml = ''; //console.log('STARTING', rspObj)

            rspObj.sort(app.compare);

            for (var _index18 = 1; _index18 <= 3; _index18++) {
              dataHtml += "<select name=\"avalWork-".concat(_index18, "\" id=\"avalWork-").concat(_index18, "\" class=\"custom-select mb-3\" >");
              dataHtml += "<option selected>Selecione...</option>";

              for (var _index19 = 0; _index19 < rspObj.length; _index19++) {
                var avaliator = rspObj[_index19];

                if (_typeof(avaliator.customer) == 'object' && typeof avaliator.customer.name == 'string') {
                  dataHtml += "<option value=\"".concat(avaliator.id, "\">").concat(avaliator.customer.name, " - ").concat(avaliator.ies, "</option>");
                }
              }

              dataHtml += "</select>";
            }

            $('.page-hover').fadeOut();
            Swal.fire({
              title: '<strong>Adicionar Avaliador</strong>',
              icon: 'info',
              html: "\n                            <div class=\"col-lg-12\">\n                                <label for=\"avalWork\">Selecione o convidado</label>\n                                ".concat(dataHtml, "\n                            </div>\n                            "),
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false,
              confirmButtonText: '<i class="fa fa-thumbs-up"></i> Adicionar',
              confirmButtonAriaLabel: 'Thumbs up, add!',
              cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
              cancelButtonAriaLabel: 'Thumbs down'
            }).then(function (e) {
              if (e.value) {
                var payloadObj = {
                  'host': window.location.host,
                  'access_token': token,
                  'congressId': congressId,
                  'avaliatorId1': $("#avalWork-1 option:selected").val(),
                  'avaliatorId2': $("#avalWork-2 option:selected").val(),
                  'avaliatorId3': $("#avalWork-3 option:selected").val(),
                  'workId': workId
                };
                $('.page-hover').fadeIn();
                app.client.request(undefined, endpoint + '/api/Admins/Institution/Congress/Work/Aval', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
                  if (statusCode == 200) {
                    $('.page-hover').fadeOut();
                    $('.page-hover').css('zIndex', '2');
                    Swal.fire('Uhu', 'Avaliador adicionado com sucesso.', 'success');
                  }
                });
              }
            });
          }
        });
      }
    }
  }
};

app.loadWorksForCordination = function _callee6(id) {
  var index, oCongress, categs, dataSet, dateCert, today, _index20, element, workCateg, media, avaliations, avaliationsByAvaliator, _index21, _avaliation3, key, avaliator, avaliation, categoryAndModule, child, nationalWorkStatus, localTable, tble;

  return regeneratorRuntime.async(function _callee6$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          $('#workAvaliationModal').modal('show');
          app.lists.workCordinationList = [];
          index = 0;

        case 3:
          if (!(index < app.institution.congresses.length)) {
            _context6.next = 62;
            break;
          }

          oCongress = app.institution.congresses[index];

          if (!(oCongress.id == id)) {
            _context6.next = 59;
            break;
          }

          categs = app.user.cordinatored[oCongress.id].categs;
          document.getElementById('sendMessageAvaliatorButton').setAttribute('congress', "".concat(oCongress.id)); // Show each created check as a new row in the table

          dataSet = [];
          document.getElementById('sendAvaliationButton').setAttribute('congress', oCongress.id);

          if (typeof oCongress.type == 'string' && oCongress.type == 'nacional') {
            document.querySelectorAll('.cordinator-list-regional').forEach(function (e) {
              e.style.display = 'none';
            });
            document.querySelectorAll('.cordinator-list-nacional').forEach(function (e) {
              e.style.display = 'inline-block';
            });
            document.getElementById("autorNameTh").innerText = "Autor"; // document.querySelectorAll('.avaliator-regional')
          } else {
            document.getElementById("autorNameTh").innerText = "Lider";
            document.querySelectorAll('.cordinator-list-nacional').forEach(function (e) {
              e.style.display = 'none';
            });
            document.querySelectorAll('.cordinator-list-regional').forEach(function (e) {
              e.style.display = 'inline-block';
            }); // document.querySelectorAll('.avaliator-resumes').style.display = 'none';
          }

          dateCert = new Date(oCongress.dateCert);
          today = new Date();

          if (today > dateCert) {
            document.querySelector('.lo-footer-congressman').style.display = 'none';
            document.querySelector('.lo-footer-avaliator').style.display = 'none';
            document.querySelector('.lo-footer-coordinator').style.display = 'block';
            document.getElementById('certificateCordinator').setAttribute('congress', oCongress.id);
          }

          _index20 = 0;

        case 15:
          if (!(_index20 < oCongress.works.length)) {
            _context6.next = 52;
            break;
          }

          element = oCongress.works[_index20];
          workCateg = element.category.split(' ');

          if (!(_typeof(element.submitionWork) == 'object' && element.submitionWork && typeof element.submitionWork.workId == 'string' && (categs.indexOf("".concat(workCateg[0], " ").concat(workCateg[1])) > -1 || categs.indexOf(element.category) > -1))) {
            _context6.next = 49;
            break;
          }

          media = false;

          if (typeof element.coerenceOfContent == 'number' && typeof element.experimentalismOfProduct == 'number' && typeof element.productQuality == 'number') {
            media = (element.coerenceOfContent + element.experimentalismOfProduct + element.productQuality) / 3;
          }

          avaliations = 0;

          if (!(_typeof(element.avaliations) == 'object' && element.avaliations instanceof Array && element.avaliations.length > 0)) {
            _context6.next = 36;
            break;
          }

          avaliationsByAvaliator = {};

          for (_index21 = 0; _index21 < element.avaliations.length; _index21++) {
            _avaliation3 = element.avaliations[_index21];

            if (_typeof(avaliationsByAvaliator[_avaliation3.customerId]) != 'object') {
              avaliationsByAvaliator[_avaliation3.customerId] = [];
            }

            avaliationsByAvaliator[_avaliation3.customerId].push(_avaliation3);
          }

          _context6.t0 = regeneratorRuntime.keys(avaliationsByAvaliator);

        case 26:
          if ((_context6.t1 = _context6.t0()).done) {
            _context6.next = 36;
            break;
          }

          key = _context6.t1.value;

          if (!Object.hasOwnProperty.call(avaliationsByAvaliator, key)) {
            _context6.next = 34;
            break;
          }

          avaliator = avaliationsByAvaliator[key];
          _context6.next = 32;
          return regeneratorRuntime.awrap(avaliator.reduce(function (a, b) {
            return new Date(a.date) > new Date(b.date) ? a : b;
          }));

        case 32:
          avaliation = _context6.sent;
          avaliations++;

        case 34:
          _context6.next = 26;
          break;

        case 36:
          categoryAndModule = element.category.split(' ');
          child = [];
          child[0] = app.lists.iesByName[element.ies] && app.lists.iesByName[element.ies].region ? app.lists.iesByName[element.ies].region : 'Não preenchido';
          child[1] = "".concat(categoryAndModule[0], " ").concat(categoryAndModule[1]);
          child[2] = element.workName.substring(0, 22) || '';
          child[3] = element.leaderName.substring(0, 18) || '';
          child[4] = app.lists.iesByName[element.ies] && app.lists.iesByName[element.ies].sigla ? app.lists.iesByName[element.ies].sigla : 'Não preenchido';
          child[5] = _typeof(element.avaliations) == 'object' && avaliations > 0 ? "<span class=\"text-success\"><i class=\"fad fa-check-circle\"></i> Avalia\xE7\xF5es <span class=\"badge badge-success\">".concat(avaliations, "</span></span>") : '<span class="text-warning"><i class="fad fa-exclamation-triangle"></i> Não avaliado</span>';

          if (typeof oCongress.type == 'string' && oCongress.type == 'nacional' && typeof element.oldCongressId == 'undefined') {
            nationalWorkStatus = {
              accept: '<span class="text-success"><i class="fad fa-file-check"></i> Aceito</span>',
              alterationNeed: '<span class="text-dark"><i class="fad fa-file-signature"></i> Alterações solicitadas</span>',
              refused: '<span class="text-danger"><i class="fas fa-file"></i> Recusado</span>'
            };

            if (typeof element.correctionVideo == 'string' && element.correctionVideo.length > 0 && typeof element.defaultCordinatorWorkOf == 'string' && element.defaultCordinatorWorkOf == 'alterationNeed') {
              child[6] = '<span class="text-primary"><i class="fas fa-sync"></i> Reavaliar</span>';
            } else {
              child[6] = typeof element.defaultCordinatorWorkOf == 'string' && element.defaultCordinatorWorkOf.length > 0 ? nationalWorkStatus[element.defaultCordinatorWorkOf] : '<span class="text-warning"><i class="fad fa-file-times"></i> Não validado</span>';
            }
          } else {
            child[6] = typeof element.status == 'string' && element.status == 'declassified' ? '<span class="text-danger"><i class="fad fa-file-times"></i> Desclassificado</span>' : media ? '<span class="text-success"><i class="fad fa-file-check"></i> Validado</span>' : '<span class="text-warning"><i class="fad fa-file-times"></i> Não validado</span>';
          }

          child[7] = typeof element.average == 'string' || typeof element.average == 'number' ? element.average.toFixed(2) : typeof oCongress.type == 'string' && oCongress.type == 'nacional' && media ? media.toFixed(2) : '-';
          child[8] = "\n                        <button type=\"button\" class=\"btn btn-success\" onClick=\"app.loadCordinatorSetAvaliation('".concat(oCongress.id, "','").concat(element.id, "')\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Atribuir Avaliador ao Trabalho\">\n                            <i class=\"fad fa-clipboard-list-check\"></i>\n                        </button>\n                        <button type=\"button\" class=\"btn btn-warning\" onClick=\"app.showAvaliators('").concat(oCongress.id, "','").concat(element.id, "')\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Visualizar Avaliadores do Trabalho\">\n                            <i class=\"fas fa-list-alt\"></i>\n                        </button>\n                        <button type=\"button\" class=\"btn btn-primary\" onClick=\"app.loadWorkCordinatorDataModal('workCordinationCordinatorModal', '").concat(element.id, "')\"  data-toggle=\"tooltip\" data-placement=\"left\" title=\"Validar Avalia\xE7\xF5es\">\n                            <i class=\"fas fa-clipboard-check\"></i>\n                        </button>\n                        ").concat(typeof oCongress.type == 'string' && oCongress.type == 'nacional' ? "<button type=\"button\" class=\"open-modal btn btn-info\" onClick=\"app.sendMessageAvaliatorsNacional('workAvaliationModal','".concat(element.id, "','").concat(oCongress.id, "')\"  data-toggle=\"tooltip\" data-placement=\"left\" title=\"Comunicar Avaliadores\"><i class=\"fad fa-envelope\"></i></button>") : '', "\n                        ").concat(typeof oCongress.type == 'string' && oCongress.type == 'regional' ? "<button type=\"button\" class=\"btn btn-danger\" onClick=\"app.inactiveWork('workAvaliationModal', '".concat(element.id, "')\"  data-toggle=\"tooltip\" data-placement=\"left\" title=\"Desclassificar Trabalho\"><i class=\"fad fa-folder-times\"></i></button>") : '', "\n                        ").concat(typeof oCongress.type == 'string' && oCongress.type == 'nacional' && typeof element.presentationVideo == 'string' && element.presentationVideo.length > 0 ? "<a class=\"btn btn-dark\" href=\"".concat(element.presentationVideo, "\" target=\"_blank\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Video de apresenta\xE7\xE3o\"><i class=\"fas fa-play-circle\"></i></a> ") : '', " \n                        ").concat(typeof element.result == 'string' && element.result == 'approved' && typeof element.presentationVideo == 'string' && element.presentationVideo.length > 0 ? "<a class=\"btn btn-dark\" href=\"".concat(element.presentationVideo, "\" target=\"_blank\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Video de apresenta\xE7\xE3o\"><i class=\"fas fa-play-circle\"></i></a> ") : '', " \n                        ");
          app.lists.workCordinationList.push(element);
          dataSet.push(child);

        case 49:
          _index20++;
          _context6.next = 15;
          break;

        case 52:
          if ($.fn.dataTable.isDataTable('#tableWorks')) {
            localTable = $('#tableWorks').DataTable();
            localTable.destroy();
          }

          $('#tableWorks thead th').each(function () {
            var title = $(this).text();

            if (title != "Ações") {
              $(this).html("\n                        <label style=\"display: none\">".concat(title.trim(), "</label>\n                        <input type=\"text\" class=\"form-control\" placeholder=\"").concat(title.trim(), "\">\n                    "));
            }
          });
          tble = $('#tableWorks').DataTable({
            scrollX: true,
            lengthChange: true,
            buttons: ['excel', 'pdf'],
            data: dataSet,
            columnDefs: [],
            order: [[1, "asc"]],
            language: {
              lengthMenu: "Exibindo _MENU_  ",
              zeroRecords: "Nenhum registro disponível.",
              info: "Exibindo pagina _PAGE_ de _PAGES_",
              search: "Filtro _INPUT_",
              paginate: {
                "next": ">",
                "previous": "<"
              }
            }
          });
          tble.columns().every(function () {
            var that = this;
            $('input', this.header()).on('keyup change', function () {
              if (that.search() !== this.value) {
                that.search(this.value).draw();
              }
            });
          });
          setTimeout(function () {
            window.dispatchEvent(new Event('resize'));
            $('[data-toggle="tooltip"]').tooltip();
          }, 300);
          $(document).on('show.bs.modal', '.modal', function (event) {
            var zIndex = 1040 + 10 * $('.modal:visible').length;
            $(this).css('z-index', zIndex);
            setTimeout(function () {
              $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
          });
          $('.page-hover').fadeOut();

        case 59:
          index++;
          _context6.next = 3;
          break;

        case 62:
        case "end":
          return _context6.stop();
      }
    }
  });
};

app.getWorkById = function (workId) {
  for (var index = 0; index < app.lists.works.length; index++) {
    var oWork = app.lists.works[index];

    if (oWork.id == workId) {
      return oWork;
    }
  }
};

app.submitPublicomLink = function (congressId, bookId) {
  $("#bookListModal").modal('hide');
  var token = typeof app.user.token == 'string' ? app.user.token : app.getData('token');
  Swal.fire({
    title: '<strong>Enviar link de apresentação</strong>',
    icon: 'info',
    html: "\n        <div class=\"col-lg-12\">\n            <label for=\"bookLinkUrl\">Preencher com url do livro:</label>\n            <input class=\"form-control\" id=\"bookLinkUrl\" />\n        </div>\n        ",
    showCloseButton: true,
    showCancelButton: true,
    focusConfirm: false,
    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Enviar',
    confirmButtonAriaLabel: 'Thumbs up, send!',
    cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
    cancelButtonAriaLabel: 'Thumbs down'
  }).then(function (e) {
    if (e.value) {
      var payloadObj = {
        'host': window.location.host,
        'access_token': token,
        congressId: congressId,
        'bookLinkUrl': $("#bookLinkUrl").val(),
        bookId: bookId
      };
      $('.page-hover').fadeIn();
      app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Congress/Publicom/Link', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
        if (statusCode == 200) {
          $('.page-hover').fadeOut();
          Swal.fire('Uhu', 'Link enviado com sucesso.', 'success').then(function (e) {
            if (e.value) {
              window.location.reload();
            }
          });
        }
      });
    } else {
      $("#bookListModal").modal('show');
    }
  });
};

app.workSuccessSubmtion = function (workId, congressId) {
  var token = typeof app.user.token == 'string' ? app.user.token : app.getData('token');
  Swal.fire({
    title: '<strong>Enviar vídeo de apresentação</strong>',
    icon: 'info',
    html: "\n        <div class=\"col-lg-12\">\n            <label for=\"videoUrl\">Preencher com url do v\xEDdeo de apresenta\xE7\xE3o do trabalho:</label>\n            <input class=\"form-control\" id=\"videoUrl\" />\n        </div>\n        ",
    showCloseButton: true,
    showCancelButton: true,
    focusConfirm: false,
    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Enviar',
    confirmButtonAriaLabel: 'Thumbs up, send!',
    cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
    cancelButtonAriaLabel: 'Thumbs down'
  }).then(function (e) {
    if (e.value) {
      var payloadObj = {
        'host': window.location.host,
        'access_token': token,
        'congressId': congressId,
        'videoUrl': $("#videoUrl").val(),
        'workId': workId
      };
      $('.page-hover').fadeIn();
      app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Congress/Work/VideoUrl', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
        if (statusCode == 200) {
          $('.page-hover').fadeOut();
          Swal.fire('Uhu', 'Video enviado com sucesso.', 'success').then(function (e) {
            if (e.value) {
              window.location.reload();
            }
          });
        }
      });
    }
  });
};

app.workAlterationNeedSubmtion = function (workId, congressId) {
  var token = typeof app.user.token == 'string' ? app.user.token : app.getData('token');
  Swal.fire({
    title: '<strong>Revisão do artigo</strong>',
    icon: 'info',
    html: "\n        <div class=\"col-lg-12\">\n            <label for=\"videoUrl\">Cole aqui o link para a vers\xE3o corrigida do artigo:</label>\n            <input class=\"form-control\" id=\"videoUrl\" />\n        </div>\n        ",
    showCloseButton: true,
    showCancelButton: true,
    focusConfirm: false,
    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Enviar',
    confirmButtonAriaLabel: 'Thumbs up, send!',
    cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
    cancelButtonAriaLabel: 'Thumbs down'
  }).then(function (e) {
    if (e.value) {
      var payloadObj = {
        'host': window.location.host,
        'access_token': token,
        'congressId': congressId,
        'videoUrl': $("#videoUrl").val(),
        'workId': workId
      };
      $('.page-hover').fadeIn();
      app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Congress/Work/Correction', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
        if (statusCode == 200) {
          $('.page-hover').fadeOut();
          Swal.fire('Uhu', 'Correção enviada com sucesso.', 'success').then(function (e) {
            if (e.value) {
              window.location.reload();
            }
          });
        }
      });
    }
  });
};

app.loadWorkStatus = function (workId) {
  var work = app.getWorkById(workId);
  console.log(work);
  var indication;

  if (typeof work.congressType == 'string' && work.congressType == 'nacional') {
    indication = '';
  } else {
    indication = "\n            <li class=\"list-group-item d-flex flex-column-reverse text-left\">\n                <div>\n                    Indica\xE7\xE3o do trabalho\n                </div>\n                <div class=\"date-on-history bg-primary\">\n                    <span class=\"badge badge-primary badge-pill\">".concat(new Date(work.date).toLocaleString(), "</span>\n                </div>\n            </li>\n        ");
  }

  var submition = "\n        <li class=\"list-group-item d-flex flex-column-reverse text-left\">\n            <div>\n                Submiss\xE3o do trabalho\n            </div>\n            <div class=\"date-on-history bg-primary\">\n                <span class=\"badge badge-primary badge-pill\">".concat(new Date(work.submitionWork.date).toLocaleString(), "</span>\n            </div>\n        </li>\n    ");
  var avaliation = '';

  if (_typeof(work.avaliations) == 'object' && work.avaliations instanceof Array && work.avaliations.length > 0) {
    avaliation = "\n            <li class=\"list-group-item d-flex flex-column-reverse text-left\">\n                <div>\n                    Iniciando avalia\xE7\xF5es\n                </div>\n                <div class=\"date-on-history bg-primary\">\n                    <span class=\"badge badge-primary badge-pill\">".concat(new Date(work.avaliations[0].date).toLocaleString(), "</span>\n                </div>\n            </li>\n        ");
  }

  var sendVideo = '<br> O prazo de envio do vídeo de apresentação está encerrado.'; //`<br><button class="btn btn-success btn-block mt-3" onclick="app.workSuccessSubmtion('${workId}','${work.congressId}')">Enviar apresentação</button>`

  if (typeof work.presentationVideo == 'string' && work.presentationVideo.length > 0) {
    sendVideo = '<br> Video já enviado.';
  }

  var result = '';

  if (typeof work.congressType == 'string' && work.congressType == 'nacional') {
    var cordinatorDescription = typeof work.descriptionAvaliationWorkCordinator == 'string' && work.descriptionAvaliationWorkCordinator.length > 0 ? work.descriptionAvaliationWorkCordinator.replaceAll('<p>', '').replaceAll('</p>', '') : '';

    if (typeof work.defaultCordinatorWorkOf == 'string' && work.defaultCordinatorWorkOf == 'accept') {
      result = "\n                <li class=\"list-group-item d-flex flex-column-reverse text-left\">\n                    <div>\n                        O seu trabalho foi aceito.<br>\n                        <button class=\"btn btn-primary\" onClick=\"app.loadAcceptLetter('".concat(work.congressId, "','").concat(work.id, "')\">Carta de aceite</button>    \n                    </div>\n                    <div class=\"date-on-history bg-primary\">    \n                        <span class=\"badge badge-primary badge-pill\">").concat(new Date(work.correctionDate).toLocaleString(), "</span>\n                    </div>\n                </li>\n            ");
    }

    if (typeof work.defaultCordinatorWorkOf == 'string' && work.defaultCordinatorWorkOf == 'alterationNeed') {
      result = "\n                <li class=\"list-group-item list-group-item-warning d-flex flex-column-reverse text-left\">\n                    <div>\n                        \n                        ".concat(typeof work.correctionVideo == 'string' && work.correctionVideo.length > 0 ? 'Uma correção já foi enviada, aguarde a validação.' : "".concat(cordinatorDescription.trim(), "<br><button class=\"btn btn-primary \" onclick=\"app.workAlterationNeedSubmtion('").concat(workId, "','").concat(work.congressId, "')\"><i class=\"fas fa-cloud-upload-alt\"></i> Atualizar</button>"), "\n                        \n                    </div>\n                    <div class=\"date-on-history bg-primary\">    \n                        <span class=\"badge badge-primary badge-pill\">").concat(new Date(work.correctionDate).toLocaleString(), "</span>\n                    </div>\n                </li>\n            ");
    }

    if (typeof work.defaultCordinatorWorkOf == 'string' && work.defaultCordinatorWorkOf == 'refused') {
      result = "\n                <li class=\"list-group-item d-flex flex-column-reverse text-left\">\n                    <div>\n                        ".concat(cordinatorDescription.trim(), "  \n                    </div>\n                    <div class=\"date-on-history bg-primary\">    \n                        <span class=\"badge badge-primary badge-pill\">").concat(new Date(work.resultDate).toLocaleString(), "</span>\n                    </div>\n                </li>\n            ");
    }
  } else {
    if (typeof work.result == 'string' && work.result == 'approved') {
      result = "\n                <li class=\"list-group-item d-flex flex-column-reverse text-left\">\n                    <div>\n                        Voc\xEA \xE9 um dos finalistas da modalidade. Parab\xE9ns!!!\n                        ".concat(sendVideo, "    \n                    </div>\n                <div class=\"date-on-history bg-primary\">    \n                        <span class=\"badge badge-primary badge-pill\">").concat(new Date(work.resultDate).toLocaleString(), "</span>\n                    </div>\n                </li>\n            ");
    }

    if (typeof work.result == 'string' && work.result == 'reproved') {
      result = "\n                <li class=\"list-group-item d-flex flex-column-reverse text-left\">\n                    <div>\n                        Seu trabalho n\xE3o ficou entre os finalistas da modalidade. A Intercom agradece a sua participa\xE7\xE3o.    \n                    </div>\n                    <div class=\"date-on-history bg-primary\">    \n                        <span class=\"badge badge-primary badge-pill\">".concat(new Date(work.resultDate).toLocaleString(), "</span>\n                    </div>\n                </li>\n            ");
    }
  }

  var dataHtml = "<p>Aqui voc\xEA pode obter informa\xE7\xF5es do seu trabalho.</p>\n        <ul class=\"list-group\">\n            ".concat(indication, "\n            ").concat(submition, "\n            ").concat(avaliation, "\n            ").concat(result, "\n        </ul>\n    ");

  if (typeof work.status == 'string' && work.status == "declassified") {
    dataHtml = "<p>Aqui voc\xEA pode obter informa\xE7\xF5es do seu trabalho.</p>\n            <div class=\"list-group\">\n                ".concat(indication, "\n                ").concat(submition, "\n                <div class=\"list-group-item list-group-item-danger list-group-item-action flex-column align-items-start\">\n                    <div class=\"d-flex w-100 justify-content-between\">\n                        <p class=\"mb-1\">\n                            Trabalho desclassificado: ").concat(work.message, "\n                        </p>\n                    </div>\n                </div>\n            </div>\n        ");
  }

  Swal.fire({
    title: '<strong>Detalhes do Trabalho</strong>',
    icon: 'info',
    html: dataHtml,
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText: '<i class="fa fa-thumbs-up"></i>',
    confirmButtonAriaLabel: 'Thumbs up!'
  });
};

app.loadAcceptLetter = function (congressId, workId) {
  var oCongress = app.lists.congresses[congressId];

  var _loop3 = function _loop3(index) {
    var oWork = app.lists.works[index];

    if (oWork.id == workId) {
      console.log(oCongress, oWork);
      document.querySelectorAll('.work-name-content').forEach(function (e) {
        e.innerText = oWork.workName;
      });
      document.querySelectorAll('.autor-coautor-name-content').forEach(function (e) {
        e.innerText = oWork.leaderName + ' ' + oWork.submitionWork.coAuthors;
      });
      document.querySelectorAll('.congress-name-content').forEach(function (e) {
        e.innerText = oCongress.name;
      });
      document.querySelectorAll('.congress-date-content').forEach(function (e) {
        e.innerText = new Date(oWork.correctionDate).toLocaleString();
      });
    }
  };

  for (var index = 0; index < app.lists.works.length; index++) {
    _loop3(index);
  }

  $('#acceptLetterModal').modal('show');
};

app.loadScheduleListt = function (congressId, modalPrefix) {
  $("#".concat(modalPrefix)).modal('show');

  for (var index = 0; index < app.institution.congresses.length; index++) {
    var oCongress = app.institution.congresses[index];

    if (oCongress.id == congressId) {
      // Show each created check as a new row in the table
      var dataStatic = [{
        date: new Date("08/02/2021 16:30").toLocaleString('pt-BR'),
        region: "Todas",
        name: "Abertura + Conferência Manuela d'Ávila",
        desc: "",
        link: "https://www.youtube.com/watch?v=gOHI8qQ9Tvs"
      }, {
        date: new Date("08/02/2021 19:00").toLocaleString('pt-BR'),
        region: "Todas",
        name: "Festa de Premiação Ganhadores Expocom Norte",
        desc: "(com participação da banda Sociedade de Esquina, de Boa Vista/RR)",
        link: "https://www.youtube.com/watch?v=rr98Gchu3FE"
      }, {
        date: new Date("08/03/2021 16:30").toLocaleString('pt-BR'),
        region: "CENTRO-OESTE",
        name: "Mesa Universidades na resistência em tempos de pandemia",
        desc: "Debatedores: Tamires Ferreira Coelho (UFMT/Projeto Pauta G\xEAnero), Katarini Miguel (UFMS/Projeto\n                        Plural), Elton Bruno Pinheiro (UnB/Lab Audio UnB) e Rosana Borges (UFG) \u2013 Media\xE7\xE3o: Luan Chagas\n                        (UFMT/Intercom)",
        link: "https://www.youtube.com/watch?v=sTeCLHqwDEA"
      }, {
        date: new Date("08/03/2021 16:30").toLocaleString('pt-BR'),
        region: "NORDESTE",
        name: "Mesa Articula\xE7\xF5es entre a p\xF3s-gradua\xE7\xE3o e a gradua\xE7\xE3o na constru\xE7\xE3o de saberes cr\xEDticos para a\n                    \xE1rea de comunica\xE7\xE3o no Nordeste",
        desc: "Debatedores: Izani Mustaf\xE1 (UFMA), Edgard Patr\xEDcio (UFC); Fl\xE1via Mayer (UFPB) e Zulmira N\xF3brega\n                    (UFPB) - Media\xE7\xE3o: Paulo Fernando Lopes (UFPI)",
        link: "https://www.youtube.com/watch?v=Yi_eTKBYpMg"
      }, {
        date: new Date("08/03/2021 16:30").toLocaleString('pt-BR'),
        region: "NORTE",
        name: "Mesa 1 - Cultura e Pesquisa na Amaz\xF4nia contempor\xE2nea: como ensinar sem cair em clich\xEAs e\n                    repeti\xE7\xF5es?",
        desc: "Debatedores: Vilso Junior Santi (UFRR) e Fernanda Ribeiro de Salvo (Ufac) - Media\xE7\xE3o: Ana Paula\n                    Vilhena (Est\xE1cio de S\xE1)",
        link: "https://teams.microsoft.com/l/meetup-join/19%3a-MCyaEDX6M6OKa0K1m-ZlNpFUCpkGQcqkDkpE_HDM9M1%40thread.tacv2/1627997075862?context=%7b%22Tid%22%3a%22da49a844-e2e3-40af-86a6-c3819d704f49%22%2c%22Oid%22%3a%221eef2290-e81f-4995-838c-3541621bcb2e%22%7d"
      }, {
        date: new Date("08/03/2021 16:30").toLocaleString('pt-BR'),
        region: "NORTE",
        name: "Mesa 2 - Liberdade ou explora\xE7\xE3o? Hipercapitalismo, empreendedorismo e \u201Cuberiza\xE7\xE3o\u201D do\n                    trabalho na Comunica\xE7\xE3o",
        desc: "Debatedores: Cynthia Mara Miranda (UFT) e Ramon Bezerra (UFMA/S\xE3o Luiz) \u2013 Media\xE7\xE3o:\n                    Arc\xE2ngela Sena (Est\xE1cio de S\xE1 -Par\xE1)",
        link: "https://teams.microsoft.com/l/meetup-join/19%3a-MCyaEDX6M6OKa0K1m-ZlNpFUCpkGQcqkDkpE_HDM9M1%40thread.tacv2/1627997173887?context=%7b%22Tid%22%3a%22da49a844-e2e3-40af-86a6-c3819d704f49%22%2c%22Oid%22%3a%221eef2290-e81f-4995-838c-3541621bcb2e%22%7d"
      }, {
        date: new Date("08/03/2021 16:30").toLocaleString('pt-BR'),
        region: "SUDESTE",
        name: "Palestra A comunicação na resistência ao movimento antivacina",
        desc: "Convidada: Vanessa Veiga de Oliveira (UFMG) – Mediação: Conrado Moreira Mendes (PUC-MG)",
        link: "https://youtu.be/33mn5eG2DHU"
      }, {
        date: new Date("08/03/2021 16:30").toLocaleString('pt-BR'),
        region: "SUL",
        name: "Mesa Comunicação, ética e mobilização social como resistência",
        desc: "Debatedores: Karina Janz (UEPG), Rog\xE9rio Cristofoletti (UFSC), Marcelo Tr\xE4esel (PUC-RS) \u2013 Media\xE7\xE3o:\n                    Carlos Roberto Praxedes dos Santos (Univali)",
        link: "https://us02web.zoom.us/j/83610543811"
      }, {
        date: new Date("08/03/2021 19:00").toLocaleString('pt-BR'),
        region: "Todas",
        name: "Festa de Premiação Ganhadores Expocom Centro-Oeste",
        desc: "(com participação de Márcio Camilo)",
        link: "https://www.youtube.com/watch?v=uJVG8wttQBs"
      }, {
        date: new Date("08/04/2021 16:30").toLocaleString('pt-BR'),
        region: "Todas",
        name: "Mesa Experiências do ensino de Comunicação na pandemia",
        desc: "Debatedores: F\xE1bio Hansen (UFPR/Grupo IEP), Pedro Curcel (Grupo IEP), M\xE1rio Abel Bressan (Unisul)\n                    \u2013 Media\xE7\xE3o: Andr\xE9 Tezza Consentino (UP)",
        link: "https://www.youtube.com/watch?v=3UHxLVegJOM"
      }, {
        date: new Date("08/04/2021 19:0").toLocaleString('pt-BR'),
        region: "Todas",
        name: "Festa de Premiação Expocom Sudeste",
        desc: "(com participação do músico Arthur Rodrigues)",
        link: "https://www.youtube.com/watch?v=8iKbOg0DN5o"
      }, {
        date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
        region: "CENTRO-OESTE",
        name: "Mesa Comunicação popular e resistência no Centro-Oeste",
        desc: "Debatedores: Geremias dos Santos (Associa\xE7\xE3o Brasileira de R\xE1dios Comunit\xE1rias); Romilda Pizani\n                    (Movimento Negro), Jo\xE3o Negr\xE3o (Expresso 61) e Nilton Jos\xE9 dos Reis Rocha (Coletivo Magn\xEDfica\n                    Mundi) \u2013 Media\xE7\xE3o: Lana Guedes (Unigran)",
        link: "https://www.youtube.com/watch?v=Ds2uAxfP-ec"
      }, {
        date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
        region: "NORDESTE",
        name: "Mesa 53 anos de “extensão ou comunicação” no centenário de Paulo Freire",
        desc: "Debatedores: Giovana Mesquita (UFPE), Sheila Borges (UFPE), Eduardo Meditsch (UFSC) - Media\xE7\xE3o:\n                    Norma Meireles (UFPB)",
        link: "https://www.youtube.com/watch?v=tCONCNkqNpM"
      }, {
        date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
        region: "NORTE",
        name: "Mesa 1 - Conhecer, entender e resistir: Big Data e Análise de Dados na comunicação da Amazônia",
        desc: "Debatedores: Lucas Milhomens (Ufam-Parintins /PPGCOM/UFRR) e Diogo Miranda (Est\xE1cio de S\xE1-Par\xE1) - Media\xE7\xE3o: Enderson Oliveira (Est\xE1cio de S\xE1 -Par\xE1)",
        link: "https://teams.microsoft.com/l/meetup-join/19%3a-MCyaEDX6M6OKa0K1m-ZlNpFUCpkGQcqkDkpE_HDM9M1%40thread.tacv2/1627997808722?context=%7b%22Tid%22%3a%22da49a844-e2e3-40af-86a6-c3819d704f49%22%2c%22Oid%22%3a%221eef2290-e81f-4995-838c-3541621bcb2e%22%7d"
      }, {
        date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
        region: "NORTE",
        name: "Mesa 2 - Tempos de pandemia, tempos de criatividade: fluxos e tend\xEAncias para a Assessoria de Comunica\xE7\xE3o",
        desc: "Debatedores: Paulo Vitor Giraldi Pires (Unifap) e Israel Rocha (Ufam) - Media\xE7\xE3o: Erika Siqueira (Est\xE1cio de S\xE1-Par\xE1)",
        link: "https://teams.microsoft.com/l/meetup-join/19%3a-MCyaEDX6M6OKa0K1m-ZlNpFUCpkGQcqkDkpE_HDM9M1%40thread.tacv2/1627998010741?context=%7b%22Tid%22%3a%22da49a844-e2e3-40af-86a6-c3819d704f49%22%2c%22Oid%22%3a%221eef2290-e81f-4995-838c-3541621bcb2e%22%7d"
      }, {
        date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
        region: "SUDESTE",
        name: "Palestra Desinforma\xE7\xE3o e cidadania nas\n                    m\xEDdias sociais brasileiras",
        desc: "Convidada: Raquel Recuero (UFPel)\n                    Media\xE7\xE3o: Geane Carvalho Alzamora (UFMG)",
        link: "https://youtu.be/9HhUpn946A0"
      }, {
        date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
        region: "SUL",
        name: "Minicurso 1 Pesquisa jornal\xEDstica: a constru\xE7\xE3o de narrativas para o livro reportagem",
        desc: "Ministrante: Iago Porfório (UFMG)",
        link: "https://us.bbcollab.com/guest/979070de36ae4ecab9cb91d1aff03dfb"
      }, {
        date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
        region: "SUL",
        name: "Minicurso 2 Produ\xE7\xE3o de conte\xFAdo",
        desc: "Ministrante: Joel Minusculi (Grupo ODP)",
        link: "https://us.bbcollab.com/guest/4452d6a03fa246e6985eafde6039a20c"
      }, {
        date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
        region: "SUL",
        name: "Minicurso 3 Realidade ampliada: exemplos e ferramentas b\xE1sicas",
        desc: "Ministrante: Vinicius Batista de Oliveira (Univali)",
        link: "https://us.bbcollab.com/guest/9e32209c730e446d8803366f2147c148"
      }, {
        date: new Date("08/05/2021 19:00").toLocaleString('pt-BR'),
        region: "Todas",
        name: "Festa de Premia\xE7\xE3o Expocom Nordeste",
        desc: "(com participação do músico Macvanny)",
        link: "https://www.youtube.com/watch?v=qFXaxOoAiZw"
      }, {
        date: new Date("08/06/2021 16:30").toLocaleString('pt-BR'),
        region: "Todas",
        name: "Palestra de Encerramento Convidada Daniela Arbex",
        desc: "",
        link: "https://www.youtube.com/watch?v=TzylTvGgAzY"
      }, {
        date: new Date("08/06/2021 19:00").toLocaleString('pt-BR'),
        region: "Todas",
        name: "Festa de Premiação Expocom Sul",
        desc: "",
        link: "https://www.youtube.com/watch?v=pTeWQskRxXs"
      }];
      var dataSet = [];
      app.lists.myBooks = [];

      for (var _index22 = 0; _index22 < dataStatic.length; _index22++) {
        var element = dataStatic[_index22];
        var child = [];
        child[0] = element.date;
        child[1] = element.region;
        child[2] = " <a href=\"".concat(element.link, "\" class=\"btn btn-primary\" target=\"_blank\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Acessar Programa\xE7\xE3o\">\n                                    <i class=\"fad fa-link\"></i>\n                                </a>");
        child[3] = element.name;
        child[4] = element.desc;
        dataSet.push(child);
      }

      if ($.fn.dataTable.isDataTable('#tableRegionalLinks')) {
        var localTable = $('#tableRegionalLinks').DataTable();
        localTable.destroy();
      }

      $('#tableRegionalLinks thead th').each(function () {
        var title = $(this).text();

        if (title != "Ações") {
          $(this).html("\n                        <label style=\"display: none\">".concat(title.trim(), "</label>\n                        <input type=\"text\" class=\"form-control\" placeholder=\"").concat(title.trim(), "\">\n                    "));
        }
      });
      var tble = $('#tableRegionalLinks').DataTable({
        scrollX: true,
        lengthChange: true,
        buttons: ['excel', 'pdf'],
        data: dataSet,
        columnDefs: [],
        order: [[0, "asc"], [1, "asc"]],
        language: {
          lengthMenu: "Exibindo _MENU_  ",
          zeroRecords: "Nenhum registro disponível.",
          info: "Exibindo pagina _PAGE_ de _PAGES_",
          search: "Filtro _INPUT_",
          paginate: {
            "next": ">",
            "previous": "<"
          }
        }
      });
      tble.columns().every(function () {
        var that = this;
        $('input', this.header()).on('keyup change', function () {
          if (that.search() !== this.value) {
            that.search(this.value).draw();
          }
        });
      });
      setTimeout(function () {
        window.dispatchEvent(new Event('resize'));
        $('[data-toggle="tooltip"]').tooltip();
      }, 300);
    }
  }
};

app.loadBookList = function (congressId, modalPrefix) {
  app.targetCongress = congressId;
  $("#".concat(modalPrefix)).modal('show');
  var bookStatus = {
    waiting: '<span class="badge badge-warning p-2"><i class="fad fa-exclamation-circle"></i> Aguardando</span>',
    ok: '<span class="badge badge-success p-2"><i class="fad fa-check-circle"></i> Aceito</span>'
  };

  for (var index = 0; index < app.institution.congresses.length; index++) {
    var oCongress = app.institution.congresses[index];

    if (oCongress.id == congressId) {
      // Show each created check as a new row in the table
      var dataSet = [];
      app.lists.myBooks = [];

      for (var _index23 = 0; _index23 < oCongress.books.length; _index23++) {
        var element = oCongress.books[_index23];
        var child = [];

        if (app.user.id == element.customerId) {
          child[0] = element.name;
          child[1] = element.area;
          child[2] = element.type;
          child[3] = element.editor;
          child[4] = typeof element.status == 'string' && element.status == 'ok' ? "".concat(bookStatus[element.status], " ").concat(typeof element.linkSubmition == 'string' && element.linkSubmition.length > 0 ? '<span class="badge badge-success p-2"><i class="fad fa-check-double"></i> Link Submetido</span>' : "<button class=\"btn btn-info btn-sm\" onclick=\"app.submitPublicomLink('".concat(oCongress.id, "','").concat(element.id, "')\"><i class=\"fad fa-cloud-upload-alt\"></i> Submeter apresenta\xE7\xE3o</button>"), " ") : bookStatus[element.status];
          app.lists.myBooks.push(element);
          dataSet.push(child);
        }
      }

      if ($.fn.dataTable.isDataTable('#tableBooks')) {
        var localTable = $('#tableBooks').DataTable();
        localTable.destroy();
      }

      $('#tableBooks thead th').each(function () {
        var title = $(this).text();

        if (title != "Ações") {
          $(this).html("\n                        <label style=\"display: none\">".concat(title.trim(), "</label>\n                        <input type=\"text\" class=\"form-control\" placeholder=\"").concat(title.trim(), "\">\n                    "));
        }
      });
      var tble = $('#tableBooks').DataTable({
        scrollX: true,
        lengthChange: true,
        buttons: ['excel', 'pdf'],
        data: dataSet,
        columnDefs: [],
        order: [[1, "asc"]],
        language: {
          lengthMenu: "Exibindo _MENU_  ",
          zeroRecords: "Nenhum registro disponível.",
          info: "Exibindo pagina _PAGE_ de _PAGES_",
          search: "Filtro _INPUT_",
          paginate: {
            "next": ">",
            "previous": "<"
          }
        }
      });
      tble.columns().every(function () {
        var that = this;
        $('input', this.header()).on('keyup change', function () {
          if (that.search() !== this.value) {
            that.search(this.value).draw();
          }
        });
      });
      setTimeout(function () {
        window.dispatchEvent(new Event('resize'));
        $('[data-toggle="tooltip"]').tooltip();
      }, 300);
      $(document).on('show.bs.modal', '.modal', function (event) {
        var zIndex = 1040 + 10 * $('.modal:visible').length;
        $(this).css('z-index', zIndex);
        setTimeout(function () {
          $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
      });
    }
  }
};

app.modalLoad = function (modalPrefix) {
  document.getElementById('congressIdBookSubmition').value = app.targetCongress;
  $("#".concat(modalPrefix)).modal('show');
  if (document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners')) document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners').forEach(function (e) {
    e.remove();
  });
  ClassicEditor.create(document.querySelector('#bookResume'), {
    removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
    cloudServices: {
      tokenUrl: 'https://79675.cke-cs.com/token/dev/05f801120cf7cccdb540ea25a7068d7d1c8108b02232eb1bfec6b89be13c',
      uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
    }
  }).then(function (editor) {
    app.ckeditor.bookResume = editor;
  })["catch"](function (error) {
    console.error(error);
  });
};

app.inscriptionAction = function (congressId) {
  var oCongress = _typeof(app.lists.congresses[congressId]) == "object" ? app.lists.congresses[congressId] : false;

  if (oCongress) {
    $('#workAvaliationModal').modal('show');
    app.lists.workCordinationList = []; // const categs = app.user.cordinatored[oCongress.id].categs;

    document.getElementById('sendMessageAvaliatorButton').setAttribute('congress', "".concat(oCongress.id)); // Show each created check as a new row in the table

    var dataSet = [];
    document.getElementById('sendAvaliationButton').setAttribute('congress', oCongress.id);
    document.querySelector('.lo-footer').style.display = 'none';
    var dateCert = new Date(oCongress.dateCert);
    var today = new Date();

    if (today > dateCert) {
      document.querySelector('.lo-footer-congressman').style.display = 'block';
      document.getElementById('certificateCongressman').setAttribute('congress', oCongress.id);
      document.querySelector('.lo-footer-avaliator').style.display = 'none';
      document.querySelector('.lo-footer-coordinator').style.display = 'none';
    }

    for (var index = 0; index < oCongress.works.length; index++) {
      var element = oCongress.works[index];
      var workCateg = element.category.split(' ');

      if (_typeof(element.submitionWork) == 'object' && element.submitionWork && typeof element.submitionWork.workId == 'string' && element.result == 'approved') {
        var categoryAndModule = element.category.split(' ');
        var child = [];
        child[0] = app.lists.iesByName[element.ies] && app.lists.iesByName[element.ies].region ? app.lists.iesByName[element.ies].region : 'Não preenchido';
        child[1] = "".concat(categoryAndModule[0], " ").concat(categoryAndModule[1]);
        child[2] = element.workName.substring(0, 22) || '';
        child[3] = element.leaderName.substring(0, 18) || '';
        child[4] = app.lists.iesByName[element.ies] && app.lists.iesByName[element.ies].sigla ? app.lists.iesByName[element.ies].sigla : 'Não preenchido';
        child[5] = '';
        child[6] = '';
        child[7] = '';
        child[8] = "\n                    <button type=\"button\" class=\"open-modal btn btn-primary\" onClick=\"app.loadUserDetailDataModal('workDetailModal','".concat(element.id, "')\"  data-toggle=\"tooltip\" data-placement=\"left\" title=\"Detalhes\">\n                        <i class=\"fas fa-clipboard-list\"></i>\n                    </button>\n                    ").concat(typeof element.result == 'string' && element.result == 'approved' && typeof element.presentationVideo == 'string' && element.presentationVideo.length > 0 ? "<a class=\"btn btn-dark\" href=\"".concat(element.presentationVideo, "\" target=\"_blank\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Video de apresenta\xE7\xE3o\"><i class=\"fas fa-play-circle\"></i></a> ") : '', " \n                    ");
        app.lists.workCordinationList.push(element);
        dataSet.push(child);
      }
    }

    if ($.fn.dataTable.isDataTable('#tableWorks')) {
      var localTable = $('#tableWorks').DataTable();
      localTable.destroy();
    }

    $('#tableWorks thead th').each(function () {
      var title = $(this).text();

      if (title != "Ações") {
        $(this).html("\n                    <label style=\"display: none\">".concat(title.trim(), "</label>\n                    <input type=\"text\" class=\"form-control\" placeholder=\"").concat(title.trim(), "\">\n                "));
      }
    });
    var tble = $('#tableWorks').DataTable({
      scrollX: true,
      lengthChange: true,
      buttons: ['excel', 'pdf'],
      data: dataSet,
      columnDefs: [{
        "targets": [5],
        "visible": false,
        "searchable": false
      }, {
        "targets": [6],
        "visible": false,
        "searchable": false
      }, {
        "targets": [7],
        "visible": false,
        "searchable": false
      }],
      order: [[1, "asc"], [0, "asc"]],
      language: {
        lengthMenu: "Exibindo _MENU_  ",
        zeroRecords: "Nenhum registro disponível.",
        info: "Exibindo pagina _PAGE_ de _PAGES_",
        search: "Filtro _INPUT_",
        paginate: {
          "next": ">",
          "previous": "<"
        }
      }
    });
    tble.columns().every(function () {
      var that = this;
      $('input', this.header()).on('keyup change', function () {
        if (that.search() !== this.value) {
          that.search(this.value).draw();
        }
      });
    });
    setTimeout(function () {
      window.dispatchEvent(new Event('resize'));
      $('[data-toggle="tooltip"]').tooltip();
    }, 300);
    $(document).on('show.bs.modal', '.modal', function (event) {
      var zIndex = 1040 + 10 * $('.modal:visible').length;
      $(this).css('z-index', zIndex);
      setTimeout(function () {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
      }, 0);
    });
    $('.page-hover').fadeOut();
  }
};

app.loadEventsIns = function () {
  var oIns = app.institution;
  var inscripted = [];

  if (_typeof(app.user.inscriptions) == 'object' && app.user.inscriptions instanceof Array && app.user.inscriptions.length > 0) {
    for (var index = 0; index < app.user.inscriptions.length; index++) {
      var oInscription = app.user.inscriptions[index];

      if (_typeof(oInscription.transaction) == 'object' && typeof oInscription.transaction.status == 'string' && oInscription.transaction.status == 'paid') {
        inscripted.push(oInscription.congressId);
      } else if (oInscription.type == 'associated') {
        inscripted.push(oInscription.congressId);
      }
    }
  }

  var avaliatored = {};

  if (_typeof(app.user.avaliators) == 'object' && app.user.avaliators instanceof Array && app.user.avaliators.length > 0) {
    for (var _index24 = 0; _index24 < app.user.avaliators.length; _index24++) {
      var avaliators = app.user.avaliators[_index24];
      avaliatored[avaliators.congressId] = avaliators;
    }
  }

  var worked = [];

  if (_typeof(app.user.works) == 'object' && app.user.works instanceof Array && app.user.works.length > 0) {
    for (var _index25 = 0; _index25 < app.user.works.length; _index25++) {
      var work = app.user.works[_index25];
      worked.push(work.customerId);
      worked.push(work.id);
    }
  }

  var cordinatored = {};

  if (_typeof(app.user.cordinators) == 'object' && app.user.cordinators instanceof Array && app.user.cordinators.length > 0) {
    for (var _index26 = 0; _index26 < app.user.cordinators.length; _index26++) {
      var cordinators = app.user.cordinators[_index26];
      cordinatored[cordinators.congressId] = cordinators;
    }
  }

  app.user.cordinatored = cordinatored;
  app.lists.iesByName = {};
  app.lists.categories = [];

  if (_typeof(oIns) == 'object' && _typeof(oIns.congresses) == 'object' && oIns.congresses instanceof Array && oIns.congresses.length > 0) {
    app.lists.works = [];
    app.lists.workCordinationList = [];
    app.lists.congresses = {};

    for (var key = 0; key < oIns.congresses.length; key++) {
      var oCongress = oIns.congresses[key];
      app.lists.congresses[oCongress.id] = oCongress;

      for (var idx = 0; idx < oCongress.iesList.length; idx++) {
        var oIes = oCongress.iesList[idx];
        app.lists.iesByName[oIes.name] = oIes;
      }

      for (var _idx = 0; _idx < oCongress.categories.length; _idx++) {
        var category = oCongress.categories[_idx];
        app.lists.categories.push(category);
      }

      var submitWorkBtn = '';
      var cordinationBtn = '';
      var inscriptionBtn = new Date() > new Date(oCongress.dateEnd) ? '' : "<button class=\"btn btn-outline-success ml-2\" onClick=\"app.loadModalAction(".concat(key, ",'inscription')\"><i class=\"fad fa-plus-octagon\"></i> Inscrever-se</button>");
      var avaliationBtn = new Date() > new Date(oCongress.dateEnd) ? '' : "<button class=\"btn btn-outline-warning ml-2\" onClick=\"app.loadModalAction(".concat(key, ",'avl')\"><i class=\"fad fa-clipboard-list-check\"></i> Ser Avaliador</button>");
      var bookSubmition = '';
      var scheduleLinks = '';

      if (typeof oCongress.type == 'undefined' || oCongress.type == 'regional') {
        document.querySelector('.type-picker').style.display = 'none';
        document.querySelector('#inscriptionWorkAlumn').style.display = 'block';

        if (_typeof(oCongress.works) == 'object' && oCongress.works instanceof Array && oCongress.works.length > 0) {
          for (var _index27 = 0; _index27 < oCongress.works.length; _index27++) {
            var oWork = oCongress.works[_index27];
            app.lists.works.push(oWork);
            var leaderAlumnCpf = typeof oWork.leaderDoc == 'string' && oWork.leaderDoc.length > 10 ? oWork.leaderDoc.replace(/[\. ,:-]+/g, "") : false;
            var userCpf = typeof app.user.cpf == 'string' && app.user.cpf.length > 10 ? app.user.cpf.replace(/[\. ,:-]+/g, "") : false;

            if (leaderAlumnCpf && userCpf) {
              if (leaderAlumnCpf == userCpf && worked.indexOf(oWork.id) < 0) {
                // submitWorkBtn  = `<button class="btn btn-outline-danger ml-2" onClick="app.loadModalAction(${key},'workinscription')"><i class="fad fa-file-upload"></i> Inscrever trabalho</button>`;
                app.user["congress_".concat(oCongress.id)] = {
                  submitedWorkId: oWork.id
                };
              } else if (worked.indexOf(oWork.id) > -1) {
                submitWorkBtn = "<button class=\"btn btn-danger ml-2\" onClick=\"app.loadWorkStatus('".concat(oWork.id, "')\"><i class=\"fad fa-file-upload\"></i> Trabalho Inscrito</button>");
              }
            }
          }
        }

        if (inscripted.indexOf(oCongress.id) > -1) {
          scheduleLinks = "<button class=\"btn btn-info ml-2\" onClick=\"app.loadScheduleListt('".concat(oCongress.id, "', 'regionalListModal')\"><i class=\"fad fa-calendar-check\"></i> Programa\xE7\xE3o</button>");
        }
      } else if (oCongress.type == 'nacional') {
        document.querySelector('.type-picker').style.display = 'block';
        document.querySelector('#inscriptionWorkAlumn').style.display = 'none';

        if (_typeof(oCongress.works) == 'object' && oCongress.works instanceof Array && oCongress.works.length > 0) {
          for (var _index28 = 0; _index28 < oCongress.works.length; _index28++) {
            var _oWork3 = oCongress.works[_index28]; // console.log(worked, oWork.id)

            _oWork3.congressType = 'nacional';
            app.lists.works.push(_oWork3);

            if (worked.indexOf(_oWork3.id) < 0) {// submitWorkBtn  = `<button class="btn btn-outline-danger ml-2" onClick="app.loadModalActionDiff('${oCongress.id}','workinscription')"><i class="fad fa-file-upload"></i> Inscrever trabalho</button>`;
              // break;
            } else if (worked.indexOf(_oWork3.id) > -1) {
              submitWorkBtn = "<button class=\"btn btn-danger ml-2\" onClick=\"app.loadWorkStatus('".concat(_oWork3.id, "')\"><i class=\"fad fa-file-upload\"></i> Trabalho Inscrito</button>");
              break;
            } else if (worked.indexOf(app.user.id) > -1) {
              submitWorkBtn = "<button class=\"btn btn-danger ml-2\" onClick=\"app.loadWorkStatus('".concat(_oWork3.id, "')\"><i class=\"fad fa-file-upload\"></i> Trabalho Inscrito</button>");
              break;
            }
          }
        } else {//console.log('Tamo Aew')
          // submitWorkBtn  = `<button class="btn btn-outline-danger ml-2" onClick="app.loadModalActionDiff('${oCongress.id}','workinscription')"><i class="fad fa-file-upload"></i> Inscrever trabalho</button>`;
        }

        if (inscripted.indexOf(oCongress.id) > -1) {
          bookSubmition = "<button class=\"btn btn-info ml-2\" onClick=\"app.loadBookList('".concat(oCongress.id, "', 'bookListModal')\"><i class=\"fad fa-books-medical\"></i> Publicom</button>");
        }
      }

      if (inscripted.indexOf(oCongress.id) > -1) {
        var today = new Date();
        var eventDay = new Date(oCongress.dateEnd);
        console.log(today > eventDay);
        var congressUrl = "https://".concat(oCongress.url, ".uhub.live/view/core/account-creation.html");

        if (oCongress.type == 'nacional' && today <= eventDay) {
          inscriptionBtn = "<a class=\"btn btn-success ml-2\" href=\"".concat(congressUrl, "?access=").concat(app.getData('token'), "\"><i class=\"fad fa-plus-octagon\"></i> Inscrito</a>");
        } else {
          inscriptionBtn = "<button class=\"btn btn-success ml-2\" onclick=\"app.inscriptionAction('".concat(oCongress.id, "')\"><i class=\"fad fa-plus-octagon\"></i> Inscrito</button>");
        }
      } else {
        submitWorkBtn = '';
      }

      if (_typeof(avaliatored[oCongress.id]) == 'object' && avaliatored[oCongress.id].status == 'ok') {
        avaliationBtn = "<button class=\"btn btn-warning ml-2\" onclick=\"app.loadWorksForAvaliation('".concat(oCongress.id, "')\"><i class=\"fad fa-clipboard-list-check\"></i> Avaliar Trabalhos</button>");
      } else if (_typeof(avaliatored[oCongress.id]) == 'object' && avaliatored[oCongress.id].status == 'waiting') {
        avaliationBtn = "<button class=\"btn btn-warning ml-2\" ><i class=\"fad fa-clipboard-list-check\"></i> Avaliador Cadastrado aguardando libera\xE7\xE3o</button>";
      }

      if (_typeof(cordinatored[oCongress.id]) == 'object') {
        cordinationBtn = "<button class=\"btn btn-primary ml-2\" onclick=\"app.loadWorksForCordination('".concat(oCongress.id, "')\"><i class=\"fas fa-user-cog\"></i> Coordena\xE7\xE3o</button>");
      } // //console.log(oCongress)


      var text = oCongress.resume;
      var regex = /(<([^>]+)>)/ig;
      text = text.replace(regex, "");
      $('#congress').append("\n                <div class=\"col-md-12\">\n                    <div class=\"card flex-md-row mb-4 shadow-sm h-md-250\">\n                        <div class=\"card-body d-flex flex-column align-items-start\">\n                            <strong class=\"d-inline-block mb-2 text-primary\">Evento</strong>\n                            <h3 class=\"mb-0\">\n                                ".concat(oCongress.name, "\n                            </h3>\n                            <div class=\"mb-1 text-muted\">").concat(new Date(oCongress.date).toLocaleString(), "</div>\n                            <p class=\"card-text mb-auto\">").concat(text.slice(0, 105), "...</p>\n                            <div class=\"d-flex flex-row multiline-mobile\">\n                                <button class=\"btn btn-dark first-btn\" onClick=\"app.loadModalCongress(").concat(key, ")\"><i class=\"fad fa-angle-double-right\"></i> Ler mais</button>\n                                ").concat(inscriptionBtn, "\n                                ").concat(submitWorkBtn, "\n                                ").concat(avaliationBtn, "\n                                ").concat(cordinationBtn, "\n                                ").concat(bookSubmition, "\n                                ").concat(scheduleLinks, "\n                            </div>\n                        </div>\n                        <div class=\"card-img-right flex-auto d-none d-lg-block\" style=\"width: 200px; height: 200px; background: url(").concat(oCongress.topImageUrl, ") center center / cover no-repeat\" alt=\"Card image cap\"></div>\n                    </div>\n                </div>\n            "));
    }
  }
};

app.bindLoadCongresses = function () {
  $('#congress').html('');
  var queryStringObj = {
    host: window.location.host,
    access_token: app.getData('token')
  };
  app.client.request(undefined, endpoint + '/api/Authenticateds/Institution', 'GET', queryStringObj, undefined, function (statusCode, responsePayload) {
    if (statusCode == 200 && responsePayload) {
      var oIns = responsePayload;

      if (_typeof(oIns.congresses) == 'object' && oIns.congresses instanceof Array && oIns.congresses.length > 0) {
        app.institution = oIns;
        app.loadEventsIns();
      } else {
        $('#congress').append("<h3>No momento n\xE3o h\xE1 eventos ativos.</h3>");
      }

      $('[data-toggle="tooltip"]').tooltip();
      $('.page-hover').fadeOut();
    }
  });
};

app.loadByClass = function () {
  // Get the current page from the body class
  var bodyClasses = document.querySelector("body").classList;
  var primaryClass = typeof bodyClasses[0] == 'string' ? bodyClasses[0] : false;

  if (primaryClass == "accountHome") {
    document.querySelector('#certificateCongressman').addEventListener('click', function () {
      localStorage.setItem('targetCert', 'congressmanCert');
      localStorage.setItem('congressCert', this.getAttribute('congress'));
      location.href = "certificate.html";
    });
    document.querySelector('#certificateAvaliator').addEventListener('click', function () {
      localStorage.setItem('targetCert', 'avaliatorCert');
      localStorage.setItem('congressCert', this.getAttribute('congress'));
      location.href = "certificate.html";
    });
    document.querySelector('#certificateCordinator').addEventListener('click', function () {
      localStorage.setItem('targetCert', 'coordinatorCert');
      localStorage.setItem('congressCert', this.getAttribute('congress'));
      location.href = "certificate.html";
    });
    var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
    var tel = document.querySelector('#phone');
    VMasker(tel).maskPattern(telMask[0]);
    tel.addEventListener('input', app.inputHandler.bind(undefined, telMask, 14), false);
    var mobile = document.querySelector('#mobile');
    VMasker(mobile).maskPattern(telMask[0]);
    mobile.addEventListener('input', app.inputHandler.bind(undefined, telMask, 14), false);
    var professional_phone = document.querySelector('#professional_phone');
    VMasker(professional_phone).maskPattern(telMask[0]);
    professional_phone.addEventListener('input', app.inputHandler.bind(undefined, telMask, 14), false);
    var docMask = ['999.999.999-999'];
    var doc = document.querySelector('#cpf');
    VMasker(doc).maskPattern(docMask[0]);
    doc.addEventListener('input', app.inputHandler.bind(undefined, docMask, 14), false);
    var docRg = ['99.999.999-999999'];
    var doc = document.querySelector('#rg');
    VMasker(doc).maskPattern(docRg[0]);
    doc.addEventListener('input', app.inputHandler.bind(undefined, docRg, 14), false);
    var updateMessage = app.getFromQuery('upd');

    if (updateMessage == 'data' && document.querySelector('.information-about-update')) {
      document.querySelector('.information-about-update').style.display = 'block';
      document.querySelector('.content-notice').innerHTML = "<i class=\"fad fa-check-circle\"></i> Dados atualizados com sucesso.";
    }

    if (updateMessage == 'evn' && document.querySelector('.information-about-update')) {
      document.querySelector('.information-about-update').style.display = 'block';
      document.querySelector('.content-notice').innerHTML = "<i class=\"fad fa-check-circle\"></i> Sua inscri\xE7\xE3o foi realizada com sucesso, para verificar sua inscri\xE7\xE3o basta clicar no menu eventos abaixo e verificar se voc\xEA est\xE1 inscrito.";
    }

    var points = 0;
    document.querySelector('.user-picture').src = typeof app.user.photoURL == 'string' && app.user.photoURL.length > 0 ? app.user.photoURL : 'http://ssl.gstatic.com/accounts/ui/avatar_2x.png';
    document.querySelector('.user-name').innerText = app.user.displayName;
    document.querySelector('#name').value = app.user.displayName;
    document.querySelector('#mobile').value = app.user.phoneNumber;
    document.querySelector('#email').value = app.user.email;
    setTimeout(function () {
      var oQueryString = {
        access_token: app.getData('token')
      };
      app.client.request(undefined, endpoint + '/api/Authenticateds/User', 'GET', oQueryString, undefined, function (statusCode, responsePayload) {
        if (statusCode == 200 && responsePayload) {
          $('.page-hover').fadeOut();
          app.user = responsePayload; // HERE WE LOAD ALL DATA FROM USER

          var date = new Date(app.user.birthday);
          document.querySelector('.user-name').innerText = app.user.name;
          document.querySelector('#name').value = app.user.name || '';
          document.querySelector('#gender').value = app.user.gender || '';
          document.querySelector('#birthday').value = "".concat(date.getFullYear().toString(), "-").concat((date.getMonth() + 1).toString().padStart(2, 0), "-").concat((date.getDate() + 1).toString().padStart(2, 0)) || '';
          document.querySelector('#cpf').value = app.user.cpf || '';
          document.querySelector('#rg').value = app.user.rg || '';
          document.querySelector('#phone').value = app.user.phone || '';
          document.querySelector('#mobile').value = app.user.mobile || '';
          document.querySelector('#email').value = app.user.email || '';
          document.querySelector('#address').value = app.user.address || '';
          document.querySelector('#address_number').value = app.user.address_number || '';
          document.querySelector('#address_complement').value = app.user.address_complement || '';
          document.querySelector('#address_neighborhood').value = app.user.address_neighborhood || '';
          document.querySelector('#address_city').value = app.user.address_city || '';
          document.querySelector('#address_state').value = app.user.address_state || '';
          document.querySelector('#address_country').value = app.user.address_country || '';
          document.querySelector('#postal_code').value = app.user.postal_code || '';
          document.querySelector('#role').value = app.user.role || '';
          document.querySelector('#institution_abreviation').value = app.user.institution_abreviation || '';
          document.querySelector('#professional_phone').value = app.user.professional_phone || '';
          document.querySelector('#professional_address').value = app.user.professional_address || '';
          document.querySelector('#professional_address_complement').value = app.user.professional_address_complement || '';
          document.querySelector('#professional_address_neighborhood').value = app.user.professional_address_neighborhood || '';
          document.querySelector('#professional_address_city').value = app.user.professional_address_city || '';
          document.querySelector('#professional_address_state').value = app.user.professional_address_state || '';
          document.querySelector('#professional_address_country').value = app.user.professional_address_country || '';
          document.querySelector('#professional_postal_code').value = app.user.professional_postal_code || '';
          document.querySelector('#title').value = app.user.title || '';
          document.querySelector('#degree_institution').value = app.user.degree_institution || '';
          document.querySelector('#degree_course').value = app.user.degree_course || '';
          document.querySelector('#year_conclusion_degree').value = app.user.year_conclusion_degree || '';
          document.querySelector('#specialization_institution').value = app.user.specialization_institution || '';
          document.querySelector('#specialization_course').value = app.user.specialization_course || '';
          document.querySelector('#year_conclusion_specialization').value = app.user.year_conclusion_specialization || '';
          document.querySelector('#master_institution').value = app.user.master_institution || '';
          document.querySelector('#master_course').value = app.user.master_course || '';
          document.querySelector('#year_conclusion_master').value = app.user.year_conclusion_master || '';
          document.querySelector('#doc_institution').value = app.user.doc_institution || '';
          document.querySelector('#doc_course').value = app.user.doc_course || '';
          document.querySelector('#year_conclusion_doc').value = app.user.year_conclusion_doc || '';
          document.querySelector('#phd_institution').value = app.user.phd_institution || '';
          document.querySelector('#phd_course').value = app.user.phd_course || '';
          document.querySelector('#year_conclusion_phd').value = app.user.year_conclusion_phd || '';
          points += app.user.name ? 1 : 0;
          points += app.user.birthday ? 1 : 0;
          points += app.user.cpf ? 1 : 0;
          points += app.user.phone ? 1 : 0;
          points += app.user.mobile ? 1 : 0;
          points += app.user.email ? 1 : 0;
          points += app.user.address ? 1 : 0;
          points += app.user.address_number ? 1 : 0;
          points += app.user.address_complement ? 1 : 0;
          points += app.user.address_neighborhood ? 1 : 0;
          points += app.user.address_city ? 1 : 0;
          points += app.user.address_state ? 1 : 0;
          points += app.user.address_country ? 1 : 0;
          points += app.user.postal_code ? 1 : 0;

          if (points >= 13) {
            document.querySelector('.payme-btn').addEventListener('click', function () {
              $('.page-hover').fadeIn();
              var checkout = new PagarMeCheckout.Checkout({
                encryption_key: app.encryption_key,
                success: function success(data) {
                  var payloadObj = _objectSpread({
                    access_token: app.getData('token'),
                    host: window.location.host
                  }, data);

                  app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Membership', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
                    if (statusCode == 200 && responsePayload) {
                      //console.log(responsePayload, 'HEY LOLO')
                      if (responsePayload.status == 'paid') {
                        app.setData('assInstMem', true);
                        document.querySelector('.information-about-member').style.display = 'none';
                        Swal.fire('Sucesso', 'O seu pagamento foi processado com sucesso.', 'success');
                      }

                      if (responsePayload.type == 'boleto') {
                        var boleto_url = responsePayload.boleto_url;
                        var boleto_barcode = responsePayload.boleto_barcode;
                        document.querySelector('.barcode-copy').innerText = boleto_barcode;
                        document.querySelector('.pdf-file').href = boleto_url;
                        $('#applicationModal').modal('show');
                      }

                      if (responsePayload.type == 'pix') {
                        document.getElementById('qrcode').innerHTML = '';
                        document.querySelector('.pixcode-copy').innerText = '';
                        $('#qrcode').qrcode(responsePayload.tx.pix_qr_code);
                        $('#pixModal').modal('show');
                        document.querySelector('.pixcode-copy').innerText = responsePayload.tx.pix_qr_code;
                      }
                    }

                    $('.page-hover').fadeOut();
                  });
                },
                error: function error(err) {//console.log(err);
                },
                close: function close() {//console.log('The modal has been closed.');
                }
              });
              var queryStringObj = {
                host: window.location.host,
                access_token: app.getData('token')
              };
              app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Membership', 'GET', queryStringObj, undefined, function (statusCode, responsePayload) {
                if (statusCode == 200 && responsePayload.length) {
                  app.institution.membership = responsePayload;
                  var aPhoneNumbers = new Array();
                  aPhoneNumbers.push("+55".concat(app.user.mobile.replace(/[^0-9]/g, "")));
                  var customerData = {
                    name: app.user.name,
                    email: app.user.email,
                    country: "br",
                    external_id: app.user.email,
                    documents: [{
                      type: "cpf",
                      number: app.user.cpf.replace(/[^0-9]/g, "")
                    }],
                    type: "individual",
                    phone_numbers: aPhoneNumbers
                  };
                  var billingData = {
                    name: app.user.name,
                    address: {
                      country: "br",
                      state: app.user.address_state,
                      city: app.user.address_city,
                      neighborhood: app.user.address_neighborhood,
                      complementary: app.user.address_complement,
                      street: app.user.address,
                      street_number: app.user.address_number,
                      zipcode: app.user.postal_code.replace(/\D/gm, "")
                    }
                  };
                  var iAmountPrice = 0;
                  var aItems = [];
                  var aPixItems = [];

                  for (var index = 0; index < app.institution.membership.length; index++) {
                    var oProduct = app.institution.membership[index];
                    var oProductTemplate = {
                      id: oProduct.id,
                      title: oProduct.name,
                      unit_price: oProduct.price,
                      quantity: 1,
                      tangible: 'false'
                    };
                    iAmountPrice += oProduct.price;
                    aItems.push(oProductTemplate);
                    aPixItems.push({
                      name: oProduct.name,
                      value: "".concat(oProduct.price)
                    });
                  }

                  var date = new Date();
                  date.setDate(date.getDate() + 1);
                  var pixExpirationDate = "".concat(date.getFullYear(), "-").concat(("0" + (date.getMonth() + 1)).slice(-2), "-").concat(("0" + date.getDate()).slice(-2)); //console.log(aPixItems)

                  var pagarmeTemp = {
                    amount: iAmountPrice,
                    customerData: 'false',
                    createToken: 'true',
                    paymentMethods: 'credit_card,boleto,pix',
                    pix_expiration_date: pixExpirationDate,
                    pix_additional_fields: aPixItems,
                    boletoDiscountPercentage: 0,
                    items: aItems,
                    customer: customerData,
                    billing: billingData
                  }; //console.log(pagarmeTemp)

                  checkout.open(pagarmeTemp);
                } else {
                  Swal.fire('Importante', 'Você já possui um pedido de pagamento, verifique o seu pagamento no seu extrato.', 'info');
                  app.bindLoadHistory();
                  $('#myTab a[href="#history"]').tab('show');
                  $('.page-hover').fadeOut();
                }
              });
            });
          } else {
            document.querySelector('.payme-btn').addEventListener('click', function () {
              Swal.fire('Importante', 'Para realizar o pagamento da sua mensalidade, você primeiro precisa preencher os dados necessários em seu cadastro.', 'info');
              $('#myTab a[href="#profile"]').tab('show');
              document.querySelector('.formInfo').style.display = 'block';
            });
          }
        }
      });
      oQueryString.host = window.location.host;
      app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Product', 'GET', oQueryString, undefined, function (statusCode, responsePayload) {
        if (statusCode == 200) {
          var d = new Date(responsePayload.dateend);
          var dateString = "".concat(d.getUTCDate() < 10 ? '0' + d.getUTCDate() : d.getUTCDate(), "/").concat(d.getUTCMonth() + 1 < 10 ? '0' + (d.getUTCMonth() + 1) : d.getUTCMonth() + 1, "/").concat(d.getUTCFullYear());
          document.querySelector('#date-string').innerText = dateString;
        }
      });
    }, 3500);
    $('[data-toggle="offcanvas"]').on('click', function () {
      $('.offcanvas-collapse').toggleClass('open');
    });
    document.querySelector('.history-btn').addEventListener('click', function () {
      $('.page-hover').fadeIn();
      app.bindLoadHistory();
    });
    document.querySelector('.congress-btn').addEventListener('click', function () {
      $('.page-hover').fadeIn();
      app.bindLoadCongresses();
    });
  }

  if (primaryClass == "accountUser") {}
};

app.toPdf = function (filename) {
  html2canvas(document.getElementById("certificate"), {
    dpi: 300,
    onrendered: function onrendered(canvas) {
      var imgData = canvas.toDataURL('image/png');
      var doc = new jspdf.jsPDF('l', 'mm', "a4");
      doc.addImage(imgData, 'PNG', 0, 0);
      doc.save(filename + '.pdf');
    },
    logging: true,
    allowTaint: true
  });
}; // Page load certificate


app.loadCertificate = function (eventData) {
  app.lists.ies = {};

  for (var index = 0; index < eventData.oCongress.iesList.length; index++) {
    var ies = eventData.oCongress.iesList[index];
    app.lists.ies[ies.name] = ies;
  }

  var regions = [],
      categs = [];

  if (eventData.type == 'avaliatorCert') {
    for (var _index29 = 0; _index29 < eventData.oCustomer.avaliations.length; _index29++) {
      var oAvaliation = eventData.oCustomer.avaliations[_index29];

      if (_typeof(oAvaliation.work) == 'object' && oAvaliation.work.congressId == eventData.oCongress.id) {
        regions.push(app.lists.ies[oAvaliation.work.ies].region);
        categs.push(oAvaliation.work.category);
      }
    }
  }

  if (eventData.type == 'coordinatorCert') {
    for (var _index30 = 0; _index30 < eventData.oCustomer.cordinators.length; _index30++) {
      var oCordinator = eventData.oCustomer.cordinators[_index30];

      if (oCordinator.congressId == eventData.oCongress.id) {
        categs = oCordinator.categs;
      }
    }
  }

  var targetCert = localStorage.getItem('targetCert');
  regions = regions.unique();
  categs = categs.unique();
  eventData.oCongress[targetCert] = eventData.oCongress[targetCert].replace('{{region}}', regions.join(","));
  eventData.oCongress[targetCert] = eventData.oCongress[targetCert].replace('{{categs}}', categs.join(","));
  var student = eventData.oCustomer.name;
  var course = eventData.oCongress.name;
  var date = new Date(eventData.oCongress.date);
  document.getElementById('cert-holder').innerText = student;
  document.getElementById('cert-course').innerText = "\"" + course + "\",";
  document.getElementById('cert-details').innerHTML = typeof eventData.oCongress[targetCert] == 'string' ? eventData.oCongress[targetCert] : "";
  document.getElementById('date').innerHTML = (date.getUTCDate() < 10 ? '0' + date.getUTCDate() : date.getUTCDate()) + "/" + (date.getUTCMonth() + 1 < 10 ? '0' + (date.getUTCMonth() + 1) : date.getUTCMonth()) + "/" + date.getUTCFullYear();
  document.getElementById('host').innerText = window.location.host;
  var certificateName = "Certificado-" + course.replaceAll(/ /g, '_') + "-" + student.replaceAll(/ /g, '_');

  function toDataURL(url, callback) {
    var xhr = new XMLHttpRequest();

    xhr.onload = function () {
      var reader = new FileReader();

      reader.onloadend = function () {
        callback(reader.result);
      };

      reader.readAsDataURL(xhr.response);
    };

    xhr.open('GET', url, true);
    xhr.responseType = 'blob';
    xhr.send();
  }

  if (typeof eventData.oCongress.certImageUrl == 'string') {
    toDataURL(eventData.oCongress.certImageUrl, function (dataUrl) {
      var img = document.getElementById('backgroundImage');
      img.src = dataUrl;
    });
  }

  var certBtn = document.getElementById("cert-btn");
  certBtn.setAttribute("onclick", "app.toPdf(\"" + certificateName + "\")"); //cert-detail
};

app.verifyMember = function () {
  var assInstMem = app.getData('assInstMem');

  if (!assInstMem) {
    var queryStringObj = {
      host: window.location.host,
      access_token: app.getData('token')
    };
    app.client.request(undefined, endpoint + '/api/Authenticateds/Institution/Member', 'GET', queryStringObj, undefined, function (statusCode, responsePayload) {
      if (statusCode == 200) {
        if (responsePayload.active) {
          var style = document.createElement('style');
          style.type = 'text/css';
          style.innerHTML = '.information-about-member { display: none; }';
          document.getElementsByTagName('head')[0].appendChild(style);
          app.setData('membership', JSON.stringify(responsePayload));
          app.setData('assInstMem', responsePayload.active);
          app.user.membership = responsePayload;
        }
      } else {
        app.setData('membership', false);
        app.setData('assInstMem', false);
      }
    });
  } else if (assInstMem == 'true') {
    var style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = '.information-about-member { display: none; }';
    document.getElementsByTagName('head')[0].appendChild(style);
  }
}; // Bind the forms


app.bindForms = function () {
  if (document.querySelector("form")) {
    var allForms = document.querySelectorAll("form");

    for (var i = 0; i < allForms.length; i++) {
      allForms[i].addEventListener("submit", function (e) {
        // Stop it from submitting
        e.preventDefault();
        var formId = this.id;
        var path = this.action;
        var method = this.method.toUpperCase(); // // Hide the error message (if it's currently shown due to a previous error)

        document.querySelector("#" + formId + " .formError").style.display = 'none'; // Set the host always into forms for admin recognize from where its the request

        document.querySelector("#" + formId + " #eventHost").value = window.location.host; // Hide the success message (if it's currently shown due to a previous error)

        if (document.querySelector("#" + formId + " .formSuccess")) {
          document.querySelector("#" + formId + " .formSuccess").style.display = 'none';
        }

        if (formId == 'inscriptionWorkAlumn') {
          if (_typeof(app.ckeditor.resumeWork) == 'object') {
            document.getElementById('resumeWork').innerText = app.ckeditor.resumeWork.getData();
          }

          if (_typeof(app.ckeditor.studyObject) == 'object') {
            document.getElementById('studyObject').innerText = app.ckeditor.studyObject.getData();
          }

          if (_typeof(app.ckeditor.researchRealized) == 'object') {
            document.getElementById('researchRealized').innerText = app.ckeditor.researchRealized.getData();
          }

          if (_typeof(app.ckeditor.productionDescription) == 'object') {
            document.getElementById('productionDescription').innerText = app.ckeditor.productionDescription.getData();
          }
        }

        if (_typeof(app.ckeditor.descriptionAvaliationWorkAvaliator) == 'object' && document.getElementById('descriptionAvaliationWorkAvaliator')) {
          document.getElementById('descriptionAvaliationWorkAvaliator').innerText = app.ckeditor.descriptionAvaliationWorkAvaliator.getData();
        }

        var valueText = '';

        if (_typeof(app.ckeditor.bookResume) == 'object' && document.getElementById('bookResume')) {
          document.getElementById('bookResume').innerText = app.ckeditor.bookResume.getData();
        }

        if (document.querySelector('.button-imgload-submit') && document.querySelector('.button-imgload-submit-loader')) {
          document.querySelector('.button-imgload-submit').style.display = 'none';
          document.querySelector('.button-imgload-submit-loader').style.display = 'block';
        } // Turn the inputs into a payload


        var leload = {};
        var payload = {};
        var elements = this.elements;

        for (var i = 0; i < elements.length; i++) {
          if (elements[i].type !== 'submit') {
            // Determine class of element and set value accordingly
            var classOfElement = typeof elements[i].classList.value == 'string' && elements[i].classList.value.length > 0 ? elements[i].classList.value : '';
            var valueOfElement = elements[i].type == 'checkbox' && classOfElement.indexOf('multiselect') == -1 ? elements[i].checked : classOfElement.indexOf('intval') == -1 ? elements[i].value : parseInt(elements[i].value);
            var elementIsChecked = elements[i].checked; // Override the method of the form if the input's name is _method

            var nameOfElement = elements[i].name;

            if (nameOfElement == '_method') {
              method = valueOfElement;
            } else {
              // Create an payload field named "method" if the elements name is actually httpmethod
              if (nameOfElement == 'httpmethod') {
                nameOfElement = 'method';
              } // Create an payload field named "id" if the elements name is actually uid


              if (nameOfElement == 'uid') {
                nameOfElement = 'id';
              } // If the element has the class "multiselect" add its value(s) as array elements


              if (classOfElement.indexOf('multiselect') > -1) {
                if (elementIsChecked) {
                  payload[nameOfElement] = _typeof(payload[nameOfElement]) == 'object' && payload[nameOfElement] instanceof Array ? payload[nameOfElement] : [];
                  payload[nameOfElement].push(valueOfElement);
                }
              } else if (classOfElement.indexOf('form-check-input') > -1) {
                if (elementIsChecked) {
                  payload[nameOfElement] = valueOfElement;
                }
              } else {
                payload[nameOfElement] = valueOfElement;
              }
            }
          }
        } // If the method is DELETE, the payload should be a queryStringObject instead


        var queryStringObject = method == 'DELETE' ? payload : {};

        if (app.user) {
          app.user.token = app.getData('token');

          if (app.user.token) {
            queryStringObject['access_token'] = app.user.token;
          }
        }

        if (_typeof(app.ckeditor.descriptionAvaliationWorkCordinator) == 'object' && document.getElementById('descriptionAvaliationWorkCordinator')) {
          valueText = app.ckeditor.descriptionAvaliationWorkCordinator.getData();
          document.getElementById('descriptionAvaliationWorkCordinator').innerText = valueText;
          payload["descriptionAvaliationWorkCordinator"] = valueText;
        } // Call the API


        app.client.request(undefined, path, method, queryStringObject, payload, function (statusCode, responsePayload) {
          // Display an error on the form if needed
          if (statusCode !== 200) {
            app.formResponseErrorProcessor(formId, payload, statusCode, responsePayload);
          } else {
            // If successful, send to form response processor
            app.formResponseProcessor(formId, payload, responsePayload);
          }

          if (document.querySelector('.button-imgload-submit') && document.querySelector('.button-imgload-submit-loader')) {
            document.querySelector('.button-imgload-submit').style.display = 'block';
            document.querySelector('.button-imgload-submit-loader').style.display = 'none';
          }
        });
      });
    }
  }
};

app.logUserOut = function () {
  window.localStorage.clear();
  window.location.href = '/';
};

app.formResponseErrorProcessor = function (formId, payload, statusCode, responsePayload) {
  switch (statusCode) {
    case 401:
      //console.log(responsePayload)
      // Try to get the error from the api, or set a default error message
      var error = typeof responsePayload.Error == 'string' ? responsePayload.Error : '<i class="fad fa-exclamation-triangle"></i> Ops essa IES já possui uma indicação para essa categoria.'; // Set the formError field with the error text

      document.querySelector("#" + formId + " .formError").innerHTML = error; // Show (unhide) the form error field on the form

      document.querySelector("#" + formId + " .formError").style.display = 'block'; // document.querySelector(`#${formId} .formError`).classList.remove('alert-warning')

      document.querySelector("#".concat(formId, " .formError")).classList.add('alert-danger');
      $("#".concat(formId, "Modal")).animate({
        scrollTop: 0
      }, "slow");
      break;

    case 403:
      app.logUserOut();
      break;

    case 405:
      //console.log(responsePayload)
      // Try to get the error from the api, or set a default error message
      var error = typeof responsePayload.Error == 'string' ? responsePayload.Error : '<i class="fad fa-exclamation-triangle"></i> Ops essa trabalho já foi finalizado'; // Set the formError field with the error text

      document.querySelector("#" + formId + " .formError").innerHTML = error; // Show (unhide) the form error field on the form

      document.querySelector("#" + formId + " .formError").style.display = 'block'; // document.querySelector(`#${formId} .formError`).classList.remove('alert-warning')

      document.querySelector("#".concat(formId, " .formError")).classList.add('alert-danger');
      $("#".concat(formId, "Modal")).animate({
        scrollTop: 0
      }, "slow");
      break;

    default:
      // Try to get the error from the api, or set a default error message
      var error = typeof responsePayload.Error == 'string' ? responsePayload.Error : '<i class="fad fa-info-circle"></i> Houve um erro, por favor tente novamente, caso o erro persista entre em contato com o suporte técnigo: contato@uhub.team.'; // Set the formError field with the error text

      document.querySelector("#" + formId + " .formError").innerHTML = error; // Show (unhide) the form error field on the form

      document.querySelector("#" + formId + " .formError").style.display = 'block';
      break;
  }
};

app.formResponseProcessor = function (formId, payload, responsePayload) {
  document.querySelector("#" + formId + " .formSuccess").style.display = 'block';

  if (document.querySelector('.formInfo')) {
    document.querySelector('.formInfo').style.display = 'none';
  }

  if (formId == 'updateDataInformation') {
    window.scrollTo(0, 0);
    document.location = '/pages/account-home.html?upd=data';
  }

  if (formId == 'institutionCongressWork') {
    document.querySelectorAll("#" + formId + " input").forEach(function (e) {//console.log(e)
      // e.disabled = true;
    }); // document.getElementById(formId).reset()
  }

  if (formId == 'inscriptionWorkAlumn') {
    window.scrollTo(0, 0);
    Swal.fire('Uhuu', 'O seu trabalho foi submetido com sucesso. Agora é só esperar a revisão e avaliação.', 'success').then(function (e) {
      window.location.reload();
    });
  }

  if (formId == 'workAvaliationAvaliator') {
    Swal.fire('Uhuu', 'A sua avaliação foi submetida com sucesso.', 'success').then(function (e) {
      $('.page-hover').fadeIn();
      $('.page-hover').css('zIndex', 9999);
      $("#".concat(formId, "Modal")).modal('hide');
      var workId = $("#".concat(formId, " #congressIdWorkAvaliator")).val();
      app.bindLoadCongresses();
      setTimeout(function () {
        app.loadWorksForAvaliation(workId);
        $('.page-hover').css('zIndex', 2);
      }, 2500);
    });
  }

  if (formId == 'workCordinationCordinator') {
    Swal.fire('Uhuu', 'A sua avaliação foi concluida com sucesso.', 'success').then(function (e) {
      $('.page-hover').fadeIn();
      $("#".concat(formId, "Modal")).modal('hide');
      $('.page-hover').css('zIndex', 9999);
      var workId = $("#".concat(formId, " #congressIdWorkCordinator")).val();
      app.bindLoadCongresses();
      setTimeout(function () {
        app.loadWorksForCordination(workId);
        $('.page-hover').css('zIndex', 2);
      }, 2500);
    });
  }

  if (formId == 'bookSubmition') {
    Swal.fire('Uhuu', 'A sua proposta de lançamento de livro foi concluida com sucesso.', 'success').then(function (e) {
      $("#".concat(formId, "Modal")).modal('hide');
      document.getElementById(formId).reset();
      window.location.reload();
    });
  }
};

app.checkFrontTemplate = function () {
  var appLogo = app.getData('applicationLogo');
  var appName = app.getData('applicationName');
  var contactMail = app.getData('contactMail');
  var contactPhone = app.getData('contactPhone');

  if (appLogo && appName) {
    if (document.querySelector('.client-name')) {
      document.querySelector('.client-logo').src = appLogo;
    }

    if (document.querySelector('.client-name')) {
      document.querySelector('.client-name').innerText = appName;
    }

    if (document.querySelector('.logo-thumbnail')) {
      document.querySelector('.logo-thumbnail').src = appLogo;
    }

    if (document.querySelector('.contactMail')) {
      document.querySelector('.contactMail').innerText = contactMail;
    }

    if (document.querySelector('.contactPhone')) {
      document.querySelector('.contactPhone').innerText = contactPhone;
    }
  } else {
    var queryStringObj = {
      host: window.location.host
    };
    app.client.request(undefined, endpoint + '/api/Publics/Institution', 'GET', queryStringObj, undefined, function (statusCode, responsePayload) {
      if (statusCode == 200) {
        appLogo = responsePayload.logo;
        appName = responsePayload.name;
        contactMail = responsePayload.contact_email;
        contactPhone = responsePayload.contact_phone;
        app.setData('applicationLogo', appLogo);
        app.setData('applicationName', appName);
        app.setData('contactMail', contactMail);
        app.setData('contactPhone', contactPhone);

        if (document.querySelector('.client-name')) {
          document.querySelector('.client-logo').src = appLogo;
        }

        if (document.querySelector('.client-name')) {
          document.querySelector('.client-name').innerText = appName;
        }

        if (document.querySelector('.logo-thumbnail')) {
          document.querySelector('.logo-thumbnail').src = appLogo;
        }

        if (document.querySelector('.contactMail')) {
          document.querySelector('.contactMail').innerText = contactMail;
        }

        if (document.querySelector('.contactPhone')) {
          document.querySelector('.contactPhone').innerText = contactPhone;
        }
      }
    });
  }
}; // Authentication, Validatation and core sutff.


app.validateFrbAuth = function () {
  var displayName = app.user.displayName;
  var email = app.user.email;
  var emailVerified = app.user.emailVerified;
  var photoURL = app.user.photoURL;
  var uid = app.user.uid;
  var phoneNumber = app.user.phoneNumber;
  var providerData = app.user.providerData;
  app.user.getIdToken().then(function (accessToken) {
    var requestOb = {
      name: displayName,
      email: email,
      emailVerified: emailVerified,
      phoneNumber: phoneNumber,
      picture: photoURL,
      uid: uid,
      accessToken: accessToken,
      front: window.location.host
    };
    app.client.request(undefined, endpoint + '/api/Authenticateds/authenticate', 'POST', undefined, requestOb, function (statusCode, responsePayload) {
      if (statusCode == 200) {
        app.setData('token', responsePayload.id);
        app.setData('uid', responsePayload.userId);
      } else {
        window.location = '/?rsn=not-autorized';
      }
    });
  });
};

app.checkAuthentication = function () {
  var isAuthenticated = app.getData('token');
  if (!isAuthenticated) app.validateFrbAuth();
  return true;
};

app.loadConfiguration = function () {
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyCCjMIuaOfDNygqU1rGOOyA8h8mST07taE",
    authDomain: "uhubclub.firebaseapp.com",
    databaseURL: "https://uhubclub-default-rtdb.firebaseio.com",
    projectId: "uhubclub",
    storageBucket: "uhubclub.appspot.com",
    messagingSenderId: "1090303585021",
    appId: "1:1090303585021:web:a44056d54ce3ce099dac94",
    measurementId: "G-0500E63LTN"
  }; // Initialize Firebase

  firebase.initializeApp(firebaseConfig); // Set analytics to collect data from application

  firebase.analytics();
};

app.initTranslate = function () {
  setTimeout(function () {
    var dl = 'pt-BR';
    var bl = navigator.language;
    var google = 'Google';
    var facebook = 'Facebook';
    var email = 'password';
    bl === 'en-US' || bl === 'en-us' ? runlang(bl) : runlang(dl);

    function translate(text, provider) {
      var container = $('.firebaseui-idp-' + provider.toLowerCase() + ' .firebaseui-idp-text-long');

      if (provider === 'password') {
        provider = 'email';
        container.text(text + ' ' + provider);
      } else {
        container.text(text + ' ' + provider);
      }
    }

    function translate2(html, provider) {
      var container = $('.firebaseui-card-footer.firebaseui-provider-sign-in-footer');
      container.html(html);
    }

    function runlang(lang) {
      //console.log(lang)
      if (lang === 'en-US' || lang === 'en-us') {
        var sign_in = 'Sign in with';
        translate(sign_in, google);
        translate(sign_in, facebook);
        translate(sign_in, email);
      } else {
        var sign_in = 'Entrar com';
        translate(sign_in, google);
        translate(sign_in, facebook);
        translate(sign_in, email);
        translate2("<p class=\"firebaseui-tos firebaseui-tospp-full-message\">Ao continuar, voc\xEA indica que aceita nossos <a href=\"/legal/terms-of-use.html\" class=\"firebaseui-link firebaseui-tos-link\" target=\"_blank\">Termos de Servi\xE7o</a> e <a href=\"/legal/privacy-policy.html\" class=\"firebaseui-link firebaseui-pp-link\" target=\"_blank\">Pol\xEDtica de Privacidade</a></p>", '.firebaseui-provider-sign-in-footer');
      }
    }
  }, 1500);
};

app.startAuthentication = function () {
  // FirebaseUI config.
  var uiConfig = {
    signInSuccessUrl: 'pages/account-home.html',
    signInOptions: [// Leave the lines as is for the providers you want to offer your users.
    firebase.auth.GoogleAuthProvider.PROVIDER_ID, firebase.auth.EmailAuthProvider.PROVIDER_ID],
    // tosUrl and privacyPolicyUrl accept either url string or a callback
    // function.
    // Terms of service url/callback.
    tosUrl: 'legal/terms-of-use.html',
    // Privacy policy url/callback.
    privacyPolicyUrl: function privacyPolicyUrl() {
      window.location.assign('legal/privacy-policy.html');
    }
  }; // Initialize the FirebaseUI Widget using Firebase.

  var ui = new firebaseui.auth.AuthUI(firebase.auth()); // The start method will wait until the DOM is loaded.

  ui.start('#firebaseui-auth-container', uiConfig);
  app.initTranslate();
};

app.checkIsLoggedIn = function () {
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      app.user = {};
      app.user = user;
      app.checkFrontTemplate();
      app.checkAuthentication();
      app.loadByClass();
      setTimeout(function () {
        app.verifyMember();
      }, 2500);
    } else {
      localStorage.removeItem('token');
      localStorage.removeItem('assInstMem');
      window.location = '/';
    }
  }, function (error) {//console.log(error);
  });
};

app.loadPageOnData = function () {
  // Get the current page from the body class
  var bodyClasses = document.querySelector("body").classList;
  var primaryClass = typeof bodyClasses[0] == 'string' ? bodyClasses[0] : false;

  if (primaryClass == 'index') {
    localStorage.removeItem('token');
    localStorage.removeItem('assInstMem');
    localStorage.removeItem('applicationLogo');
    localStorage.removeItem('applicationName');
    localStorage.removeItem('contactMail');
    localStorage.removeItem('contactPhone');

    if (_typeof(app.user) == 'object' && typeof app.user.token == 'string' && app.user.token.length > 0) {
      delete app.user.token;
    }

    var oQueryString = {
      host: window.location.host
    };
    app.startAuthentication();
    setTimeout(function () {
      app.checkFrontTemplate();
    }, 300);
  }

  if (primaryClass != 'index' && primaryClass != 'institutionCongress' && primaryClass != 'institutionCongressIndication' && primaryClass != 'eventCertificate') {
    app.checkIsLoggedIn();
  }

  if (primaryClass == 'eventCertificate') {
    var _oQueryString = {
      access_token: localStorage.getItem('token'),
      congressId: localStorage.getItem('congressCert'),
      type: localStorage.getItem('targetCert')
    };
    app.client.request(undefined, endpoint + '/api/Authenticateds/Certificate', 'GET', _oQueryString, undefined, function (statusCode, responsePayload) {
      if (statusCode == 200 && responsePayload) {
        app.loadCertificate(responsePayload);
        $('#setPreference').fadeOut(1000);
      }
    });
  }

  if (primaryClass == 'institutionCongresses') {
    var queryStringObj = {
      host: window.location.host
    };
    app.client.request(undefined, endpoint + '/api/Publics/Institution', 'GET', queryStringObj, undefined, function (statusCode, responsePayload) {
      if (statusCode == 200 && responsePayload) {
        var oIns = responsePayload;
        document.getElementById('application-name').innerHTML = "\n                    <img src=\"".concat(responsePayload.logomark, "\" height=\"50px\"/>\n                ");

        if (_typeof(oIns.congresses) == 'object' && oIns.congresses instanceof Array && oIns.congresses.length > 0) {
          oIns.congresses.forEach(function (oCongress, key) {
            var text = oCongress.resume;
            var regex = /(<([^>]+)>)/ig;
            text = text.replace(regex, "");

            if (key == 0) {
              document.querySelector('.home-bg-image').style.background = "url(".concat(oCongress.topImageUrl, ") center center / cover no-repeat");
              document.querySelector('.home-title').innerText = oCongress.name;
              document.querySelector('.home-resume-short').innerText = "".concat(text.slice(0, 240), "..."); // document.querySelector('.home-link-more').src = `/event.html?congressNode=${oCongress.id}`;

              document.getElementById('home-link-more').href = "/event.html?congressNode=".concat(oCongress.id);
            } else {
              $('.congress-collection').append("\n                                <div class=\"col-md-6\">\n                                    <div class=\"card flex-md-row mb-4 shadow-sm h-md-250\">\n                                        <div class=\"card-body d-flex flex-column align-items-start\">\n                                            <strong class=\"d-inline-block mb-2 text-primary\">Evento</strong>\n                                            <h3 class=\"mb-0\">\n                                                <a class=\"text-dark\" href=\"/event.html?congressNode=".concat(oCongress.id, "\">").concat(oCongress.name, "</a>\n                                            </h3>\n                                            <div class=\"mb-1 text-muted\">").concat(new Date(oCongress.date).toLocaleString(), "</div>\n                                            <p class=\"card-text mb-auto\">").concat(text.slice(0, 37), "...</p>\n                                            <a href=\"/event.html?congressNode=").concat(oCongress.id, "\"><i class=\"fad fa-angle-double-right\"></i> Ver evento</a>\n                                        </div>\n                                        <div class=\"card-img-right flex-auto d-none d-lg-block\" style=\"width: 200px; height: 250px; background: url(").concat(oCongress.topImageUrl, ") center center / cover no-repeat\" alt=\"Card image cap\"></div>\n                                    </div>\n                                </div>\n                            ")); //console.log(oCongress, regex, key)
            }
          });
        } else {//console.log(
          // 'ZERAR TUDO'
          // )
        }
      }
    });
  }

  if (primaryClass == 'institutionCongress') {
    var congressNode = app.getFromQuery('congressNode');
    var _queryStringObj = {
      host: window.location.host
    };
    app.client.request(undefined, endpoint + '/api/Publics/Institution', 'GET', _queryStringObj, undefined, function (statusCode, responsePayload) {
      if (statusCode == 200 && responsePayload) {
        var oIns = responsePayload;
        document.querySelector('.phone').innerText = oIns.contact_phone;
        document.querySelector('.email').innerText = oIns.contact_email;
        document.getElementById('application-name').innerHTML = "\n                    <img src=\"".concat(responsePayload.logomark, "\" height=\"50px\"/>\n                ");
        var count = 0;

        if (_typeof(oIns.congresses) == 'object' && oIns.congresses instanceof Array && oIns.congresses.length > 0) {
          oIns.congresses.forEach(function (oCongress, key) {
            var text = oCongress.resume;
            var regex = /(<([^>]+)>)/ig;
            text = text.replace(regex, "");

            if (oCongress.id == congressNode) {
              document.querySelector('.home-bg-image').style.background = "url(".concat(oCongress.topImageUrl, ") center center / cover no-repeat");
              document.querySelector('.home-title').innerText = oCongress.name;
              document.querySelector('.home-resume-long').innerHTML = oCongress.resume;
            } else {
              count++;
              $('.event-calendar').append("\n                                <li>\n                                    <div class=\"row\">\n                                        <div class=\"card flex-md-row mb-4 shadow-sm h-md-250\">\n                                            <div class=\"card-body d-flex flex-column align-items-start\">\n                                                <strong class=\"d-inline-block mb-2 text-primary\">Evento</strong>\n                                                <h3 class=\"mb-0\">\n                                                    <a class=\"text-dark\" href=\"/event.html?congressNode=".concat(oCongress.id, "\">").concat(oCongress.name, "</a>\n                                                </h3>\n                                                <div class=\"mb-1 text-muted\">").concat(new Date(oCongress.date).toLocaleString(), "</div>\n                                                <p class=\"card-text mb-auto\">").concat(text.slice(0, 37), "...</p>\n                                                <a href=\"/event.html?congressNode=").concat(oCongress.id, "\"><i class=\"fad fa-angle-double-right\"></i> Ver evento</a>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </li>\n                            ")); //console.log(oCongress, regex, key)
            }
          });
        } else {//console.log(
          //     'ZERAR TUDO'
          // )
        }

        if (count == 0) {
          document.querySelector('.event-calendar').innerHTML = "<li><i>Esta organiza\xE7\xE3o n\xE3o possui outros eventos agendados</i></li>";
        }
      }
    });
  }

  if (primaryClass == 'institutionCongressIndication') {
    var congressNode = app.getFromQuery('node');
    var _queryStringObj2 = {
      host: window.location.host
    };
    app.client.request(undefined, endpoint + '/api/Publics/Institution', 'GET', _queryStringObj2, undefined, function (statusCode, responsePayload) {
      if (statusCode == 200 && responsePayload) {
        var oIns = responsePayload;
        document.querySelector('.phone').innerText = oIns.contact_phone;
        document.querySelector('.email').innerText = oIns.contact_email;
        document.getElementById('application-name').innerHTML = "\n                    <img src=\"".concat(responsePayload.logomark, "\" height=\"50px\"/>\n                ");

        if (_typeof(oIns.congresses) == 'object' && oIns.congresses instanceof Array && oIns.congresses.length > 0) {
          oIns.congresses.forEach(function (oCongress, key) {
            var text = oCongress.resume;
            var regex = /(<([^>]+)>)/ig;
            text = text.replace(regex, "");

            if (oCongress.id == congressNode) {
              //console.log(oCongress)
              if (_typeof(oCongress.categories) == 'object' && oCongress.categories instanceof Array && oCongress.categories.length > 0) {
                var categorySelect = document.getElementById('category');

                for (var index = 0; index < oCongress.categories.length; index++) {
                  var oCategory = oCongress.categories[index];
                  categorySelect.options[categorySelect.options.length] = new Option(oCategory, oCategory);
                }
              }

              if (_typeof(oCongress.iesList) == 'object' && oCongress.iesList instanceof Array && oCongress.iesList.length > 0) {
                var ieSelect = document.getElementById('ies');

                for (var _index31 = 0; _index31 < oCongress.iesList.length; _index31++) {
                  var ies = oCongress.iesList[_index31];
                  ieSelect.options[ieSelect.options.length] = new Option(ies.name, ies.name);
                }
              }

              document.querySelector('.home-bg-image').style.background = "url(".concat(oCongress.topImageUrl, ") center center / cover no-repeat");
              document.querySelector('.home-title').innerText = oCongress.name;
              document.querySelector('#eventHost').value = window.location.host;
              document.getElementById('congressId').value = oCongress.id;
              var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
              var mobile = document.querySelector('#mobile');
              VMasker(mobile).maskPattern(telMask[0]);
              mobile.addEventListener('input', app.inputHandler.bind(undefined, telMask, 14), false);
              var docMask = ['999.999.999-99'];
              var doc = document.querySelector('#cpf');
              VMasker(doc).maskPattern(docMask[0]);
              doc.addEventListener('input', app.inputHandler.bind(undefined, docMask, 14), false);
              var docLeader = document.querySelector('#leaderDoc');
              VMasker(docLeader).maskPattern(docMask[0]);
              docLeader.addEventListener('input', app.inputHandler.bind(undefined, docMask, 14), false);
            }
          });
        } else {//console.log(
          //     'ZERAR TUDO'
          // )
        }
      }
    });
  }
}; // AJAX Client (for RESTful API)


app.client = {}; // Interface for making API calls

app.client.request = function (headers, path, method, queryStringObject, payload, callback) {
  // Set defaults
  headers = _typeof(headers) == 'object' && headers !== null ? headers : {};
  path = typeof path == 'string' ? path : '/';
  method = typeof method == 'string' && ['POST', 'GET', 'PUT', 'DELETE'].indexOf(method.toUpperCase()) > -1 ? method.toUpperCase() : 'GET';
  queryStringObject = _typeof(queryStringObject) == 'object' && queryStringObject !== null ? queryStringObject : {};
  payload = _typeof(payload) == 'object' && payload !== null ? payload : {};
  callback = typeof callback == 'function' ? callback : false; // For each query string parameter sent, add it to the path

  var requestUrl = path + '?';
  var counter = 0;

  for (var queryKey in queryStringObject) {
    if (queryStringObject.hasOwnProperty(queryKey)) {
      counter++; // If at least one query string parameter has already been added, preprend new ones with an ampersand

      if (counter > 1) {
        requestUrl += '&';
      } // Add the key and value


      requestUrl += queryKey + '=' + queryStringObject[queryKey];
    }
  } // Form the http request as a JSON type


  var xhr = new XMLHttpRequest();
  xhr.open(method, requestUrl, true);
  xhr.setRequestHeader("Content-type", "application/json"); // For each header sent, add it to the request

  for (var headerKey in headers) {
    if (headers.hasOwnProperty(headerKey)) {
      xhr.setRequestHeader(headerKey, headers[headerKey]);
    }
  } // If there is a current session token set, add that as a header


  if (app.session.token) {
    xhr.setRequestHeader("acces_token", app.session.token);
  } // When the request comes back, handle the response


  xhr.onreadystatechange = function () {
    if (xhr.readyState == XMLHttpRequest.DONE) {
      var statusCode = xhr.status;
      var responseReturned = xhr.responseText; // Callback if requested

      if (callback) {
        try {
          var parsedResponse = JSON.parse(responseReturned);
          callback(statusCode, parsedResponse);
        } catch (e) {
          callback(statusCode, false);
        }
      }
    }
  }; // Send the payload as JSON


  var payloadString = JSON.stringify(payload);
  xhr.send(payloadString);
};

app.start = function () {
  app.loadConfiguration();
  app.loadPageOnData(); // Bind all form submissions

  app.bindForms();
};

app.start(); // Helpers

app.setData = function (key, value) {
  localStorage.setItem(key, value);
  return true;
};

app.getData = function (key) {
  return localStorage.getItem(key);
};

app.eventFire = function (el, etype) {
  if (el.fireEvent) {
    el.fireEvent('on' + etype);
  } else {
    var evObj = document.createEvent('Events');
    evObj.initEvent(etype, true, false);
    el.dispatchEvent(evObj);
  }
};

app.inputHandler = function (masks, max, event) {
  var c = event.target;
  var v = c.value.replace(/\D/g, '');
  var m = c.value.length > max ? 1 : 0;
  VMasker(c).unMask();
  VMasker(c).maskPattern(masks[m]);
  c.value = VMasker.toPattern(v, masks[m]);
};

app.loadPixPayment = function (data) {
  document.getElementById('qrcode').innerHTML = '';
  document.querySelector('.pixcode-copy').innerText = '';
  $('#qrcode').qrcode(data);
  $('#pixModal').modal('show');
  document.querySelector('.pixcode-copy').innerText = data;
};