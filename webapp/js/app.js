"use strict";


Array.prototype.unique = function() {
    var a = this.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }
    return a;
};

/*
 * Frontend Logic for application
 *
 */
var endpoint = 'https://bapi.uhub.team';

var app = {
    encryption_key: endpoint.indexOf('localhost') > -1 ? 'ek_test_lF0W00Ds6hN45ZwAdQZQy2VZXSHHNU' : 'ek_live_MSc1djH6K72YHrVQW4guhnFm8wIugu'
};

app.lists = {};

app.session = {};

app.institution = {
    membership: {}
};

app.ckeditor = {};

// Get data from query string
app.getFromQuery = function(param){
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get(param);
}

app.printDiv = function(divName) {
    var documentPrintable = document.getElementById(divName);
    var documentChield = documentPrintable.children[0];
    documentChield.classList.add("w-100");
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = `
       <div class="d-flex flex-column h-100 w-100 p-3">
        <div class="container d-flex align-content-center flex-row mb-3">
            <img src="${app.getData('applicationLogo')}" class="printableLogo" width="64"/>
        </div>
        <h1 class="text-center">
            Comprovante de pagamento ${app.getData('applicationName')}
        </h1>
        <div class="mt-3 mb-5">
            <p class="d-flex justify-content-center">A ${app.getData('applicationName')} comprova que&nbsp;<strong>${app.user.name}</strong>&nbsp;realizou o pagamento dos itens abaixo por meio da plataforma da Uhub</p>
        </div>
        ${printContents}
        CNPJ: 51.201.093/0001-53
       </div>
    `;
    window.print();
    document.body.innerHTML = originalContents;
    documentChield.classList.remove("w-100");
}

app.sendMessageAvaliators = function(modalId){
    $(`#${modalId}`).modal('hide');
    const congressId = document.getElementById('sendMessageAvaliatorButton').getAttribute('congress');
    const oCongress = app.lists.congresses[congressId];
   
    app.lists.categories = app.user.cordinatored[congressId].categs;
    let dataHtml = `
        <div class="form-group">
            <label for="textarea-message-avalitor">Mensagem que será enviada para os avaliadores.</label>
            <textarea class="form-control" id="textarea-message-avalitor" rows="3"></textarea>
        </div>
       
    `;

    if(typeof(oCongress.type) == 'string' && oCongress.type == 'regional' && typeof(oCongress.subdivision) == 'boolean' && oCongress.subdivision && typeof(oCongress.subdivs) == 'object' && oCongress.subdivs instanceof Array && oCongress.subdivs.length > 0 ){
        dataHtml += '<hr><span>Região</span>';
        dataHtml += `<select name="select-region" id="select-region" class="select-region custom-select mb-3" >`
        dataHtml += `<option selected>Selecione...</option>`;
        for (const key in oCongress.subdivs) {
          if (Object.hasOwnProperty.call(oCongress.subdivs, key)) {
            const region = oCongress.subdivs[key];
            dataHtml += `<option value="${region}">${region}</option>`;
          }
        }
        dataHtml += `</select>`;
    }

    dataHtml += '<hr><div class="form-group"><span>Modalidade</span>';
    dataHtml += '<div class="d-flex-row justify-content-between mt-3">';
    for (let index = 0; index < app.lists.categories.length; index++) {
        let modalidade = app.lists.categories[index].split(' ');
        let category = `${modalidade[0]} ${modalidade[1]}`;
        dataHtml += `
            <div class="form-check form-check-inline col-3">
                <input class="form-check-input checkbox-categ-avaliator" type="checkbox" id="${category.replace(" ","")}" value="${category}" >
                <label class="form-check-label" for="${category.replace(" ","")}">${category}</label>
            </div>
        `;
    }
    dataHtml += '</div></div>';



    Swal.fire({
        title: 'Atenção',
        html:  `
            <div class="col-lg-12">
            ${dataHtml}
            </div>
        `,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Enviar Comunicado',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if(result.value){
            var categCheck = [];
            var categories = $('.checkbox-categ-avaliator:checkbox:checked');
            var region = $('#select-region').val() || '';
            var message = $('#textarea-message-avalitor').val();
            for (let index = 0; index < categories.length; index++) {
                const categ = categories[index];
                categCheck.push(categ.value)
            }

            const  payloadObj = { 
                access_token: app.getData('token'),
                host: window.location.host,
                congressId,
                message,
                categs: categCheck,
                region
            }

            app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Congress/Cordinator/Notification', 'POST', undefined, payloadObj, function(statusCode, responsePayload){
                if(statusCode == 200 && responsePayload){
                    Swal.fire(
                        '',
                        'Comunicado foi enviado com sucesso.',
                        'success'
                    )
                } else {
                    Swal.fire(
                        '',
                        'Houve um problema para enviar o comunicado, tente novamente mais tarde.',
                        'warning'
                    )
                }
            })
        }   
        $(`#${modalId}`).modal('show');
    })
}

app.sendMessageAvaliatorsNacional = function(modalId, workId, congressId){


    $(`#${modalId}`).modal('hide');
   
    let dataHtml = `
        <div class="form-group">
            <label for="textarea-message-avalitor">Mensagem que será enviada para os avaliadores.</label>
            <textarea class="form-control" id="textarea-message-avalitor" rows="3"></textarea>
        </div>
       
    `;

    Swal.fire({
        title: 'Atenção',
        html:  `
            <div class="col-lg-12">
            ${dataHtml}
            </div>
        `,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Enviar Comunicado',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if(result.value){
            var message = $('#textarea-message-avalitor').val();

            const  payloadObj = { 
                access_token: app.getData('token'),
                host: window.location.host,
                message,
                congressId,
                workId
            }

            app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Congress/Cordinator/Notification/Work', 'POST', undefined, payloadObj, function(statusCode, responsePayload){
                if(statusCode == 200 && responsePayload){
                    Swal.fire(
                        '',
                        'Comunicado foi enviado com sucesso.',
                        'success'
                    )
                } else {
                    Swal.fire(
                        '',
                        'Houve um problema para enviar o comunicado, tente novamente mais tarde.',
                        'warning'
                    )
                }
            })
        }   
        $(`#${modalId}`).modal('show');
    })
}

app.inactiveWork = function(modalId, workid){
    $(`#${modalId}`).modal('hide');
    const congressId = document.getElementById('sendMessageAvaliatorButton').getAttribute('congress');

    app.lists.categories = app.user.cordinatored[congressId].categs;
    let dataHtml = `
        <div class="form-group">
            <label for="textarea-message-avalitor">Mensagem que será enviada para os estudante.</label>
            <textarea class="form-control" id="textarea-message-avalitor" rows="3"></textarea>
        </div>
       
    `;

    Swal.fire({
        title: 'Atenção',
        html:  `
            <div class="col-lg-12">
                ${dataHtml}
            </div>
        `,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Desclassificar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
       if(result.value){
            var message = $('#textarea-message-avalitor').val();
            const  payloadObj = { 
                access_token: app.getData('token'),
                host: window.location.host,
                congressId,
                message,
                workId: workid
            }

            app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Congress/Work/Declassified', 'POST', undefined, payloadObj, function(statusCode, responsePayload){
                if(statusCode == 200 && responsePayload){
                    Swal.fire(
                        '',
                        'Trabalho desclassificado com sucesso.',
                        'success'
                    )
                } else {
                    Swal.fire(
                        '',
                        'Houve um problema para enviar o comunicado, tente novamente mais tarde.',
                        'warning'
                    )
                }
            })
        }   
        $(`#${modalId}`).modal('show');
      })
}

app.sendAvaliations  = function(modalId){
    const congressId = document.getElementById('sendAvaliationButton').getAttribute('congress');
    app.lists.categories = app.user.cordinatored[congressId].categs;
    const oCongress = app.lists.congresses[congressId];

    let dataHtml = "<span>Após você enviar os trabalhos os algoritimos da Intercom vão processar todas os trabalhos de sua modalidade, aprovando e reprovando automaticamente de acordo com as notas que você aplicou aos trabalhos.<span><hr>";
    dataHtml += '<div class="d-flex-row justify-content-between mt-3">';
    for (let index = 0; index < app.lists.categories.length; index++) {
        let modalidade = app.lists.categories[index].split(' ');
        let category = `${modalidade[0]} ${modalidade[1]}`;
        dataHtml += `
            <div class="form-check form-check-inline col-3">
                <input class="form-check-input checkbox-categ-avaliator" type="checkbox" id="${category.replace(" ","")}" value="${category}" >
                <label class="form-check-label" for="${category.replace(" ","")}">${category}</label>
            </div>
        `;
    }
    dataHtml += '</div>';

    if(typeof(oCongress.subdivision) == 'boolean' && oCongress.subdivision && typeof(oCongress.subdivs) == 'object' && oCongress.subdivs instanceof Array && oCongress.subdivs.length > 0 ){
        dataHtml += '<hr><span>Região</span>';
        dataHtml += `<select name="select-region" id="select-region" class="select-region custom-select mb-3" >`
        dataHtml += `<option selected>Selecione...</option>`;
        for (const key in oCongress.subdivs) {
          if (Object.hasOwnProperty.call(oCongress.subdivs, key)) {
            const region = oCongress.subdivs[key];
            dataHtml += `<option value="${region}">${region}</option>`;
          }
        }
        dataHtml += `</select>`;
    }

    Swal.fire({
        title: 'Atenção',
        html:  `
            <div class="col-lg-12">
            ${dataHtml}
            </div>
        `,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Vamos lá, pode processar as avaliações',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
       if(result.value){
            var categCheck = [];
            var categories = $('.checkbox-categ-avaliator:checkbox:checked');
            for (let index = 0; index < categories.length; index++) {
                const categ = categories[index];
                categCheck.push(categ.value)
            }

            var region = $('#select-region').val();

            const  payloadObj = { 
                access_token: app.getData('token'),
                host: window.location.host,
                congressId,
                categs: categCheck,
                region
            }

            app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Works/Finish', 'POST', undefined, payloadObj, function(statusCode, responsePayload){
                if(statusCode == 200 && responsePayload){
                    Swal.fire(
                        '',
                        'Trabalhos enviados com sucesso.',
                        'success'
                    )
                } if(statusCode == 411 && responsePayload) {
                    Swal.fire(
                        '',
                        'Os trabalhos já foram enviados, caso seja necessário reenviar ou modificar classificações entre em contato com o suporte.',
                        'error'
                    )
                } else {
                    Swal.fire(
                        '',
                        'Houve um problema para enviar os trabalhos, tente novamente mais tarde.',
                        'warning'
                    )
                }
            })
        }   
      })
}

app.bindLoadHistory = function(){

    const oStatus = {
        paid: 'Pago',
        waiting_payment: "Aguardando Pagamento",
        platform_include: "Pagamento Antecipado"
    }

    const colorTheme = {
        "waiting_payment" : "bg-warning text-white",
        "paid" : "bg-success text-white"
    }

    const iconTheme = {
        platform_include : `<i class="fad fa-cash-register" ></i>`,
        boleto: `<i class="fad fa-file-invoice-dollar"></i>`,
        credit_card: `<i class="fad fa-credit-card"></i>`,
        pix: `<i class="fab fa-xing"></i>`
    }

    const  queryStringObj = { 
        host: window.location.host,
        access_token: app.getData('token')
    }

    app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/User/Transactions', 'GET', queryStringObj, undefined, function(statusCode, responsePayload){
        if(statusCode == 200){
            var  sHtmlTemplate = '';
            

            responsePayload.forEach(row => {
               //console.log(row.type, row.status)
                var buttonBoleto = '';
                var buttonHtml = ''
                if(row.type == 'boleto' && row.status == 'waiting_payment'){
                    buttonBoleto = `<a href="${row.tx.boleto_url}" target="_blank" class="btn btn-primary"><i class="fad fa-file-invoice-dollar"></i> Visualizar Boleto</a> `
                }

                if(row.type == 'pix' && row.status == 'waiting_payment'){
                    //console.log(row.tx.pix_expiration_date)
                    buttonBoleto = `<button onClick="app.loadPixPayment('${row.tx.pix_qr_code}')" class="btn btn-primary"><i class="fad fa-qrcode"></i> Visualizar QRCode</a>`
                }
                
                let historyContentIcon = `
                    <div class="tracking-icon status-inforeceived ${colorTheme[row.status]}" data-toggle="tooltip" data-placement="top" title="Transação anterior a Uhub">
                        ${iconTheme[row.type]}
                    </div>
                `
                let historyContentBody = `
                    <div class="tracking-date">${new Date(row.date).toLocaleString()}</div>
                    <div class="tracking-content">
                        <div class="card">
                            <div class="card-header d-flex flex-row justify-content-between">
                                <strong><i><small>#${row.id}</small></i></strong>
                                <button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Transações anteriores a Uhub servem apenas como histórico e não possuem comprovante."/><i class="fad fa-print"></i> Imprimir Comprovante</button>
                            </div>
                            <div class="card-body">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">Item</th>
                                            <th scope="col">Data</th>
                                            <th scope="col">Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Anuidade da associação referente a ${new Date(row.date).getFullYear()}</td>
                                            <td>${new Date(row.date).toLocaleString()}</td>
                                            <td>R$ ${(row.amount / 100).toLocaleString('pt-BR', {
                                                minimumFractionDigits: 2,
                                                maximumFractionDigits: 3
                                            })}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer d-flex flex-row justify-content-between">
                                <p>
                                    <strong>${buttonBoleto} Status:</strong> ${oStatus[row.status]}<br>
                                </p>
                                <p>
                                    <strong>Valor:</strong> R$ ${(row.amount / 100).toLocaleString('pt-BR', {
                                        minimumFractionDigits: 2,
                                        maximumFractionDigits: 3
                                    })}<br>
                                </p>
                            </div>
                        </div>
                    </div>
                    
                `;

               

                if(row.type != 'platform_include'){
                    historyContentIcon = `
                        <div class="tracking-icon status-inforeceived ${colorTheme[row.status]}" data-toggle="tooltip" data-placement="top" title="Transação realizada com a Uhub">
                            ${iconTheme[row.type]}
                        </div>
                    `

                    var itemContent = '';

                    for (let index = 0; index < row.tx.items.length; index++) {
                        const oItem = row.tx.items[index];
                        itemContent += `
                            <tr>
                                <td>${oItem.title}</td>
                                <td>${new Date(row.date).toLocaleString()}</td>
                                <td>R$ ${(oItem.unit_price / 100).toLocaleString('pt-BR', {
                                    minimumFractionDigits: 2,
                                    maximumFractionDigits: 3
                                })}</td>
                            </tr>
                        `
                    }

                    buttonHtml = buttonBoleto

                    if(buttonBoleto.length < 1){
                        buttonHtml = `<button onclick="app.printDiv('printable-${row.id}')" class="btn btn-primary"/><i class="fad fa-print"></i> Imprimir Comprovante</button>`;
                    }

                    historyContentBody = `
                        <div class="tracking-date">${new Date(row.date).toLocaleString()}</div>
                        <div class="tracking-content">
                            <div id="printable-${row.id}">
                                
                                <div class="card mb-3">
                                    <div class="card-header d-flex flex-row justify-content-between">
                                        <strong><i><small>#${row.id}</small></i></strong>
                                        ${buttonHtml}
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Item</th>
                                                    <th scope="col">Data</th>
                                                    <th scope="col">Valor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                ${itemContent}
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="card-footer d-flex flex-row justify-content-between">
                                        <p>
                                            <strong>Status:</strong> ${oStatus[row.status]}<br>
                                        </p>
                                        <p>
                                            <strong>Valor:</strong> R$ ${(row.amount / 100).toLocaleString('pt-BR', {
                                                minimumFractionDigits: 2,
                                                maximumFractionDigits: 3
                                            })}<br>
                                        </p>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                        
                    `;
                    // Exibir o comprovante pois já o deve possuir.
                    //console.log()
                }

                sHtmlTemplate += `
                    <div class="tracking-item">
                        ${historyContentIcon}
                        ${historyContentBody}
                    </div>`;
            })

            document.getElementById('historyTransaction').innerHTML = sHtmlTemplate;
            $('[data-toggle="tooltip"]').tooltip()
            $('.page-hover').fadeOut();
        } 
    })
    
}

app.loadModalCongress = function(index){
    if(app.institution.congresses){
        
        document.getElementById('eventModalTitle').innerHTML = app.institution.congresses[index].name
        document.getElementById('eventModalContent').innerHTML = app.institution.congresses[index].resume
        $('#eventModal').modal('show')
       
    }
}

app.resetElement = function(elm){
    var select = elm;
    var length = select.options.length;
    for (var i = length-1; i >= 0; i--) {
        select.options[i] = null;
    }
}

app.modalConcentActionDiff = function(congressId, modalPrefix, type){
    document.querySelector('#inscriptionWorkAlumn').style.display = 'block';
    document.querySelectorAll('.regional').forEach(elm => {
        elm.remove()
    })

    document.getElementById('myTypeSubmition').value = type;


    $(`#${modalPrefix}Modal`).modal('show') 
    // 
    if(app.institution && app.institution.congresses){
        for (let index = 0; index < app.institution.congresses.length; index++) {
            const oCongress = app.institution.congresses[index];
            if(oCongress.id == congressId){
                
                if(typeof(oCongress.categories) == 'object' && oCongress.categories instanceof Array && oCongress.categories.length > 0){
                    var categSelect = document.querySelector(`#${modalPrefix}Modal #category`);
                    app.resetElement(categSelect)
                    categSelect.options[categSelect.options.length] = new Option("Selecione...", "");
                    for (let index = 0; index < oCongress.categories.length; index++) {
                        const oCategory = oCongress.categories[index];
                        const prefixCategory = oCategory.substring(0, 2);
                        if(type == 'research' && prefixCategory == 'GP'){
                            categSelect.options[categSelect.options.length] = new Option(oCategory, oCategory);
                        } else if(type == 'intercomjr' && prefixCategory == 'IJ'){
                            categSelect.options[categSelect.options.length] = new Option(oCategory, oCategory);
                        }
                        
                    }                
                }
                if(typeof(oCongress.iesList) == 'object' && oCongress.iesList instanceof Array && oCongress.iesList.length > 0){
                    var ieSelect = document.querySelector(`#${modalPrefix}Modal #ies`);
                    app.resetElement(ieSelect)
                    for (let index = 0; index < oCongress.iesList.length; index++) {
                        const ies = oCongress.iesList[index];
                        ieSelect.options[ieSelect.options.length] = new Option(ies.name, ies.name);
                    }
                }

                var aSubtype = [];

                if (type == 'research') {
                    document.querySelectorAll('.intercomjr').forEach(elm => {
                        elm.remove()
                    })

                    aSubtype.push('Doutores(as)')
                    aSubtype.push('Doutorandos(as)')
                    aSubtype.push('Mestres')
                    aSubtype.push('Mestrandos(as)')
                } else if(type == 'intercomjr') {
                    document.querySelectorAll('.research').forEach(elm => {
                        elm.remove()
                    })

                    aSubtype.push('Graduação (recém-graduado e graduando)')
                }

                var subtypeSelect = document.querySelector(`#${modalPrefix}Modal #subtype`);
                
                app.resetElement(subtypeSelect)

                for (let index = 0; index < aSubtype.length; index++) {
                    const subtype = aSubtype[index];
                    subtypeSelect.options[subtypeSelect.options.length] = new Option(subtype, subtype);
                }

                if(modalPrefix == 'inscription'){
                    var priceSelect = document.getElementById('priceSelect');
                    for (let index = 0; index < oCongress.products.length; index++) {
                        const oProduct = oCongress.products[index];
                        priceSelect.options[priceSelect.options.length] = new Option(oProduct.text, oProduct.value);
                    }
        
                    document.querySelector('.payme-inscription-btn').addEventListener('click', function() {     
                        $('.page-hover').fadeIn();
                        $(`#${modalPrefix}Modal`).modal('hide')
                        var membership = app.getData('membership');
                        membership = typeof(membership) == 'string' && membership.length > 0 ? JSON.parse(membership) : false;
                        if(typeof(membership) == 'object' && typeof(membership.active) == 'boolean' && membership.active){
                            const  payloadObj = { 
                                access_token: app.getData('token'),
                                host: window.location.host,
                                congressId: oCongress.id,
                                products: app.institution.congress,
                                associated: app.user.id,
                                oPrice: {
                                    label : document.getElementById('priceSelect').options[document.getElementById('priceSelect').selectedIndex].text,
                                    value : document.getElementById('priceSelect').value
                                }
                            }
                            app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Congress/Inscription', 'POST', undefined, payloadObj, function(statusCode, responsePayload){
                                if(statusCode == 200 && responsePayload){
                                    Swal.fire(
                                        'Sucesso',
                                        'A sua inscrição foi registrada com sucesso.',
                                        'success'
                                    ).then(function(e){
                                        document.location = '/pages/account-home.html?upd=evn';
                                    })
                                } 
                                $('.page-hover').fadeOut();
                            })
                            return true;   
                        } else {
                            
                            var points = 0;
                            points += app.user.name ? 1 : 0;
                            points += app.user.birthday ? 1 : 0;
                            points += app.user.cpf ? 1 : 0;
                            points += app.user.phone ? 1 : 0;
                            points += app.user.mobile ? 1 : 0;
                            points += app.user.email ? 1 : 0;
                            points += app.user.address ? 1 : 0;
                            points += app.user.address_number ? 1 : 0;
                            points += app.user.address_complement ? 1 : 0;
                            points += app.user.address_neighborhood ? 1 : 0;
                            points += app.user.address_city ? 1 : 0;
                            points += app.user.address_state ? 1 : 0;
                            points += app.user.address_country ? 1 : 0;
                            points += app.user.postal_code ? 1 : 0;
                            if(points >= 13){
                                $('.page-hover').fadeIn();
        
                                var value = document.getElementById('priceSelect').value;
                            
                                app.institution.congress = [
                                    {
                                        id: oCongress.id,
                                        name: document.getElementById('priceSelect').options[document.getElementById('priceSelect').selectedIndex].text,
                                        price: +value
                                    }
                                ];
                                
                                var checkout = new PagarMeCheckout.Checkout({
                                    encryption_key: app.encryption_key,
                                    success: function(data) {
                                        const  payloadObj = { 
                                            access_token: app.getData('token'),
                                            host: window.location.host,
                                            congressId: oCongress.id,
                                            products: app.institution.congress,
                                            oPrice: {
                                                label : document.getElementById('priceSelect').options[document.getElementById('priceSelect').selectedIndex].text,
                                                value : document.getElementById('priceSelect').value
                                            },
                                            ...data
                                        }
                                        app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Congress/Inscription', 'POST', undefined, payloadObj, function(statusCode, responsePayload){
                                            if(statusCode == 200 && responsePayload){
                                                if(responsePayload.status == 'paid'){
                                                    Swal.fire(
                                                        'Sucesso',
                                                        'O seu pagamento foi processado com sucesso.',
                                                        'success'
                                                    ).then(function(e){
                                                        document.location = '/pages/account-home.html?upd=evn';
                                                    })
                                                } 
                                                
                                                if(responsePayload.type == 'boleto'){
                                                    var boleto_url = responsePayload.boleto_url;
                                                    var boleto_barcode = responsePayload.boleto_barcode;
                                                    document.querySelector('.barcode-copy').innerText = boleto_barcode;
                                                    document.querySelector('.pdf-file').href = boleto_url;
                                                    $('#applicationModal').modal('show');
                                                }
        
                                                if(responsePayload.type == 'pix'){
                                                    document.getElementById('qrcode').innerHTML = '';
                                                    document.querySelector('.pixcode-copy').innerText = '';
                                                    $('#qrcode').qrcode(responsePayload.tx.pix_qr_code);
                                                    $('#pixModal').modal('show')
                                                    document.querySelector('.pixcode-copy').innerText = responsePayload.tx.pix_qr_code;
                                                }
                                            } 
                                            $('.page-hover').fadeOut();
                                        })
                                    },
                                    error: function(err) {
                                        //console.log(err);
                                    },
                                    close: function() {
                                        //console.log('The modal has been closed.');
                                    }
                                });             
                    
                                const aPhoneNumbers = new Array();
                                aPhoneNumbers.push(`+55${app.user.mobile.replace(/[^0-9]/g, "")}`);
        
                                const customerData = {
                                    name: app.user.name,
                                    email: app.user.email,
                                    country: "br",
                                    external_id: app.user.email,
                                    documents: [{
                                        type: "cpf",
                                        number: app.user.cpf.replace(/[^0-9]/g, "")
                                    }],
                                    type: "individual",
                                    phone_numbers: aPhoneNumbers
                                };
        
                                const billingData = {
                                    name: app.user.name,
                                    address: {
                                        country: "br",
                                        state: app.user.address_state,
                                        city: app.user.address_city,
                                        neighborhood: app.user.address_neighborhood,
                                        complementary: app.user.address_complement,
                                        street: app.user.address,
                                        street_number: app.user.address_number,
                                        zipcode: app.user.postal_code.replace(/\D/gm,""),
                                    }
                                }
        
                                var iAmountPrice = 0;
                                var aItems = [];
                                var aPixItems = [];
        
                                for (let index = 0; index < app.institution.congress.length; index++) {
                                    const oProduct = app.institution.congress[index];
                                    const oProductTemplate = {
                                        id: oProduct.id,
                                        title: oProduct.name,
                                        unit_price: oProduct.price, 
                                        quantity: 1,
                                        tangible: 'false'
                                    }
                                    iAmountPrice += oProduct.price
                                    aItems.push(oProductTemplate)
                                    aPixItems.push({name: oProduct.name, value: `${oProduct.price}`})
                                }
                                var date = new Date();
                                date.setDate(date.getDate() + 1);
        
                                var pixExpirationDate = `${date.getFullYear()}-${(`0`+ ( date.getMonth() + 1 ) ).slice(-2)}-${(`0`+date.getDate()).slice(-2)}`;
                                //console.log(aPixItems)
                                const  pagarmeTemp = {
                                    amount: iAmountPrice,
                                    customerData: 'false',
                                    createToken: 'true',
                                    paymentMethods: 'credit_card,boleto,pix',
                                    pix_expiration_date: pixExpirationDate,
                                    pix_additional_fields: aPixItems,
                                    boletoDiscountPercentage: 0,
                                    items: aItems,
                                    customer: customerData,
                                    billing: billingData
                                };
                                //console.log(pagarmeTemp)
                                checkout.open(pagarmeTemp)
                            } else {
                                $('#inscriptionModal').modal('hide');
                                Swal.fire(
                                    'Importante',
                                    'Para realizar a inscrição no evento, você primeiro precisa terminar de preencher os dados necessários em seu cadastro.',
                                    'info'
                                ).then(e => {
                                    $('#myTab a[href="#profile"]').tab('show')
                                    $('.page-hover').fadeOut();
                                    document.querySelector('.formInfo').style.display = 'block'
                                    document.querySelector('.formInfo').innerHTML = `<i class="fad fa-check-circle"></i> Preencha os dados abaixo para poder realizar a inscrição`
                                })
                            }
        
        
                            
        
                        }
                    });
                    
                    var text = oCongress.resume;
                    var regex = /(<([^>]+)>)/ig;
                    text = text.replace(regex,"");
                    document.querySelector('.inscription-event-name').innerText = oCongress.name;
                    document.querySelector('.inscription-event-desc').innerHTML = text.slice(0,150);
                } else 
                if(modalPrefix == 'avl'){
                    //console.log(`#${modalPrefix}Modal`, oCongress)
                    if(typeof(oCongress.categories) == 'object' && oCongress.categories instanceof Array && oCongress.categories.length > 0){
                        $('.las-categories').html('')
                        for (let index = 0; index < oCongress.categories.length; index++) {
                            const oCategory = oCongress.categories[index];
                            $('.las-categories').append(`
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="dynamicCategCheck${index}" name="${oCategory}">
                                    <label class="form-check-label" for="dynamicCategCheck${index}">${oCategory}</label>
                                </div>
                            `)
                        }                
                    }
                    if(typeof(oCongress.iesList) == 'object' && oCongress.iesList instanceof Array && oCongress.iesList.length > 0){
                        var ieSelect = document.getElementById('las-ies');
                        for (let index = 0; index < oCongress.iesList.length; index++) {
                            const ies = oCongress.iesList[index];
                            ieSelect.options[ieSelect.options.length] = new Option(ies.name, ies.name);
                        }
                    }
        
                    document.getElementById('congressIdAvaliation').value = oCongress.id;
                    document.getElementById('eventHostModalCongressAvaliator').value = window.location.host;
                } else 
                if(modalPrefix == 'workinscription'){
        
                    document.getElementById('congressIdWorkSub').value = oCongress.id;
                    // document.getElementById('lectiveYearLabel').innerText = `O trabalho foi realizado no ano letivo de ${(new Date().getFullYear() - 1)}?`
        
                    
                    document.getElementById('workIdWorkSub').value = '';
        
                    if(document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners'))
                        document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners').forEach(e => {e.remove()});
                        
                    ClassicEditor.create( document.querySelector( '#resumeWork' ),{
                        removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
                        cloudServices: {
                          tokenUrl: 'https://79675.cke-cs.com/token/dev/8d3071b981497c80e501930b07de8518dee6246738ef56f045b00ae10bd9',
                            uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
                        }
                    })
                    .then( editor => {
                        app.ckeditor.resumeWork = editor ;
                    })
                    .catch( error => {
                        console.error( error );
                    });
        
                    ClassicEditor.create( document.querySelector( '#studyObject' ),{
                        removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
                        cloudServices: {
                          tokenUrl: 'https://79675.cke-cs.com/token/dev/8d3071b981497c80e501930b07de8518dee6246738ef56f045b00ae10bd9',
                            uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
                        }
                    })
                    .then( editor => {
                        app.ckeditor.studyObject = editor ;
                    })
                    .catch( error => {
                        console.error( error );
                    });
        
                    ClassicEditor.create( document.querySelector( '#researchRealized' ),{
                        removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
                        cloudServices: {
                          tokenUrl: 'https://79675.cke-cs.com/token/dev/8d3071b981497c80e501930b07de8518dee6246738ef56f045b00ae10bd9',
                            uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
                        }
                    })
                    .then( editor => {
                        app.ckeditor.researchRealized = editor ;
                    })
                    .catch( error => {
                        console.error( error );
                    });
        
                    ClassicEditor.create( document.querySelector( '#productionDescription' ),{
                        removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
                        cloudServices: {
                          tokenUrl: 'https://79675.cke-cs.com/token/dev/8d3071b981497c80e501930b07de8518dee6246738ef56f045b00ae10bd9',
                            uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
                        }
                    })
                    .then( editor => {
                        app.ckeditor.productionDescription = editor ;
                    })
                    .catch( error => {
                        console.error( error );
                    });
                }
            } 
        }
    }
}

app.loadModalActionDiff = function(congressId,modalPrefix){

    document.querySelector('.type-picker').style.display = 'block';
    

    Swal.fire({
        title: '<strong>Submissão de Trabalho</strong>',
        icon: 'info',
        html: 'Para prosseguir por favor selecione para qual tipo você deseja submeter o trabalho',
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: '#ff0000',
        cancelButtonColor: '#0000ff',  
        focusConfirm: false,
        confirmButtonText:
          '<i class="fad fa-laptop-house"></i> Grupo de Pesquisa',
        confirmButtonAriaLabel: 'Grupo de Pesquisa',
        cancelButtonText:
          '<i class="fas fa-user-graduate"></i> Intercom Jr',
        cancelButtonAriaLabel: 'Intercom Jr'
    }).then(function(e){
        if(e.value){
            app.modalConcentActionDiff(congressId, modalPrefix, 'research')
        }else if(e.dismiss == 'cancel'){
            app.modalConcentActionDiff(congressId, modalPrefix, 'intercomjr')
        }
    })
    
}

app.loadModalAction = (key,modalPrefix) => {
    document.querySelector('.type-picker').style.display = 'none';
    document.querySelector('#inscriptionWorkAlumn').style.display = 'block';
    $(`#${modalPrefix}Modal`).modal('show')
    if(app.institution && app.institution.congresses){
        const oCongress = app.institution.congresses[key];
        if(modalPrefix == 'inscription'){
            var priceSelect = document.getElementById('priceSelect');
            for (let index = 0; index < oCongress.products.length; index++) {
                const oProduct = oCongress.products[index];
                priceSelect.options[priceSelect.options.length] = new Option(oProduct.text, oProduct.value);
            }


            document.querySelector('.payme-inscription-btn').addEventListener('click', function() {     
                $('.page-hover').fadeIn();
                $(`#${modalPrefix}Modal`).modal('hide')
                var membership = app.getData('membership');
                membership = typeof(membership) == 'string' && membership.length > 0 ? JSON.parse(membership) : false;
                if(typeof(membership) == 'object' && typeof(membership.active) == 'boolean' && membership.active){
                    const  payloadObj = { 
                        access_token: app.getData('token'),
                        host: window.location.host,
                        congressId: oCongress.id,
                        products: app.institution.congress,
                        associated: app.user.id,
                        oPrice: {
                            label : document.getElementById('priceSelect').options[document.getElementById('priceSelect').selectedIndex].text,
                            value : document.getElementById('priceSelect').value
                        }
                    }
                    app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Congress/Inscription', 'POST', undefined, payloadObj, function(statusCode, responsePayload){
                        if(statusCode == 200 && responsePayload){
                            Swal.fire(
                                'Sucesso',
                                'A sua inscrição foi registrada com sucesso.',
                                'success'
                            ).then(function(e){
                                document.location = '/pages/account-home.html?upd=evn';
                            })
                        } 
                        $('.page-hover').fadeOut();
                    })
                    return true;   
                } else {
                    
                    var points = 0;
                    points += app.user.name ? 1 : 0;
                    points += app.user.birthday ? 1 : 0;
                    points += app.user.cpf ? 1 : 0;
                    points += app.user.phone ? 1 : 0;
                    points += app.user.mobile ? 1 : 0;
                    points += app.user.email ? 1 : 0;
                    points += app.user.address ? 1 : 0;
                    points += app.user.address_number ? 1 : 0;
                    points += app.user.address_complement ? 1 : 0;
                    points += app.user.address_neighborhood ? 1 : 0;
                    points += app.user.address_city ? 1 : 0;
                    points += app.user.address_state ? 1 : 0;
                    points += app.user.address_country ? 1 : 0;
                    points += app.user.postal_code ? 1 : 0;
                    if(points >= 13){
                        $('.page-hover').fadeIn();

                        var value = document.getElementById('priceSelect').value;
                    
                        app.institution.congress = [
                            {
                                id: oCongress.id,
                                name: document.getElementById('priceSelect').options[document.getElementById('priceSelect').selectedIndex].text,
                                price: +value
                            }
                        ];
                        
                        var checkout = new PagarMeCheckout.Checkout({
                            encryption_key: app.encryption_key,
                            success: function(data) {
                                const  payloadObj = { 
                                    access_token: app.getData('token'),
                                    host: window.location.host,
                                    congressId: oCongress.id,
                                    products: app.institution.congress,
                                    oPrice: {
                                        label : document.getElementById('priceSelect').options[document.getElementById('priceSelect').selectedIndex].text,
                                        value : document.getElementById('priceSelect').value
                                    },
                                    ...data
                                }
                                app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Congress/Inscription', 'POST', undefined, payloadObj, function(statusCode, responsePayload){
                                    if(statusCode == 200 && responsePayload){
                                        if(responsePayload.status == 'paid'){
                                            Swal.fire(
                                                'Sucesso',
                                                'O seu pagamento foi processado com sucesso.',
                                                'success'
                                            ).then(function(e){
                                                document.location = '/pages/account-home.html?upd=evn';
                                            })
                                        } 
                                        
                                        if(responsePayload.type == 'boleto'){
                                            var boleto_url = responsePayload.boleto_url;
                                            var boleto_barcode = responsePayload.boleto_barcode;
                                            document.querySelector('.barcode-copy').innerText = boleto_barcode;
                                            document.querySelector('.pdf-file').href = boleto_url;
                                            $('#applicationModal').modal('show');
                                        }

                                        if(responsePayload.type == 'pix'){
                                            document.getElementById('qrcode').innerHTML = '';
                                            document.querySelector('.pixcode-copy').innerText = '';
                                            $('#qrcode').qrcode(responsePayload.tx.pix_qr_code);
                                            $('#pixModal').modal('show')
                                            document.querySelector('.pixcode-copy').innerText = responsePayload.tx.pix_qr_code;
                                        }
                                    } 
                                    $('.page-hover').fadeOut();
                                })
                            },
                            error: function(err) {
                                //console.log(err);
                            },
                            close: function() {
                                //console.log('The modal has been closed.');
                            }
                        });             
            
                        const aPhoneNumbers = new Array();
                        aPhoneNumbers.push(`+55${app.user.mobile.replace(/[^0-9]/g, "")}`);

                        const customerData = {
                            name: app.user.name,
                            email: app.user.email,
                            country: "br",
                            external_id: app.user.email,
                            documents: [{
                                type: "cpf",
                                number: app.user.cpf.replace(/[^0-9]/g, "")
                            }],
                            type: "individual",
                            phone_numbers: aPhoneNumbers
                        };

                        const billingData = {
                            name: app.user.name,
                            address: {
                                country: "br",
                                state: app.user.address_state,
                                city: app.user.address_city,
                                neighborhood: app.user.address_neighborhood,
                                complementary: app.user.address_complement,
                                street: app.user.address,
                                street_number: app.user.address_number,
                                zipcode: app.user.postal_code.replace(/\D/gm,""),
                            }
                        }

                        var iAmountPrice = 0;
                        var aItems = [];
                        var aPixItems = [];

                        for (let index = 0; index < app.institution.congress.length; index++) {
                            const oProduct = app.institution.congress[index];
                            const oProductTemplate = {
                                id: oProduct.id,
                                title: oProduct.name,
                                unit_price: oProduct.price, 
                                quantity: 1,
                                tangible: 'false'
                            }
                            iAmountPrice += oProduct.price
                            aItems.push(oProductTemplate)
                            aPixItems.push({name: oProduct.name, value: `${oProduct.price}`})
                        }
                        var date = new Date();
                        date.setDate(date.getDate() + 1);

                        var pixExpirationDate = `${date.getFullYear()}-${(`0`+ ( date.getMonth() + 1 ) ).slice(-2)}-${(`0`+date.getDate()).slice(-2)}`;
                        //console.log(aPixItems)
                        const  pagarmeTemp = {
                            amount: iAmountPrice,
                            customerData: 'false',
                            createToken: 'true',
                            paymentMethods: 'credit_card,boleto,pix',
                            pix_expiration_date: pixExpirationDate,
                            pix_additional_fields: aPixItems,
                            boletoDiscountPercentage: 0,
                            items: aItems,
                            customer: customerData,
                            billing: billingData
                        };
                        //console.log(pagarmeTemp)
                        checkout.open(pagarmeTemp)
                    } else {
                        $('#inscriptionModal').modal('hide');
                        Swal.fire(
                            'Importante',
                            'Para realizar a inscrição no evento, você primeiro precisa terminar de preencher os dados necessários em seu cadastro.',
                            'info'
                        ).then(e => {
                            $('#myTab a[href="#profile"]').tab('show')
                            $('.page-hover').fadeOut();
                            document.querySelector('.formInfo').style.display = 'block'
                            document.querySelector('.formInfo').innerHTML = `<i class="fad fa-check-circle"></i> Preencha os dados abaixo para poder realizar a inscrição`
                        })
                    }


                    

                }
                
                
                
            });
            
            
            var text = oCongress.resume;
            var regex = /(<([^>]+)>)/ig;
            text = text.replace(regex,"");
            document.querySelector('.inscription-event-name').innerText = oCongress.name;
            document.querySelector('.inscription-event-desc').innerHTML = text.slice(0,150);
        } else 
        if(modalPrefix == 'avl'){
            //console.log(`#${modalPrefix}Modal`, oCongress)
            if(typeof(oCongress.categories) == 'object' && oCongress.categories instanceof Array && oCongress.categories.length > 0){
                $('.las-categories').html('')
                for (let index = 0; index < oCongress.categories.length; index++) {
                    const oCategory = oCongress.categories[index];
                    $('.las-categories').append(`
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="dynamicCategCheck${index}" name="${oCategory}">
                            <label class="form-check-label" for="dynamicCategCheck${index}">${oCategory}</label>
                        </div>
                    `)
                }                
            }
            if(typeof(oCongress.iesList) == 'object' && oCongress.iesList instanceof Array && oCongress.iesList.length > 0){
                var ieSelect = document.getElementById('las-ies');
                for (let index = 0; index < oCongress.iesList.length; index++) {
                    const ies = oCongress.iesList[index];
                    ieSelect.options[ieSelect.options.length] = new Option(ies.name, ies.name);
                }
            }

            document.getElementById('congressIdAvaliation').value = oCongress.id;
            document.getElementById('eventHostModalCongressAvaliator').value = window.location.host;
        } else 
        if(modalPrefix == 'workinscription'){

            document.getElementById('congressIdWorkSub').value = oCongress.id;
            document.getElementById('lectiveYearLabel').innerText = `O trabalho foi realizado no ano letivo de ${(new Date().getFullYear() - 1)}?`

            
            document.getElementById('workIdWorkSub').value = app.user[`congress_${oCongress.id}`].submitedWorkId;

            if(document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners'))
                document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners').forEach(e => {e.remove()});
                
            ClassicEditor.create( document.querySelector( '#resumeWork' ),{
                removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
                cloudServices: {
                  tokenUrl: 'https://79675.cke-cs.com/token/dev/8d3071b981497c80e501930b07de8518dee6246738ef56f045b00ae10bd9',
                   uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
                }
            })
            .then( editor => {
                app.ckeditor.resumeWork = editor ;
            })
            .catch( error => {
                console.error( error );
            });

            ClassicEditor.create( document.querySelector( '#studyObject' ),{
                removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
                cloudServices: {
                  tokenUrl: 'https://79675.cke-cs.com/token/dev/8d3071b981497c80e501930b07de8518dee6246738ef56f045b00ae10bd9',
                   uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
                }
            })
            .then( editor => {
                app.ckeditor.studyObject = editor ;
            })
            .catch( error => {
                console.error( error );
            });

            ClassicEditor.create( document.querySelector( '#researchRealized' ),{
                removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
                cloudServices: {
                  tokenUrl: 'https://79675.cke-cs.com/token/dev/8d3071b981497c80e501930b07de8518dee6246738ef56f045b00ae10bd9',
                   uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
                }
            })
            .then( editor => {
                app.ckeditor.researchRealized = editor ;
            })
            .catch( error => {
                console.error( error );
            });

            ClassicEditor.create( document.querySelector( '#productionDescription' ),{
                removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
                cloudServices: {
                  tokenUrl: 'https://79675.cke-cs.com/token/dev/8d3071b981497c80e501930b07de8518dee6246738ef56f045b00ae10bd9',
                   uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
                }
            })
            .then( editor => {
                app.ckeditor.productionDescription = editor ;
            })
            .catch( error => {
                console.error( error );
            });
        }
    }
}

app.loadWorkDataModal = function(modalId, workId){
    
    if(document.querySelector('.ck.ck-reset.ck-editor.ck-rounded-corners')){
        document.querySelector('.ck.ck-reset.ck-editor.ck-rounded-corners').remove();
    }

    for (let index = 0; index < 11; index++) {
        var ele = document.getElementsByName(`defaultAvaliatorOption[${index}]`);
        for(var i=0;i<ele.length;i++)
            ele[i].checked = false;
    }

    var el = document.getElementsByName(`defaultWorkOf`);
    for(var idx=0;idx<el.length;idx++)
        el[idx].checked = false;
    
    for (let index = 0; index < app.lists.workList.length; index++) {
        const oWork = app.lists.workList[index];
        if(oWork.id == workId){
            
            document.getElementById('experimentalismOfProduct').value = 0;
            document.getElementById('productQuality').value = 0;
            document.getElementById('coerenceOfContent').value = 0;

            const categoryAndModule = oWork.category.split(' ');
            document.getElementById('discipline-2').innerHTML = oWork.submitionWork.discipline;
            document.getElementById('currentYear').innerHTML = oWork.submitionWork.lectiveYear == 'yes' ? 'Sim' : 'Não';
            document.getElementById('congressIdWorkAvaliator').value = oWork.congressId;
            document.getElementById('workIdWorkAvaliator').value = oWork.id;
            document.getElementById('workNameAvaliator').innerHTML = oWork.workName;
            document.getElementById('iesWorkAvaliator').innerHTML = oWork.ies;
            document.getElementById('leaderName').innerHTML = oWork.leaderName;
            document.getElementById('responsableTeatcherWorkAvaliator').innerHTML = oWork.submitionWork.responsableTeatcher;
            document.getElementById('coAuthorsWorkAvaliator').innerHTML = oWork.submitionWork.coAuthors;
            document.getElementById('studyObjectWorkAvaliator').innerHTML = oWork.submitionWork.studyObject;
            document.getElementById('researchRealizedWorkAvaliator').innerHTML = oWork.submitionWork.researchRealized;
            document.getElementById('productionDescriptionWorkAvaliator').innerHTML = oWork.submitionWork.productionDescription;
            

            if(typeof(oWork.submitionWork.linkCloudWork) == 'string' && oWork.submitionWork.linkCloudWork.length > 0){
                document.getElementById('buttonDownloadMaterialWorkAvaliator').style.display = 'block'
                document.getElementById('buttonDownloadMaterialWorkAvaliator').href = oWork.submitionWork.linkCloudWork;
            } else {
                document.getElementById('buttonDownloadMaterialWorkAvaliator').style.display = 'none'
            }

            document.getElementById('category-1').innerHTML = categoryAndModule[0];
            document.getElementById('category-2').innerHTML = `${categoryAndModule[0]} ${categoryAndModule[1]}`;
          
            document.getElementById('videos-from-work').innerHTML = '';

            if(typeof(oWork.correctionVideo) == 'string' && oWork.correctionVideo.length > 0 ){
                document.getElementById('videos-from-work').innerHTML += `
                    <hr>
                    <a href="${oWork.correctionVideo}" class="btn btn-primary" target="_blank">
                        <i class="fad fa-download"></i> Link de Correção
                    </a>`;
            }

            if(typeof(oWork.correctionHistories) == 'object' && oWork.correctionHistories.length > 0 ){
                oWork.correctionHistories.reverse();
                for (let index = 0; index < oWork.correctionHistories.length; index++) {
                    const historyVid = oWork.correctionHistories[index];
                    document.getElementById('videos-from-work').innerHTML += `
                        <hr>
                        <a href="${historyVid}" class="btn btn-primary" target="_blank">
                            <i class="fad fa-download"></i> Link de Correção
                        </a>`;
                }
            }   

            // console.log('Avaliator',app.lists.congresses[oWork.congressId].type)

            if( typeof(app.lists.congresses[oWork.congressId]) == 'object' && typeof(app.lists.congresses[oWork.congressId].type) == 'string' && app.lists.congresses[oWork.congressId].type == 'nacional' && typeof(oWork.oldCongressId) == 'undefined' ){
                document.querySelectorAll('.avaliator-regional').forEach(e => {
                    e.style.display = 'none';
                })
                document.querySelectorAll('.avaliator-nacional').forEach(e => {
                    e.style.display = 'block';
                })
            } else {
                document.querySelectorAll('.avaliator-nacional').forEach(e => {
                    e.style.display = 'none';
                })
                document.querySelectorAll('.avaliator-regional').forEach(e => {
                    e.style.display = 'block';
                })
                // document.querySelectorAll('.avaliator-resumes').style.display = 'none';
            }
           
            
            ClassicEditor.create( document.querySelector( '#descriptionAvaliationWorkAvaliator' ),{
                removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
                cloudServices: {
                tokenUrl: 'https://79675.cke-cs.com/token/dev/8d3071b981497c80e501930b07de8518dee6246738ef56f045b00ae10bd9',
                   uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
                }
            })
            .then( editor => {
                app.ckeditor.descriptionAvaliationWorkAvaliator = editor ;
                app.ckeditor.descriptionAvaliationWorkAvaliator.setData('') 
            })
            .catch( error => {
                console.error( error );
            });

            var formName = modalId.replace('Modal','');

            $(`#${formName} .formError`).hide();
            $(`#${formName} .formSuccess`).hide();

            $(`#${modalId}`).modal('show');

        } 
    }
}

app.getReferenceAvalition = async function(data){

    var avaliatorsCollection = [];

    const avaliationsByAvaliator = {}

    for (let index = 0; index < data.length; index++) {
        const avaliation = data[index];
        if(avaliation.customerId == app.user.id){
            if(typeof(avaliationsByAvaliator[avaliation.customerId]) != 'object'){
                avaliationsByAvaliator[avaliation.customerId] = []
            }
            avaliationsByAvaliator[avaliation.customerId].push(avaliation)   
        }
    }

   for (const key in avaliationsByAvaliator) {
        
        if (Object.hasOwnProperty.call(avaliationsByAvaliator, key)) {
            
            const avaliator = avaliationsByAvaliator[key];

            var avaliation = await avaliator.reduce((a, b) => {
                return new Date(a.date) > new Date(b.date) ? a : b;
            })
            
            avaliatorsCollection.push(avaliation)
       }

   }

   var response = typeof(avaliatorsCollection) == 'object' && avaliatorsCollection.length > 0 ? avaliatorsCollection[0] : false;

   return response;
} 

app.loadWorksForAvaliation = async function(id){

    //console.log('Carregando Lista de Trabalhos', id)

    app.lists.workList = []
    
    document.querySelector('.lo-footer').style.display = 'none';
    
    

    $('#workAvaliationModal').modal('show')

    for (let index = 0; index < app.institution.congresses.length; index++) {
        const oCongress = app.institution.congresses[index];
        if(oCongress.id == id){

            const dateCert = new Date(oCongress.dateCert)
            const today = new Date()

            if(today > dateCert){
                document.querySelector('.lo-footer-congressman').style.display = 'none';
                document.querySelector('.lo-footer-avaliator').style.display = 'block';
                document.getElementById('certificateAvaliator').setAttribute('congress', oCongress.id)
                document.querySelector('.lo-footer-coordinator').style.display = 'none';
            }

          // Show each created check as a new row in the table
            var dataSet = [];
                
            for (let index = 0; index < oCongress.works.length; index++) {
                const element = oCongress.works[index];
                if(typeof(element.submitionWork) == 'object' && element.submitionWork &&  typeof(element.submitionWork.workId) == 'string' && element.avaliators && typeof(element.avaliators) == 'object' && element.avaliators.indexOf(app.user.id) > -1){
                    let aAvalations;
                    if(typeof(element.avaliations) == 'object' && element.avaliations instanceof Array && element.avaliations.length > 0){
                       aAvalations = await app.getReferenceAvalition(element.avaliations);
                    }
                   
                    const categoryAndModule = element.category.split(' ');

                    const child = [];
                    child[0] = app.lists.iesByName[element.ies] && app.lists.iesByName[element.ies].region ? app.lists.iesByName[element.ies].region : 'Não preenchido';
                    child[1] = `${categoryAndModule[0]} ${categoryAndModule[1]}`;
                    child[2] = element.workName.substring(0,22) || '';
                    child[3] = element.leaderName.substring(0,18) || '';
                    child[4] = app.lists.iesByName[element.ies] && app.lists.iesByName[element.ies].sigla ? app.lists.iesByName[element.ies].sigla : 'Não preenchido';
                    if( typeof(element.correctionVideo) == 'string' && element.correctionVideo.length > 0 && typeof(element.defaultCordinatorWorkOf) == 'string' && element.defaultCordinatorWorkOf == 'alterationNeed'){
                        child[5] = '<span class="text-primary"><i class="fas fa-sync"></i> Reavaliar</span>'
                    } else {
                        child[5] = aAvalations ? '<span class="text-success"><i class="fad fa-check-circle"></i> Avaliado</span>' : '<span class="text-warning"><i class="fad fa-exclamation-triangle"></i> Não avaliado</span>';
                    }
                    child[6] = '';
                    child[7] = '';
                    child[8] = `
                        <button type="button" class="open-modal btn btn-dark" onClick="app.loadWorkDataModal('workAvaliationAvaliatorModal','${element.id}')">
                            Avaliar <i class="fad fa-clipboard-list-check"></i>
                        </button>
                        
                        `;
                    app.lists.workList.push(element)
                    dataSet.push(child); 
                }
            }

            if ( $.fn.dataTable.isDataTable( '#tableWorks' ) ) {
                let localTable = $('#tableWorks').DataTable();
                localTable.destroy();
            }
              
            $('#tableWorks thead th').each( function () {
                var title = $(this).text();
                if(title != "Ações"){
                    $(this).html( `
                            <label style="display: none">${title.trim()}</label>
                            <input type="text" class="form-control" placeholder="${title.trim()}">
                    ` );
                }
            });
        
            var tble = $('#tableWorks').DataTable({
                scrollX: true,
                lengthChange: true,
                buttons: [ 'excel', 'pdf' ],
                data: dataSet,
                columnDefs: [
                    {
                        "targets": [ 6 ],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [ 7 ],
                        "visible": false,
                        "searchable": false
                    }
                ],
                order: [[ 1, "asc" ]],
                language: {
                    lengthMenu: "Exibindo _MENU_  ",
                    zeroRecords: "Nenhum registro disponível.",
                    info: "Exibindo pagina _PAGE_ de _PAGES_",
                    search: "Filtro _INPUT_",
                    paginate: {
                        "next": ">",
                        "previous": "<"
                    }
                }
            });
            
            tble.columns().every( function () {
                var that = this;
                $( 'input', this.header() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that.search( this.value ).draw();
                }
                });
            });

            setTimeout(function(){window.dispatchEvent(new Event('resize'))},300)  
            
            $(document).on('show.bs.modal', '.modal', function (event) {
                var zIndex = 1040 + (10 * $('.modal:visible').length);
                $(this).css('z-index', zIndex);
                setTimeout(function() {
                    $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                }, 0);
            });
            $('.page-hover').fadeOut();
        }
    }
}

app.getReferenceAvalitionForCordinator = async function(data, typef){

    if(document.querySelector('.ck.ck-reset.ck-editor.ck-rounded-corners')){
        document.querySelector('.ck.ck-reset.ck-editor.ck-rounded-corners').remove();
    }

    
    document.getElementById('cordinator-avaliation-history').innerHTML = ''

    var avaliatorsFinal = [];

    var avaliationPoints = {
        coerenceOfContent : 0,
        experimentalismOfProduct : 0,
        productQuality : 0
    }

    var divider = 0;


    const avaliationsByAvaliator = {}

    for (let index = 0; index < data.length; index++) {
        const avaliation = data[index];
        if(typeof(avaliationsByAvaliator[avaliation.customerId]) != 'object'){
            avaliationsByAvaliator[avaliation.customerId] = []
        }
        avaliationsByAvaliator[avaliation.customerId].push(avaliation)
    }

   for (const key in avaliationsByAvaliator) {
        
        if (Object.hasOwnProperty.call(avaliationsByAvaliator, key)) {
            
            const avaliator = avaliationsByAvaliator[key];

            var avaliation = await avaliator.reduce((a, b) => {
                return new Date(a.date) > new Date(b.date) ? a : b;
            })
            
            
            if(typeof(avaliation.defaultWorkOf) == 'string' && avaliation.defaultWorkOf.length > 0 && typef == 'undefined'){
                var avaliatorStatus = {
                    accept: 'Aceito',
                    alterationNeed: 'Alterações solicitadas',
                    refused: 'Recusado'
                }

                var typeOfRadio = {
                    yes: 'Sim',
                    not: 'Não'
                }

                document.getElementById('cordinator-avaliation-history').innerHTML += `
                    <div class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1"><i class="fad fa-clipboard-list-check"></i> ${avaliation.customer.name}</h5>
                            <small class="text-muted">${new Date(avaliation.date).toLocaleString()}</small>
                        </div>
                        <hr>
                        <small class="mb-1">
                            <strong>1 - O texto está no modelo-padrão do congresso?</strong> ${typeOfRadio[avaliation.nationalWorks[0]]}<br>
                            <strong>2 - O texto tem entre 10 e 15 páginas?</strong> ${typeOfRadio[avaliation.nationalWorks[1]]}<br>
                            <strong>3 - O texto apresenta, no início, título, nome do(s) autor(es), instituição a que pertence(m), um resumo de até 10 linhas e palavra-chave?</strong> ${typeOfRadio[avaliation.nationalWorks[2]]}<br>
                            <strong>4 - O texto apresenta a primeira nota de rodapé de acordo com a formação exigida no padrão do congresso?</strong> ${typeOfRadio[avaliation.nationalWorks[3]]}<br>
                            <strong>5 - Ao final do texto é apresentada a bibliografia de referência?</strong> ${typeOfRadio[avaliation.nationalWorks[4]]}<br>
                            <strong>6 - O texto está rigorosamente dentro das exigências acadêmicas: solidez teórica, relevância do tema, correção contextual e normas técnicas ABNT?</strong> ${typeOfRadio[avaliation.nationalWorks[5]]}<br>
                            <strong>7 - O texto apresenta contribuição resultante de pesquisa científica?</strong> ${typeOfRadio[avaliation.nationalWorks[6]]}<br>
                            <strong>8 - O texto é pertinente à emenda do GP?</strong> ${typeOfRadio[avaliation.nationalWorks[7]]}<br>
                            <strong>9 - Os objetivos do texto estão bem definidos?</strong> ${typeOfRadio[avaliation.nationalWorks[8]]}<br>
                            <strong>10 - O texto apresenta fundamentação teórica adequada e atualizada?</strong> ${typeOfRadio[avaliation.nationalWorks[9]]}<br>
                            <strong>11 - O texto está metodicamente organizado?</strong> ${typeOfRadio[avaliation.nationalWorks[10]]}<br>
                        </small>
                        <hr>
                            <strong>O trabalho foi:</strong> ${avaliatorStatus[avaliation.defaultWorkOf]}
                        <hr>
                        <small class="text-muted text-bold">Observações:</small>
                        <div class="container">
                            ${avaliation.descriptionAvaliationWorkAvaliator}
                        </div>
                    </div>
                `;

            } else {
                avaliationPoints.coerenceOfContent +=  +avaliation.coerenceOfContent;
                avaliationPoints.experimentalismOfProduct += +avaliation.experimentalismOfProduct;
                avaliationPoints.productQuality += +avaliation.productQuality;
                divider++; 
                avaliatorsFinal.push(avaliation);
                document.getElementById('cordinator-avaliation-history').innerHTML += `
                    <div class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1"><i class="fad fa-clipboard-list-check"></i> ${avaliation.customer.name}</h5>
                            <small class="text-muted">${new Date(avaliation.date).toLocaleString()}</small>
                        </div>
                        <hr>
                        <small class="mb-1">
                            <strong>O experimentalismo do produto:</strong> ${avaliation.experimentalismOfProduct}<br>
                            <strong>A qualidade técnica do produto:</strong> ${avaliation.productQuality}<br>
                            <strong>A consistência teórica e coerência do conteúdo inserido no formulário-padrão com o respectivo produto:</strong> ${avaliation.coerenceOfContent}<br>
                        </small>
                        <hr>
                        <small class="text-muted text-bold">Observações:</small>
                        <div class="container">
                            ${avaliation.descriptionAvaliationWorkAvaliator}
                        </div>
                    </div>
                `;
            }

            
       }

   }

    for (const key in avaliationPoints) {
        if (Object.hasOwnProperty.call(avaliationPoints, key)) {
            avaliationPoints[key] = avaliationPoints[key] / divider
        }
    }

   return {
       notes: avaliationPoints,
       avalations: avaliatorsFinal
   }
}

app.loadWorkCordinatorDataModal = async function(modalId, workId){
    
    for (let index = 0; index < app.lists.workCordinationList.length; index++) {
        const oWork = app.lists.workCordinationList[index];
        if(oWork.id == workId){
            const oWork = app.lists.workCordinationList[index];

            document.getElementById('cordinator-avaliation-history').innerHTML = 'Este trabalho ainda não possui avaliações.';

            var el = document.getElementsByName(`defaultCordinatorWorkOf`);
            for(var idx=0;idx<el.length;idx++)
                el[idx].checked = false;


            var el = document.getElementsByName(`defaultCordinatorWorkOfPresented`);
            for(var idx=0;idx<el.length;idx++)
                el[idx].checked = false;

            if(typeof(oWork.avaliations) == 'object' && oWork.avaliations instanceof Array && oWork.avaliations.length > 0){
                const aAvalations = await app.getReferenceAvalitionForCordinator(oWork.avaliations, typeof(oWork.oldCongressId));
                document.getElementById('experimentalismOfProductCordinator').value = aAvalations.notes.experimentalismOfProduct;
                document.getElementById('productQualityCordinator').value = aAvalations.notes.productQuality;
                document.getElementById('coerenceOfContentCordinator').value = aAvalations.notes.coerenceOfContent;
            }

            const categoryAndModule = oWork.category.split(' ');
            document.getElementById('discipline-2Cordinator').innerHTML = oWork.submitionWork.discipline;
            document.getElementById('currentYearCordinator').innerHTML = oWork.submitionWork.lectiveYear == 'yes' ? 'Sim' : 'Não';
            document.getElementById('congressIdWorkCordinator').value = oWork.congressId;
            document.getElementById('workIdWorkCordinator').value = oWork.id;
            document.getElementById('workNameCordinator').innerHTML = oWork.workName;
            document.getElementById('iesWorkCordinator').innerHTML = oWork.ies;
            document.getElementById('leaderNameCordinator').innerHTML = oWork.leaderName;
            document.getElementById('responsableTeatcherWorkCordinator').innerHTML = oWork.submitionWork.responsableTeatcher;
            document.getElementById('coAuthorsWorkCordinator').innerHTML = oWork.submitionWork.coAuthors;
            document.getElementById('studyObjectWorkCordinator').innerHTML = oWork.submitionWork.studyObject;
            document.getElementById('researchRealizedWorkCordinator').innerHTML = oWork.submitionWork.researchRealized;
            document.getElementById('productionDescriptionWorkCordinator').innerHTML = oWork.submitionWork.productionDescription;
            if(typeof(oWork.submitionWork.linkCloudWork) == 'string' && oWork.submitionWork.linkCloudWork.length > 0){
                document.getElementById('buttonDownloadMaterialWorkCordinator').style.display = 'block'
                document.getElementById('buttonDownloadMaterialWorkCordinator').href = oWork.submitionWork.linkCloudWork;
            } else {
                document.getElementById('buttonDownloadMaterialWorkCordinator').style.display = 'none'
            }
            document.getElementById('category-1Cordinator').innerHTML = categoryAndModule[0];
            document.getElementById('category-2Cordinator').innerHTML = `${categoryAndModule[0]} ${categoryAndModule[1]}`

            // console.log('Cordinator',app.lists.congresses[oWork.congressId].type)
            
            if( typeof(app.lists.congresses[oWork.congressId]) == 'object' && typeof(app.lists.congresses[oWork.congressId].type) == 'string' && app.lists.congresses[oWork.congressId].type == 'nacional' && typeof(oWork.oldCongressId) == 'undefined' ){

                if(typeof(oWork.descriptionAvaliationWorkCordinator) == "string" && oWork.descriptionAvaliationWorkCordinator.length > 0){
                    document.querySelector( '#descriptionAvaliationWorkCordinator' ).value = oWork.descriptionAvaliationWorkCordinator
                } else {
                    document.querySelector( '#descriptionAvaliationWorkCordinator' ).value = ''
                }
                
                document.getElementById('videos-from-work-cordinator').innerHTML = '';

                if(typeof(oWork.correctionVideo) == 'string' && oWork.correctionVideo.length > 0 ){
                    document.getElementById('videos-from-work-cordinator').innerHTML += `
                        <hr>
                        <a href="${oWork.correctionVideo}" class="btn btn-primary" target="_blank">
                            <i class="fad fa-download"></i> Link de Correção
                        </a>`;
                }

                if(typeof(oWork.correctionHistories) == 'object' && oWork.correctionHistories.length > 0 ){
                    oWork.correctionHistories.reverse();
                    for (let index = 0; index < oWork.correctionHistories.length; index++) {
                        const historyVid = oWork.correctionHistories[index];
                        document.getElementById('videos-from-work-cordinator').innerHTML += `
                            <hr>
                            <a href="${historyVid}" class="btn btn-primary" target="_blank">
                                <i class="fad fa-download"></i> Link de Correção
                            </a>`;
                    }
                }
                

                document.getElementById('submitButtonEnding').innerText = 'Salvar e Notificar'

                document.querySelectorAll('.cordinator-regional').forEach(e => {
                    e.style.display = 'none';
                })
                document.querySelectorAll('.cordinator-nacional').forEach(e => {
                    e.style.display = 'block';
                })


                document.getElementById("autorNameId").innerText = 'Autor(a):'
                // document.querySelectorAll('.avaliator-regional')
            } else {
                document.getElementById("autorNameId").innerText = 'Estudante Lider:'

                document.getElementById('submitButtonEnding').innerText = 'Finaliar Trabalho'

                document.querySelectorAll('.cordinator-nacional').forEach(e => {
                    e.style.display = 'none';
                })
                document.querySelectorAll('.cordinator-regional').forEach(e => {
                    e.style.display = 'block';
                })
                // document.querySelectorAll('.avaliator-resumes').style.display = 'none';
            }


            if(typeof(app.lists.congresses[oWork.congressId]) == 'object' && typeof(app.lists.congresses[oWork.congressId].type) == 'string' && app.lists.congresses[oWork.congressId].type == 'nacional'){
                document.querySelectorAll('.cordinator-work-presented').forEach(e => {
                    e.style.display = 'block';
                })
            } else {
                document.querySelectorAll('.cordinator-work-presented').forEach(e => {
                    e.style.display = 'none';
                })
            }


            if(document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners'))
                document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners').forEach(e => {e.remove()});



            ClassicEditor.create( document.querySelector( '#descriptionAvaliationWorkCordinator' ),{
                removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
                cloudServices: {
                tokenUrl: 'https://79675.cke-cs.com/token/dev/8d3071b981497c80e501930b07de8518dee6246738ef56f045b00ae10bd9',
                   uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
                }
            })
            .then( editor => {
                app.ckeditor.descriptionAvaliationWorkCordinator = editor ;
            })
            .catch( error => {
                console.error( error );
            });

            var formName = modalId.replace('Modal','');
            $(`#${formName} .formError`).hide();
            $(`#${formName} .formSuccess`).hide();
            $(`#${modalId}`).modal('show');
        } 
    }
}

app.loadUserDetailDataModal = async function(modalId, workId){
    
    for (let index = 0; index < app.lists.workCordinationList.length; index++) {
        const oWork = app.lists.workCordinationList[index];
        if(oWork.id == workId){
            const oWork = app.lists.workCordinationList[index];
            const categoryAndModule = oWork.category.split(' ');
            console.log(categoryAndModule, oWork)
            document.getElementById('discipline-2Detail').innerHTML = oWork.submitionWork.discipline;
            document.getElementById('currentYearDetail').innerHTML = oWork.submitionWork.lectiveYear == 'yes' ? 'Sim' : 'Não';
            // document.getElementById('congressIdWorkDetail').value = oWork.congressId;
            // document.getElementById('workIdWorkDetail').value = oWork.id;
            document.getElementById('workNameDetail').innerHTML = oWork.workName;
            document.getElementById('iesWorkDetail').innerHTML = oWork.ies;
            document.getElementById('leaderNameDetail').innerHTML = oWork.leaderName;
            document.getElementById('responsableTeatcherWorkDetail').innerHTML = oWork.submitionWork.responsableTeatcher;
            document.getElementById('coAuthorsWorkDetail').innerHTML = oWork.submitionWork.coAuthors;
            // document.getElementById('studyObjectWorkDetail').innerHTML = oWork.submitionWork.studyObject;
            // document.getElementById('researchRealizedWorkDetail').innerHTML = oWork.submitionWork.researchRealized;
            // document.getElementById('productionDescriptionWorkDetail').innerHTML = oWork.submitionWork.productionDescription;
            if(typeof(oWork.submitionWork.linkCloudWork) == 'string' && oWork.submitionWork.linkCloudWork.length > 0){
                document.getElementById('buttonDownloadMaterialWorkDetail').href = oWork.submitionWork.linkCloudWork;
            } else {
                document.getElementById('buttonDownloadMaterialWorkDetail').style.display = 'none'
            }
            
            document.getElementById('category-1Detail').innerHTML = categoryAndModule[0];
            document.getElementById('category-2Detail').innerHTML = `${categoryAndModule[0]} ${categoryAndModule[1]}`

            var formName = modalId.replace('Modal','');
            $(`#${formName} .formError`).hide();
            $(`#${formName} .formSuccess`).hide();
            $(`#${modalId}`).modal('show');
        } 
    }
}

app.showAvaliators = function(congressId, workId){
    
    $('.page-hover').fadeIn();
    $('.page-hover').css('zIndex','99999999');

    for (let index = 0; index < app.lists.workCordinationList.length; index++) {
        const work = app.lists.workCordinationList[index];
        if(work.id == workId){
            var token = typeof(app.user.token) == 'string' ? app.user.token : app.getData('token');
            if(token){
                const pld = {
                    'host' : window.location.host,
                    'access_token': token,
                    'congressId' : congressId,
                    'category' : work.category
                }

                app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Congress/Work/Cordinator/List/Avaliators','POST',undefined,pld,function(statusCode,rspObj){
                    if(statusCode == 200 && typeof(rspObj) == 'object' && rspObj){
                        $('.page-hover').fadeOut();
                        $('.page-hover').css('zIndex','2');                        
                        let showAvaliatorsHtml = '';

                        if(typeof(work.avaliators) == 'object' && work.avaliators instanceof Array && work.avaliators.length > 0){
                            showAvaliatorsHtml += '<ul class="list-group">';
                            for (let index = 0; index < rspObj.length; index++) {
                                const oAvaliator = rspObj[index];
                                if(typeof(oAvaliator) == 'object' && typeof(oAvaliator.customer) == 'object'){
                                    if(work.avaliators.indexOf(oAvaliator.customer.id) > -1){
                                        showAvaliatorsHtml +=  `<li class="list-group-item"><strong>${oAvaliator.customer.name}</strong></li>`
                                    }
                                }
                            }
                            showAvaliatorsHtml +=  '</ul>'

                            
                            Swal.fire({
                                title: 'Avaliadores',
                                icon: '',
                                html: showAvaliatorsHtml,
                                showCloseButton: false,
                                showCancelButton: false,
                                showConfirmButton: false,
                                focusConfirm: false
                            })
                        } else {
                            showAvaliatorsHtml = 'No momento este trabalho não possui avaliadores.'
                            Swal.fire({
                            icon: 'warning',
                            html: showAvaliatorsHtml,
                            showCloseButton: false,
                            showCancelButton: false,
                            showConfirmButton: false,
                            focusConfirm: false
                            })
                        }
                    }
                })
            }
        }
    }
}

app.compare = function( a, b ) {
    if(typeof(a.customer) == 'object' && typeof(a.customer.name) == 'string' &&  typeof(b.customer) == 'object' &&  typeof(b.customer.name) == 'string' ){
        if ( a.customer.name < b.customer.name ){
        return -1;
        }
        if ( a.customer.name > b.customer.name ){
        return 1;
        }
        return 0;
    }
    return 0;
}

app.loadCordinatorSetAvaliation = function(congressId, workId){

    $('.page-hover').fadeIn();
    $('.page-hover').css('zIndex','99999999')

    for (let index = 0; index < app.lists.workCordinationList.length; index++) {
        const work = app.lists.workCordinationList[index];
        if(work.id == workId){
            var token = typeof(app.user.token) == 'string' ? app.user.token : app.getData('token');
            if(token){
                const pld = {
                    'host' : window.location.host,
                    'access_token': token,
                    'congressId' : congressId,
                    'category' : work.category
                }

                app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Congress/Work/Cordinator/List/Avaliators','POST',undefined,pld,function(statusCode,rspObj){
                    if(statusCode == 200 && rspObj){
                        let dataHtml = '';
        
                        //console.log('STARTING', rspObj)

                        rspObj.sort(app.compare);


                        for (let index = 1; index <= 3; index++) {
                            dataHtml += `<select name="avalWork-${index}" id="avalWork-${index}" class="custom-select mb-3" >`
                            dataHtml += `<option selected>Selecione...</option>`;
                            for (let index = 0; index < rspObj.length; index++) {
                                const avaliator = rspObj[index];
                                if(typeof(avaliator.customer) == 'object' &&  typeof(avaliator.customer.name) == 'string'){
                                    dataHtml += `<option value="${avaliator.id}">${ avaliator.customer.name } - ${avaliator.ies}</option>`;
                                }
                            }
                            dataHtml += `</select>`;
                        }
                        $('.page-hover').fadeOut();
                        Swal.fire({
                            title: '<strong>Adicionar Avaliador</strong>',
                            icon: 'info',
                            html:  `
                            <div class="col-lg-12">
                                <label for="avalWork">Selecione o convidado</label>
                                ${dataHtml}
                            </div>
                            `,
                            showCloseButton: true,
                            showCancelButton: true,
                            focusConfirm: false,
                            confirmButtonText:
                            '<i class="fa fa-thumbs-up"></i> Adicionar',
                            confirmButtonAriaLabel: 'Thumbs up, add!',
                            cancelButtonText:
                            '<i class="fa fa-thumbs-down"></i>',
                            cancelButtonAriaLabel: 'Thumbs down'
                        }).then(e => {
                            if(e.value){
                                const payloadObj = {
                                    'host' : window.location.host,
                                    'access_token': token,
                                    'congressId' : congressId,
                                    'avaliatorId1' : $( "#avalWork-1 option:selected" ).val(),
                                    'avaliatorId2' : $( "#avalWork-2 option:selected" ).val(),
                                    'avaliatorId3' : $( "#avalWork-3 option:selected" ).val(),
                                    'workId' : workId
                                }
                                $('.page-hover').fadeIn();
                                app.client.request(undefined,endpoint+'/api/Admins/Institution/Congress/Work/Aval','POST',undefined,payloadObj,function(statusCode,responsePayload){
                                    if(statusCode == 200){
                                        $('.page-hover').fadeOut();
                                        $('.page-hover').css('zIndex','2')
                                        Swal.fire(
                                            'Uhu',
                                            'Avaliador adicionado com sucesso.',
                                            'success'
                                        )
                                    }
                                })
                            }
                        })
                    }
                })
            }
        }
    }
}

app.loadWorksForCordination = async function(id){
    $('#workAvaliationModal').modal('show')
    app.lists.workCordinationList = [];
    for (let index = 0; index < app.institution.congresses.length; index++) {
        const oCongress = app.institution.congresses[index];
        if(oCongress.id == id){
            const categs = app.user.cordinatored[oCongress.id].categs;

            document.getElementById('sendMessageAvaliatorButton').setAttribute('congress',`${oCongress.id}`)

            // Show each created check as a new row in the table
            var dataSet = [];

            document.getElementById('sendAvaliationButton').setAttribute('congress',oCongress.id)


            if( typeof(oCongress.type) == 'string' && oCongress.type == 'nacional'){
                document.querySelectorAll('.cordinator-list-regional').forEach(e => {
                    e.style.display = 'none';
                })
                document.querySelectorAll('.cordinator-list-nacional').forEach(e => {
                    e.style.display = 'inline-block';
                })
                document.getElementById("autorNameTh").innerText = "Autor"
                // document.querySelectorAll('.avaliator-regional')
            } else {
                document.getElementById("autorNameTh").innerText = "Lider"

                document.querySelectorAll('.cordinator-list-nacional').forEach(e => {
                    e.style.display = 'none';
                })
                document.querySelectorAll('.cordinator-list-regional').forEach(e => {
                    e.style.display = 'inline-block';
                })
                // document.querySelectorAll('.avaliator-resumes').style.display = 'none';
            }

            
            const dateCert = new Date(oCongress.dateCert)
            const today = new Date()

            if(today > dateCert){
                document.querySelector('.lo-footer-congressman').style.display = 'none';
                document.querySelector('.lo-footer-avaliator').style.display = 'none';
                document.querySelector('.lo-footer-coordinator').style.display = 'block';
                document.getElementById('certificateCordinator').setAttribute('congress',oCongress.id)
            }
                
            for (let index = 0; index < oCongress.works.length; index++) {
                const element = oCongress.works[index];
                const workCateg = element.category.split(' ')
                if(typeof(element.submitionWork) == 'object' && element.submitionWork &&  typeof(element.submitionWork.workId) == 'string' && ( categs.indexOf(`${workCateg[0]} ${workCateg[1]}`) > -1 || categs.indexOf(element.category) > -1 )){
                    
                    var media = false;
                    if(typeof(element.coerenceOfContent) == 'number' && typeof(element.experimentalismOfProduct) == 'number' && typeof(element.productQuality) == 'number'){
                        media = (element.coerenceOfContent + element.experimentalismOfProduct + element.productQuality) / 3; 
                    }
                    var avaliations = 0;
                    if (typeof(element.avaliations) == 'object' && element.avaliations instanceof Array && element.avaliations.length > 0) {
                        const avaliationsByAvaliator = {}

                        for (let index = 0; index < element.avaliations.length; index++) {
                            const avaliation = element.avaliations[index];
                            if(typeof(avaliationsByAvaliator[avaliation.customerId]) != 'object'){
                                avaliationsByAvaliator[avaliation.customerId] = []
                            }
                            avaliationsByAvaliator[avaliation.customerId].push(avaliation)
                        }

                        for (const key in avaliationsByAvaliator) {
                            if (Object.hasOwnProperty.call(avaliationsByAvaliator, key)) {
                                const avaliator = avaliationsByAvaliator[key];
                                var avaliation = await avaliator.reduce((a, b) => {
                                    return new Date(a.date) > new Date(b.date) ? a : b;
                                })
                                avaliations++;
                            }
                        }

                    }

                    const categoryAndModule = element.category.split(' ');
                    const child = [];
                    child[0] = app.lists.iesByName[element.ies] && app.lists.iesByName[element.ies].region ? app.lists.iesByName[element.ies].region : 'Não preenchido';
                    child[1] = `${categoryAndModule[0]} ${categoryAndModule[1]}`;
                    child[2] = element.workName.substring(0,22) || '';
                    child[3] = element.leaderName.substring(0,18) || '';
                    child[4] = app.lists.iesByName[element.ies] && app.lists.iesByName[element.ies].sigla ? app.lists.iesByName[element.ies].sigla : 'Não preenchido';
                    child[5] = typeof(element.avaliations) == 'object' && avaliations > 0 ? `<span class="text-success"><i class="fad fa-check-circle"></i> Avaliações <span class="badge badge-success">${avaliations}</span></span>`  : '<span class="text-warning"><i class="fad fa-exclamation-triangle"></i> Não avaliado</span>';
                    
                    if( typeof(oCongress.type) == 'string' && oCongress.type == 'nacional' && typeof(element.oldCongressId) == 'undefined'){
                        var nationalWorkStatus = {
                            accept : '<span class="text-success"><i class="fad fa-file-check"></i> Aceito</span>',
                            alterationNeed : '<span class="text-dark"><i class="fad fa-file-signature"></i> Alterações solicitadas</span>',
                            refused : '<span class="text-danger"><i class="fas fa-file"></i> Recusado</span>'
                        }

                        if( typeof(element.correctionVideo) == 'string' && element.correctionVideo.length > 0 && typeof(element.defaultCordinatorWorkOf) == 'string' && element.defaultCordinatorWorkOf == 'alterationNeed'){
                            child[6] = '<span class="text-primary"><i class="fas fa-sync"></i> Reavaliar</span>'
                        } else {
                            child[6] = typeof(element.defaultCordinatorWorkOf) == 'string' && element.defaultCordinatorWorkOf.length > 0 ? nationalWorkStatus[element.defaultCordinatorWorkOf] : '<span class="text-warning"><i class="fad fa-file-times"></i> Não validado</span>';
                        }      

                    }else{
                        child[6] = typeof(element.status) == 'string' && element.status == 'declassified' ? '<span class="text-danger"><i class="fad fa-file-times"></i> Desclassificado</span>' : media ? '<span class="text-success"><i class="fad fa-file-check"></i> Validado</span>' : '<span class="text-warning"><i class="fad fa-file-times"></i> Não validado</span>';
                    }
                    child[7] = typeof(element.average) == 'string' || typeof(element.average) == 'number' ? element.average.toFixed(2) : ( typeof(oCongress.type) == 'string' && oCongress.type == 'nacional' && media ? media.toFixed(2) : '-'  );
                    child[8] = `
                        <button type="button" class="btn btn-success" onClick="app.loadCordinatorSetAvaliation('${oCongress.id}','${element.id}')" data-toggle="tooltip" data-placement="left" title="Atribuir Avaliador ao Trabalho">
                            <i class="fad fa-clipboard-list-check"></i>
                        </button>
                        <button type="button" class="btn btn-warning" onClick="app.showAvaliators('${oCongress.id}','${element.id}')" data-toggle="tooltip" data-placement="left" title="Visualizar Avaliadores do Trabalho">
                            <i class="fas fa-list-alt"></i>
                        </button>
                        <button type="button" class="btn btn-primary" onClick="app.loadWorkCordinatorDataModal('workCordinationCordinatorModal', '${element.id}')"  data-toggle="tooltip" data-placement="left" title="Validar Avaliações">
                            <i class="fas fa-clipboard-check"></i>
                        </button>
                        ${typeof(oCongress.type) == 'string' && oCongress.type == 'nacional' ? `<button type="button" class="open-modal btn btn-info" onClick="app.sendMessageAvaliatorsNacional('workAvaliationModal','${element.id}','${oCongress.id}')"  data-toggle="tooltip" data-placement="left" title="Comunicar Avaliadores"><i class="fad fa-envelope"></i></button>` : '' }
                        ${typeof(oCongress.type) == 'string' && oCongress.type == 'regional' ? `<button type="button" class="btn btn-danger" onClick="app.inactiveWork('workAvaliationModal', '${element.id}')"  data-toggle="tooltip" data-placement="left" title="Desclassificar Trabalho"><i class="fad fa-folder-times"></i></button>` : '' }
                        ${typeof(oCongress.type) == 'string' && oCongress.type == 'nacional' && typeof(element.presentationVideo) == 'string' && element.presentationVideo.length > 0 ? `<a class="btn btn-dark" href="${element.presentationVideo}" target="_blank" data-toggle="tooltip" data-placement="left" title="Video de apresentação"><i class="fas fa-play-circle"></i></a> `  : '' } 
                        ${typeof(element.result) == 'string' && element.result == 'approved' && typeof(element.presentationVideo) == 'string' && element.presentationVideo.length > 0 ? `<a class="btn btn-dark" href="${element.presentationVideo}" target="_blank" data-toggle="tooltip" data-placement="left" title="Video de apresentação"><i class="fas fa-play-circle"></i></a> `  : '' } 
                        `;
                    app.lists.workCordinationList.push(element)
                    dataSet.push(child); 
                    
                }
            }
       
            if ( $.fn.dataTable.isDataTable( '#tableWorks' ) ) {
                let localTable = $('#tableWorks').DataTable();
                localTable.destroy();
            }
              
            $('#tableWorks thead th').each( function () {
                var title = $(this).text();
                if(title != "Ações"){
                    $(this).html( `
                        <label style="display: none">${title.trim()}</label>
                        <input type="text" class="form-control" placeholder="${title.trim()}">
                    ` );
                }
            });
        
            var tble = $('#tableWorks').DataTable({
                scrollX: true,
                lengthChange: true,
                buttons: [ 'excel', 'pdf' ],
                data: dataSet,
                columnDefs: [
                    
                ],
                order: [[ 1, "asc" ]],
                language: {
                    lengthMenu: "Exibindo _MENU_  ",
                    zeroRecords: "Nenhum registro disponível.",
                    info: "Exibindo pagina _PAGE_ de _PAGES_",
                    search: "Filtro _INPUT_",
                    paginate: {
                        "next": ">",
                        "previous": "<"
                    }
                }
            });
            
            tble.columns().every( function () {
                var that = this;
                $( 'input', this.header() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that.search( this.value ).draw();
                }
                });
            });
            
            setTimeout(function(){window.dispatchEvent(new Event('resize')); $('[data-toggle="tooltip"]').tooltip() },300)  
            
            $(document).on('show.bs.modal', '.modal', function (event) {
                var zIndex = 1040 + (10 * $('.modal:visible').length);
                $(this).css('z-index', zIndex);
                setTimeout(function() {
                    $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                }, 0);
            });

            $('.page-hover').fadeOut();

        }
    }
}

app.getWorkById = function(workId){
    for (let index = 0; index < app.lists.works.length; index++) {
        const oWork = app.lists.works[index];
        if(oWork.id == workId){
            return oWork
        } 
    }
}

app.submitPublicomLink = function(congressId, bookId){
    $(`#bookListModal`).modal('hide');

    var token = typeof(app.user.token) == 'string' ? app.user.token : app.getData('token');

    Swal.fire({
        title: '<strong>Enviar link de apresentação</strong>',
        icon: 'info',
        html:  `
        <div class="col-lg-12">
            <label for="bookLinkUrl">Preencher com url do livro:</label>
            <input class="form-control" id="bookLinkUrl" />
        </div>
        `,
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Enviar',
        confirmButtonAriaLabel: 'Thumbs up, send!',
        cancelButtonText:
        '<i class="fa fa-thumbs-down"></i>',
        cancelButtonAriaLabel: 'Thumbs down'
    }).then(e => {
        if(e.value){
            const payloadObj = {
                'host' : window.location.host,
                'access_token': token,
                congressId,
                'bookLinkUrl' : $( "#bookLinkUrl" ).val(),
                bookId
            }
            $('.page-hover').fadeIn();
            app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Congress/Publicom/Link','POST',undefined,payloadObj,function(statusCode,responsePayload){
                if(statusCode == 200){
                    $('.page-hover').fadeOut();
                    Swal.fire(
                        'Uhu',
                        'Link enviado com sucesso.',
                        'success'
                    ).then(e => {
                        if(e.value){
                            window.location.reload()
                        }
                    })
                }
            })
        } else {
            $(`#bookListModal`).modal('show');
        }
    })
}

app.workSuccessSubmtion = function(workId, congressId){
    var token = typeof(app.user.token) == 'string' ? app.user.token : app.getData('token');

    Swal.fire({
        title: '<strong>Enviar vídeo de apresentação</strong>',
        icon: 'info',
        html:  `
        <div class="col-lg-12">
            <label for="videoUrl">Preencher com url do vídeo de apresentação do trabalho:</label>
            <input class="form-control" id="videoUrl" />
        </div>
        `,
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Enviar',
        confirmButtonAriaLabel: 'Thumbs up, send!',
        cancelButtonText:
        '<i class="fa fa-thumbs-down"></i>',
        cancelButtonAriaLabel: 'Thumbs down'
    }).then(e => {
        if(e.value){
            const payloadObj = {
                'host' : window.location.host,
                'access_token': token,
                'congressId' : congressId,
                'videoUrl' : $( "#videoUrl" ).val(),
                'workId' : workId
            }
            $('.page-hover').fadeIn();
            app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Congress/Work/VideoUrl','POST',undefined,payloadObj,function(statusCode,responsePayload){
                if(statusCode == 200){
                    $('.page-hover').fadeOut();
                    Swal.fire(
                        'Uhu',
                        'Video enviado com sucesso.',
                        'success'
                    ).then(e => {
                        if(e.value){
                            window.location.reload()
                        }
                    })
                }
            })
        }
    })
}

app.workAlterationNeedSubmtion = function(workId, congressId){
    var token = typeof(app.user.token) == 'string' ? app.user.token : app.getData('token');

    Swal.fire({
        title: '<strong>Revisão do artigo</strong>',
        icon: 'info',
        html:  `
        <div class="col-lg-12">
            <label for="videoUrl">Cole aqui o link para a versão corrigida do artigo:</label>
            <input class="form-control" id="videoUrl" />
        </div>
        `,
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Enviar',
        confirmButtonAriaLabel: 'Thumbs up, send!',
        cancelButtonText:
        '<i class="fa fa-thumbs-down"></i>',
        cancelButtonAriaLabel: 'Thumbs down'
    }).then(e => {
        if(e.value){
            const payloadObj = {
                'host' : window.location.host,
                'access_token': token,
                'congressId' : congressId,
                'videoUrl' : $( "#videoUrl" ).val(),
                'workId' : workId
            }
            $('.page-hover').fadeIn();
            app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Congress/Work/Correction','POST',undefined,payloadObj,function(statusCode,responsePayload){
                if(statusCode == 200){
                    $('.page-hover').fadeOut();
                    Swal.fire(
                        'Uhu',
                        'Correção enviada com sucesso.',
                        'success'
                    ).then(e => {
                        if(e.value){
                            window.location.reload()
                        }
                    })
                }
            })
        }
    })
}

app.loadWorkStatus = function(workId){
    const work = app.getWorkById(workId);

    console.log('Carregando Status do Trabalho',work)

    let indication;
    

    if(typeof(work.congressType) == 'string' && work.congressType == 'nacional'){
        indication = '';
    } else {
        indication = `
            <li class="list-group-item d-flex flex-column-reverse text-left">
                <div>
                    Indicação do trabalho
                </div>
                <div class="date-on-history bg-primary">
                    <span class="badge badge-primary badge-pill">${new Date(work.date).toLocaleString()}</span>
                </div>
            </li>
        `
    }

    let submition = `
        <li class="list-group-item d-flex flex-column-reverse text-left">
            <div>
                Submissão do trabalho
            </div>
            <div class="date-on-history bg-primary">
                <span class="badge badge-primary badge-pill">${new Date(work.submitionWork.date).toLocaleString()}</span>
            </div>
        </li>
    `

    let avaliation = ''

    if(typeof(work.avaliations) == 'object' && work.avaliations instanceof Array && work.avaliations.length > 0 ){
        avaliation = `
            <li class="list-group-item d-flex flex-column-reverse text-left">
                <div>
                    Iniciando avaliações
                </div>
                <div class="date-on-history bg-primary">
                    <span class="badge badge-primary badge-pill">${new Date(work.avaliations[0].date).toLocaleString()}</span>
                </div>
            </li>
        `
    }
    
    
    let sendVideo =  '<br> O prazo de envio do vídeo de apresentação está encerrado.';
    //`<br><button class="btn btn-success btn-block mt-3" onclick="app.workSuccessSubmtion('${workId}','${work.congressId}')">Enviar apresentação</button>`

    if (typeof(work.presentationVideo) == 'string' && work.presentationVideo.length > 0) {
        sendVideo = '<br> Video já enviado.'        
    }

    let result = ''

    if(typeof(work.congressType) == 'string' && work.congressType == 'nacional'){
        
        let cordinatorDescription = typeof(work.descriptionAvaliationWorkCordinator) == 'string' && work.descriptionAvaliationWorkCordinator.length > 0 ? (work.descriptionAvaliationWorkCordinator.replaceAll('<p>','')).replaceAll('</p>','') : '';

        if(typeof(work.defaultCordinatorWorkOf) == 'string' && work.defaultCordinatorWorkOf == 'accept'){
            result = `
                <li class="list-group-item d-flex flex-column-reverse text-left">
                    <div>
                        O seu trabalho foi aceito.<br>
                        <button class="btn btn-primary" onClick="app.loadAcceptLetter('${work.congressId}','${work.id}')">Carta de aceite</button>    
                    </div>
                    <div class="date-on-history bg-primary">    
                        <span class="badge badge-primary badge-pill">${new Date(work.correctionDate).toLocaleString()}</span>
                    </div>
                </li>
            `  
        } 

        if(typeof(work.defaultCordinatorWorkOf) == 'string' && work.defaultCordinatorWorkOf == 'alterationNeed'){
            result = `
                <li class="list-group-item list-group-item-warning d-flex flex-column-reverse text-left">
                    <div>
                        
                        ${typeof(work.correctionVideo) == 'string' && work.correctionVideo.length > 0 ? 'Uma correção já foi enviada, aguarde a validação.' : `${cordinatorDescription.trim()}<br><button class="btn btn-primary " onclick="app.workAlterationNeedSubmtion('${workId}','${work.congressId}')"><i class="fas fa-cloud-upload-alt"></i> Atualizar</button>`}
                        
                    </div>
                    <div class="date-on-history bg-primary">    
                        <span class="badge badge-primary badge-pill">${new Date(work.correctionDate).toLocaleString()}</span>
                    </div>
                </li>
            ` 
        } 

        if(typeof(work.defaultCordinatorWorkOf) == 'string' && work.defaultCordinatorWorkOf == 'refused'){
            result = `
                <li class="list-group-item d-flex flex-column-reverse text-left">
                    <div>
                        ${cordinatorDescription.trim()}  
                    </div>
                    <div class="date-on-history bg-primary">    
                        <span class="badge badge-primary badge-pill">${new Date(work.resultDate).toLocaleString()}</span>
                    </div>
                </li>
            ` 
        }


        
    } else {
        if(typeof(work.result) == 'string' && work.result == 'approved'){
            result = `
                <li class="list-group-item d-flex flex-column-reverse text-left">
                    <div>
                        Você é um dos finalistas da modalidade. Parabéns!!!
                        ${sendVideo}    
                    </div>
                <div class="date-on-history bg-primary">    
                        <span class="badge badge-primary badge-pill">${new Date(work.resultDate).toLocaleString()}</span>
                    </div>
                </li>
            `


            // if(today > eventDay){

            //     result += `
            //         <button id="certificateAcceptedWork" type="button" class="open-modal btn btn-info certificate-button" congress="${work.congressId}">
            //             Certificado <i class="fad fa-file-certificate"></i>
            //         </button>
            //     `;
                

            //     console.log('Temos que mostrar o certificado final');
            // }
        }
    
        if(typeof(work.result) == 'string' && work.result == 'reproved'){
            result = `
                <li class="list-group-item d-flex flex-column-reverse text-left">
                    <div>
                        Seu trabalho não ficou entre os finalistas da modalidade. A Intercom agradece a sua participação.    
                    </div>
                    <div class="date-on-history bg-primary">    
                        <span class="badge badge-primary badge-pill">${new Date(work.resultDate).toLocaleString()}</span>
                    </div>
                </li>
            `
        }
    }


    
    


    if( (typeof(work.result) == 'string' && work.result.length > 0 && work.result == 'approved') || (typeof(work.defaultCordinatorWorkOfPresented) == 'string' && work.defaultCordinatorWorkOfPresented.length > 0 && work.defaultCordinatorWorkOfPresented == 'yes')){
        localStorage.setItem('targetCert','acceptedWorkCert')
        localStorage.setItem('congressCert', work.congressId)
        localStorage.setItem('workName', work.workName)
        localStorage.setItem('categoryName', work.category)
        
        avaliation = `
            <li class="list-group-item d-flex flex-column-reverse text-left">
                <div>
                    <a href="certificate.html" class="btn btn-primary">Certificado <i class="fad fa-file-certificate"></i></a>
                </div>
            </li>
        `
    }


    var dataHtml = `<p>Aqui você pode obter informações do seu trabalho.</p>
        <ul class="list-group">
            ${indication}
            ${submition}
            ${result}
            ${avaliation}
        </ul>
    `

    if(typeof(work.status) == 'string' && work.status == "declassified"){
        dataHtml = `<p>Aqui você pode obter informações do seu trabalho.</p>
            <div class="list-group">
                ${indication}
                ${submition}
                <div class="list-group-item list-group-item-danger list-group-item-action flex-column align-items-start">
                    <div class="d-flex w-100 justify-content-between">
                        <p class="mb-1">
                            Trabalho desclassificado: ${work.message}
                        </p>
                    </div>
                </div>
            </div>
        `
    }

    Swal.fire({
        title: '<strong>Detalhes do Trabalho</strong>',
        icon: 'info',
        html:  dataHtml,
        showCloseButton: true,
        showCancelButton: false,
        focusConfirm: false,
        confirmButtonText:'<i class="fa fa-thumbs-up"></i>',
        confirmButtonAriaLabel: 'Thumbs up!',
    })

    // if(loadAcceptedWork){
    //     document.querySelector('#certificateAcceptedWork').addEventListener('click', function() {  
    //         localStorage.setItem('targetCert','acceptedWorkCert')
    //         localStorage.setItem('congressCert',this.getAttribute('congress'))
    //         location.href = "certificate.html"
    //     })
    // }

}

app.loadAcceptLetter = function(congressId, workId){

    

    const oCongress = app.lists.congresses[congressId];

    for (let index = 0; index < app.lists.works.length; index++) {
        const oWork = app.lists.works[index];
        if(oWork.id == workId){
            console.log(oCongress, oWork)


            document.querySelectorAll('.work-name-content').forEach(e => {
                e.innerText = oWork.workName;
            })
        
            document.querySelectorAll('.autor-coautor-name-content').forEach(e => {
                e.innerText = oWork.leaderName + ' ' + oWork.submitionWork.coAuthors;
            })
        
            document.querySelectorAll('.congress-name-content').forEach(e => {
                e.innerText = oCongress.name;
            })
        
            document.querySelectorAll('.congress-date-content').forEach(e => {
                e.innerText = new Date(oWork.correctionDate).toLocaleString();
            })
        }
    }

    $('#acceptLetterModal').modal('show')

    


    
    
    
    
}

app.loadScheduleListt = function(congressId, modalPrefix){

    $(`#${modalPrefix}`).modal('show') 

    for (let index = 0; index < app.institution.congresses.length; index++) {
        const oCongress = app.institution.congresses[index];
        if(oCongress.id == congressId){
            // Show each created check as a new row in the table
            let dataStatic = [
                {
                    date: new Date("08/02/2021 16:30").toLocaleString('pt-BR'),
                    region: "Todas",
                    name: "Abertura + Conferência Manuela d'Ávila",
                    desc: "",
                    link: "https://www.youtube.com/watch?v=gOHI8qQ9Tvs"
                },
                {
                    date: new Date("08/02/2021 19:00").toLocaleString('pt-BR'),
                    region: "Todas",
                    name: "Festa de Premiação Ganhadores Expocom Norte",
                    desc: "(com participação da banda Sociedade de Esquina, de Boa Vista/RR)",
                    link: "https://www.youtube.com/watch?v=rr98Gchu3FE"
                },
                {
                    date: new Date("08/03/2021 16:30").toLocaleString('pt-BR'),
                    region: "CENTRO-OESTE",
                    name: "Mesa Universidades na resistência em tempos de pandemia",
                    desc: `Debatedores: Tamires Ferreira Coelho (UFMT/Projeto Pauta Gênero), Katarini Miguel (UFMS/Projeto
                        Plural), Elton Bruno Pinheiro (UnB/Lab Audio UnB) e Rosana Borges (UFG) – Mediação: Luan Chagas
                        (UFMT/Intercom)`,
                    link: "https://www.youtube.com/watch?v=sTeCLHqwDEA"
                },
                {
                    date: new Date("08/03/2021 16:30").toLocaleString('pt-BR'),
                    region: "NORDESTE",
                    name: `Mesa Articulações entre a pós-graduação e a graduação na construção de saberes críticos para a
                    área de comunicação no Nordeste`,
                    desc: `Debatedores: Izani Mustafá (UFMA), Edgard Patrício (UFC); Flávia Mayer (UFPB) e Zulmira Nóbrega
                    (UFPB) - Mediação: Paulo Fernando Lopes (UFPI)`,
                    link: "https://www.youtube.com/watch?v=Yi_eTKBYpMg"
                },
                {
                    date: new Date("08/03/2021 16:30").toLocaleString('pt-BR'),
                    region: "NORTE",
                    name: `Mesa 1 - Cultura e Pesquisa na Amazônia contemporânea: como ensinar sem cair em clichês e
                    repetições?`,
                    desc: `Debatedores: Vilso Junior Santi (UFRR) e Fernanda Ribeiro de Salvo (Ufac) - Mediação: Ana Paula
                    Vilhena (Estácio de Sá)`,
                    link: "https://teams.microsoft.com/l/meetup-join/19%3a-MCyaEDX6M6OKa0K1m-ZlNpFUCpkGQcqkDkpE_HDM9M1%40thread.tacv2/1627997075862?context=%7b%22Tid%22%3a%22da49a844-e2e3-40af-86a6-c3819d704f49%22%2c%22Oid%22%3a%221eef2290-e81f-4995-838c-3541621bcb2e%22%7d"
                },
                {
                    date: new Date("08/03/2021 16:30").toLocaleString('pt-BR'),
                    region: "NORTE",
                    name: `Mesa 2 - Liberdade ou exploração? Hipercapitalismo, empreendedorismo e “uberização” do
                    trabalho na Comunicação`,
                    desc: `Debatedores: Cynthia Mara Miranda (UFT) e Ramon Bezerra (UFMA/São Luiz) – Mediação:
                    Arcângela Sena (Estácio de Sá -Pará)`,
                    link: "https://teams.microsoft.com/l/meetup-join/19%3a-MCyaEDX6M6OKa0K1m-ZlNpFUCpkGQcqkDkpE_HDM9M1%40thread.tacv2/1627997173887?context=%7b%22Tid%22%3a%22da49a844-e2e3-40af-86a6-c3819d704f49%22%2c%22Oid%22%3a%221eef2290-e81f-4995-838c-3541621bcb2e%22%7d"
                },
                {
                    date: new Date("08/03/2021 16:30").toLocaleString('pt-BR'),
                    region: "SUDESTE",
                    name: "Palestra A comunicação na resistência ao movimento antivacina",
                    desc: "Convidada: Vanessa Veiga de Oliveira (UFMG) – Mediação: Conrado Moreira Mendes (PUC-MG)",
                    link: "https://youtu.be/33mn5eG2DHU"
                },
                {
                    date: new Date("08/03/2021 16:30").toLocaleString('pt-BR'),
                    region: "SUL",
                    name: "Mesa Comunicação, ética e mobilização social como resistência",
                    desc: `Debatedores: Karina Janz (UEPG), Rogério Cristofoletti (UFSC), Marcelo Träesel (PUC-RS) – Mediação:
                    Carlos Roberto Praxedes dos Santos (Univali)`,
                    link: "https://us02web.zoom.us/j/83610543811"
                },
                {
                    date: new Date("08/03/2021 19:00").toLocaleString('pt-BR'),
                    region: "Todas",
                    name: "Festa de Premiação Ganhadores Expocom Centro-Oeste",
                    desc: "(com participação de Márcio Camilo)",
                    link: "https://www.youtube.com/watch?v=uJVG8wttQBs"
                },
                {
                    date: new Date("08/04/2021 16:30").toLocaleString('pt-BR'),
                    region: "Todas",
                    name: "Mesa Experiências do ensino de Comunicação na pandemia",
                    desc: `Debatedores: Fábio Hansen (UFPR/Grupo IEP), Pedro Curcel (Grupo IEP), Mário Abel Bressan (Unisul)
                    – Mediação: André Tezza Consentino (UP)`,
                    link: "https://www.youtube.com/watch?v=3UHxLVegJOM"
                },
                {
                    date: new Date("08/04/2021 19:0").toLocaleString('pt-BR'),
                    region: "Todas",
                    name: "Festa de Premiação Expocom Sudeste",
                    desc: "(com participação do músico Arthur Rodrigues)",
                    link: "https://www.youtube.com/watch?v=8iKbOg0DN5o"
                },
                {
                    date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
                    region: "CENTRO-OESTE",
                    name: "Mesa Comunicação popular e resistência no Centro-Oeste",
                    desc: `Debatedores: Geremias dos Santos (Associação Brasileira de Rádios Comunitárias); Romilda Pizani
                    (Movimento Negro), João Negrão (Expresso 61) e Nilton José dos Reis Rocha (Coletivo Magnífica
                    Mundi) – Mediação: Lana Guedes (Unigran)`,
                    link: "https://www.youtube.com/watch?v=Ds2uAxfP-ec"
                },
                {
                    date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
                    region: "NORDESTE",
                    name: "Mesa 53 anos de “extensão ou comunicação” no centenário de Paulo Freire",
                    desc: `Debatedores: Giovana Mesquita (UFPE), Sheila Borges (UFPE), Eduardo Meditsch (UFSC) - Mediação:
                    Norma Meireles (UFPB)`,
                    link: "https://www.youtube.com/watch?v=tCONCNkqNpM"
                },
                {
                    date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
                    region: "NORTE",
                    name: "Mesa 1 - Conhecer, entender e resistir: Big Data e Análise de Dados na comunicação da Amazônia",
                    desc: `Debatedores: Lucas Milhomens (Ufam-Parintins /PPGCOM/UFRR) e Diogo Miranda (Estácio de Sá-Pará) - Mediação: Enderson Oliveira (Estácio de Sá -Pará)`,
                    link: "https://teams.microsoft.com/l/meetup-join/19%3a-MCyaEDX6M6OKa0K1m-ZlNpFUCpkGQcqkDkpE_HDM9M1%40thread.tacv2/1627997808722?context=%7b%22Tid%22%3a%22da49a844-e2e3-40af-86a6-c3819d704f49%22%2c%22Oid%22%3a%221eef2290-e81f-4995-838c-3541621bcb2e%22%7d"
                },
                {
                    date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
                    region: "NORTE",
                    name: `Mesa 2 - Tempos de pandemia, tempos de criatividade: fluxos e tendências para a Assessoria de Comunicação`,
                    desc: `Debatedores: Paulo Vitor Giraldi Pires (Unifap) e Israel Rocha (Ufam) - Mediação: Erika Siqueira (Estácio de Sá-Pará)`,
                    link: "https://teams.microsoft.com/l/meetup-join/19%3a-MCyaEDX6M6OKa0K1m-ZlNpFUCpkGQcqkDkpE_HDM9M1%40thread.tacv2/1627998010741?context=%7b%22Tid%22%3a%22da49a844-e2e3-40af-86a6-c3819d704f49%22%2c%22Oid%22%3a%221eef2290-e81f-4995-838c-3541621bcb2e%22%7d"
                },
                {
                    date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
                    region: "SUDESTE",
                    name: `Palestra Desinformação e cidadania nas
                    mídias sociais brasileiras`,
                    desc: `Convidada: Raquel Recuero (UFPel)
                    Mediação: Geane Carvalho Alzamora (UFMG)`,
                    link: "https://youtu.be/9HhUpn946A0"
                },
                {
                    date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
                    region: "SUL",
                    name: `Minicurso 1 Pesquisa jornalística: a construção de narrativas para o livro reportagem`,
                    desc: "Ministrante: Iago Porfório (UFMG)",
                    link: "https://us.bbcollab.com/guest/979070de36ae4ecab9cb91d1aff03dfb"
                },
                {
                    date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
                    region: "SUL",
                    name: `Minicurso 2 Produção de conteúdo`,
                    desc: "Ministrante: Joel Minusculi (Grupo ODP)",
                    link: "https://us.bbcollab.com/guest/4452d6a03fa246e6985eafde6039a20c"
                },
                {
                    date: new Date("08/05/2021 16:30").toLocaleString('pt-BR'),
                    region: "SUL",
                    name: `Minicurso 3 Realidade ampliada: exemplos e ferramentas básicas`,
                    desc: "Ministrante: Vinicius Batista de Oliveira (Univali)",
                    link: "https://us.bbcollab.com/guest/9e32209c730e446d8803366f2147c148"
                },
                {
                    date: new Date("08/05/2021 19:00").toLocaleString('pt-BR'),
                    region: "Todas",
                    name: `Festa de Premiação Expocom Nordeste`,
                    desc: "(com participação do músico Macvanny)",
                    link: "https://www.youtube.com/watch?v=qFXaxOoAiZw"
                },
                {
                    date: new Date("08/06/2021 16:30").toLocaleString('pt-BR'),
                    region: "Todas",
                    name: "Palestra de Encerramento Convidada Daniela Arbex",
                    desc: "",
                    link: "https://www.youtube.com/watch?v=TzylTvGgAzY"
                },
                {
                    date: new Date("08/06/2021 19:00").toLocaleString('pt-BR'),
                    region: "Todas",
                    name: "Festa de Premiação Expocom Sul",
                    desc: "",
                    link: "https://www.youtube.com/watch?v=pTeWQskRxXs"
                }
            ]

            var dataSet = [];
            app.lists.myBooks = [];
            for (let index = 0; index < dataStatic.length; index++) {
                const element = dataStatic[index];
                const child = [];
                    child[0] = element.date;
                    child[1] = element.region;
                    child[2] = ` <a href="${element.link}" class="btn btn-primary" target="_blank" data-toggle="tooltip" data-placement="left" title="Acessar Programação">
                                    <i class="fad fa-link"></i>
                                </a>`;
                    child[3] = element.name;
                    child[4] = element.desc;
                    
                    dataSet.push(child); 
            }
       
            if ( $.fn.dataTable.isDataTable( '#tableRegionalLinks' ) ) {
                let localTable = $('#tableRegionalLinks').DataTable();
                localTable.destroy();
            }
              
            $('#tableRegionalLinks thead th').each( function () {
                var title = $(this).text();
                if(title != "Ações"){
                    $(this).html( `
                        <label style="display: none">${title.trim()}</label>
                        <input type="text" class="form-control" placeholder="${title.trim()}">
                    ` );
                }
            });
        
            var tble = $('#tableRegionalLinks').DataTable({
                scrollX: true,
                lengthChange: true,
                buttons: [ 'excel', 'pdf' ],
                data: dataSet,
                columnDefs: [
                    
                ],
                order: [[ 0, "asc" ], [1, "asc"]],
                language: {
                    lengthMenu: "Exibindo _MENU_  ",
                    zeroRecords: "Nenhum registro disponível.",
                    info: "Exibindo pagina _PAGE_ de _PAGES_",
                    search: "Filtro _INPUT_",
                    paginate: {
                        "next": ">",
                        "previous": "<"
                    }
                }
            });
            
            tble.columns().every( function () {
                var that = this;
                $( 'input', this.header() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that.search( this.value ).draw();
                }
                });
            });
            
            setTimeout(function(){window.dispatchEvent(new Event('resize')); $('[data-toggle="tooltip"]').tooltip() },300)  
            
        }
    } 

} 

app.loadBookList = function(congressId, modalPrefix){

    app.targetCongress = congressId;

    $(`#${modalPrefix}`).modal('show') 

    var bookStatus = {
        waiting: '<span class="badge badge-warning p-2"><i class="fad fa-exclamation-circle"></i> Aguardando</span>',
        ok: '<span class="badge badge-success p-2"><i class="fad fa-check-circle"></i> Aceito</span>'
    }

    for (let index = 0; index < app.institution.congresses.length; index++) {
        const oCongress = app.institution.congresses[index];
        if(oCongress.id == congressId){
            // Show each created check as a new row in the table
            var dataSet = [];
            app.lists.myBooks = [];
            for (let index = 0; index < oCongress.books.length; index++) {
                const element = oCongress.books[index];
                const child = [];
                if(app.user.id == element.customerId){
                    child[0] = element.name;
                    child[1] = element.area;
                    child[2] = element.type;
                    child[3] = element.editor;
                    child[4] = typeof(element.status) == 'string' && element.status == 'ok'  ? `${bookStatus[element.status]} ${ typeof(element.linkSubmition) == 'string' && element.linkSubmition.length > 0 ? '<span class="badge badge-success p-2"><i class="fad fa-check-double"></i> Link Submetido</span>' : `<button class="btn btn-info btn-sm" onclick="app.submitPublicomLink('${oCongress.id}','${element.id}')"><i class="fad fa-cloud-upload-alt"></i> Submeter apresentação</button>` } ` : bookStatus[element.status];
                    app.lists.myBooks.push(element)
                    dataSet.push(child); 
                }
            }
       
            if ( $.fn.dataTable.isDataTable( '#tableBooks' ) ) {
                let localTable = $('#tableBooks').DataTable();
                localTable.destroy();
            }
              
            $('#tableBooks thead th').each( function () {
                var title = $(this).text();
                if(title != "Ações"){
                    $(this).html( `
                        <label style="display: none">${title.trim()}</label>
                        <input type="text" class="form-control" placeholder="${title.trim()}">
                    ` );
                }
            });
        
            var tble = $('#tableBooks').DataTable({
                scrollX: true,
                lengthChange: true,
                buttons: [ 'excel', 'pdf' ],
                data: dataSet,
                columnDefs: [
                    
                ],
                order: [[ 1, "asc" ]],
                language: {
                    lengthMenu: "Exibindo _MENU_  ",
                    zeroRecords: "Nenhum registro disponível.",
                    info: "Exibindo pagina _PAGE_ de _PAGES_",
                    search: "Filtro _INPUT_",
                    paginate: {
                        "next": ">",
                        "previous": "<"
                    }
                }
            });
            
            tble.columns().every( function () {
                var that = this;
                $( 'input', this.header() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that.search( this.value ).draw();
                }
                });
            });
            
            setTimeout(function(){window.dispatchEvent(new Event('resize')); $('[data-toggle="tooltip"]').tooltip() },300)  
            
            $(document).on('show.bs.modal', '.modal', function (event) {
                var zIndex = 1040 + (10 * $('.modal:visible').length);
                $(this).css('z-index', zIndex);
                setTimeout(function() {
                    $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                }, 0);
            });

        }
    } 

}

app.modalLoad = function(modalPrefix){
    
    document.getElementById('congressIdBookSubmition').value = app.targetCongress;

    $(`#${modalPrefix}`).modal('show') 

    if(document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners'))
        document.querySelectorAll('.ck.ck-reset.ck-editor.ck-rounded-corners').forEach(e => {e.remove()});
    
    ClassicEditor.create( document.querySelector( '#bookResume' ),{
        removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
        cloudServices: {
        tokenUrl: 'https://79675.cke-cs.com/token/dev/8d3071b981497c80e501930b07de8518dee6246738ef56f045b00ae10bd9',
                   uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
        }
    })
    .then( editor => {
        app.ckeditor.bookResume = editor ;
    })
    .catch( error => {
        console.error( error );
    });
}

app.inscriptionAction = function(congressId){
    const oCongress = typeof(app.lists.congresses[congressId]) == "object" ? app.lists.congresses[congressId] : false;
    
    if(oCongress){

        $('#workAvaliationModal').modal('show')

        app.lists.workCordinationList = [];
        
        // const categs = app.user.cordinatored[oCongress.id].categs;

        document.getElementById('sendMessageAvaliatorButton').setAttribute('congress',`${oCongress.id}`)

        // Show each created check as a new row in the table
        var dataSet = [];

        document.getElementById('sendAvaliationButton').setAttribute('congress',oCongress.id)

        document.querySelector('.lo-footer').style.display = 'none'
        

        const dateCert = new Date(oCongress.dateCert)
        const today = new Date()
        if(today > dateCert){
            document.querySelector('.lo-footer-congressman').style.display = 'block';
            document.getElementById('certificateCongressman').setAttribute('congress',oCongress.id)
            document.querySelector('.lo-footer-avaliator').style.display = 'none';
            document.querySelector('.lo-footer-coordinator').style.display = 'none';
        }

        
        for (let index = 0; index < oCongress.works.length; index++) {
            const element = oCongress.works[index];
            const workCateg = element.category.split(' ')
            if(typeof(element.submitionWork) == 'object' && element.submitionWork &&  typeof(element.submitionWork.workId) == 'string' && element.result == 'approved'){
               
                const categoryAndModule = element.category.split(' ');
                const child = [];
                child[0] = app.lists.iesByName[element.ies] && app.lists.iesByName[element.ies].region ? app.lists.iesByName[element.ies].region : 'Não preenchido';
                child[1] = `${categoryAndModule[0]} ${categoryAndModule[1]}`;
                child[2] = element.workName.substring(0,22) || '';
                child[3] = element.leaderName.substring(0,18) || '';
                child[4] = app.lists.iesByName[element.ies] && app.lists.iesByName[element.ies].sigla ? app.lists.iesByName[element.ies].sigla : 'Não preenchido';
                child[5] = '';
                child[6] = '';
                child[7] = '';
                child[8] = `
                    <button type="button" class="open-modal btn btn-primary" onClick="app.loadUserDetailDataModal('workDetailModal','${element.id}')"  data-toggle="tooltip" data-placement="left" title="Detalhes">
                        <i class="fas fa-clipboard-list"></i>
                    </button>
                    ${typeof(element.result) == 'string' && element.result == 'approved' && typeof(element.presentationVideo) == 'string' && element.presentationVideo.length > 0 ? `<a class="btn btn-dark" href="${element.presentationVideo}" target="_blank" data-toggle="tooltip" data-placement="left" title="Video de apresentação"><i class="fas fa-play-circle"></i></a> `  : '' } 
                    `;
                app.lists.workCordinationList.push(element)
                dataSet.push(child); 
                
            }
        }

        if ( $.fn.dataTable.isDataTable( '#tableWorks' ) ) {
            let localTable = $('#tableWorks').DataTable();
            localTable.destroy();
        }
        
        $('#tableWorks thead th').each( function () {
            var title = $(this).text();
            if(title != "Ações"){
                $(this).html( `
                    <label style="display: none">${title.trim()}</label>
                    <input type="text" class="form-control" placeholder="${title.trim()}">
                ` );
            }
        });
    
        var tble = $('#tableWorks').DataTable({
            scrollX: true,
            lengthChange: true,
            buttons: [ 'excel', 'pdf' ],
            data: dataSet,
            columnDefs: [
                {
                    "targets": [ 5 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [ 6 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [ 7 ],
                    "visible": false,
                    "searchable": false
                },    
            ],
            order: [[ 1, "asc" ],[ 0, "asc" ]],
            language: {
                lengthMenu: "Exibindo _MENU_  ",
                zeroRecords: "Nenhum registro disponível.",
                info: "Exibindo pagina _PAGE_ de _PAGES_",
                search: "Filtro _INPUT_",
                paginate: {
                    "next": ">",
                    "previous": "<"
                }
            }
        });
        
        tble.columns().every( function () {
            var that = this;
            $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that.search( this.value ).draw();
            }
            });
        });
        
        setTimeout(function(){window.dispatchEvent(new Event('resize')); $('[data-toggle="tooltip"]').tooltip() },300)  
        
        $(document).on('show.bs.modal', '.modal', function (event) {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        });

        $('.page-hover').fadeOut();

    }
}

app.loadEventsIns = function(){
    const oIns = app.institution;
    var inscripted = [];
    if(typeof(app.user.inscriptions) == 'object' && app.user.inscriptions instanceof Array && app.user.inscriptions.length > 0 ){
        for (let index = 0; index < app.user.inscriptions.length; index++) {
            const oInscription = app.user.inscriptions[index];
            if(typeof(oInscription.transaction) == 'object' && typeof(oInscription.transaction.status) == 'string' && oInscription.transaction.status == 'paid'){
                inscripted.push(oInscription.congressId)
            } else if(oInscription.type == 'associated'){
                inscripted.push(oInscription.congressId)
            }
        }
    }

    var avaliatored = {};
    if(typeof(app.user.avaliators) == 'object' && app.user.avaliators instanceof Array && app.user.avaliators.length > 0 ){
        for (let index = 0; index < app.user.avaliators.length; index++) {
            const avaliators = app.user.avaliators[index];
            avaliatored[avaliators.congressId] = avaliators;
        }
    }

    var worked = [];
    if(typeof(app.user.works) == 'object' && app.user.works instanceof Array && app.user.works.length > 0 ){
        for (let index = 0; index < app.user.works.length; index++) {
            const work = app.user.works[index];
            worked.push(work.customerId)
            worked.push(work.id)
        }
    }


    var cordinatored = {};
    if(typeof(app.user.cordinators) == 'object' && app.user.cordinators instanceof Array && app.user.cordinators.length > 0 ){
        for (let index = 0; index < app.user.cordinators.length; index++) {
            const cordinators = app.user.cordinators[index];
            cordinatored[cordinators.congressId] = cordinators;
            
        }
    }
    app.user.cordinatored = cordinatored;

    app.lists.iesByName = {};

    app.lists.categories = [];


    if(typeof(oIns) == 'object' && typeof(oIns.congresses) == 'object' && oIns.congresses instanceof Array && oIns.congresses.length > 0 ){

        app.lists.works = [];
        
        app.lists.workCordinationList = [];

        app.lists.congresses = {};
        

        for (let key = 0; key < oIns.congresses.length; key++) {
            const oCongress = oIns.congresses[key];

            app.lists.congresses[oCongress.id] = oCongress;

            for (let idx = 0; idx < oCongress.iesList.length; idx++) {
                const oIes = oCongress.iesList[idx];
                app.lists.iesByName[oIes.name] = oIes;
                
            }

            for (let idx = 0; idx < oCongress.categories.length; idx++) {
                const category = oCongress.categories[idx];
                app.lists.categories.push(category)
            }

            let submitWorkBtn = '';
            let cordinationBtn = '';
            let inscriptionBtn = new Date() > new Date(oCongress.dateEnd)  ? '' : `<button class="btn btn-outline-success ml-2" onClick="app.loadModalAction(${key},'inscription')"><i class="fad fa-plus-octagon"></i> Inscrever-se</button>`;
            let avaliationBtn = new Date() > new Date(oCongress.dateEnd)  ? '' : `<button class="btn btn-outline-warning ml-2" onClick="app.loadModalAction(${key},'avl')"><i class="fad fa-clipboard-list-check"></i> Ser Avaliador</button>`;
            let bookSubmition = '';
            let scheduleLinks = '';

            if(typeof(oCongress.type) == 'undefined' ||  oCongress.type == 'regional'){
                document.querySelector('.type-picker').style.display = 'none'
                document.querySelector('#inscriptionWorkAlumn').style.display = 'block'

                if(typeof(oCongress.works) == 'object' && oCongress.works instanceof Array && oCongress.works.length > 0){
                    for (let index = 0; index < oCongress.works.length; index++) {
                        const oWork = oCongress.works[index];
                        app.lists.works.push(oWork)
                        const leaderAlumnCpf = typeof(oWork.leaderDoc) == 'string' && oWork.leaderDoc.length > 10 ? oWork.leaderDoc.replace(/[\. ,:-]+/g, "") : false;
                        const userCpf        = typeof(app.user.cpf) == 'string' && app.user.cpf.length > 10 ? app.user.cpf.replace(/[\. ,:-]+/g, "") : false;
                        if(leaderAlumnCpf && userCpf){
                            if(leaderAlumnCpf == userCpf && worked.indexOf(oWork.id) < 0){
                                // submitWorkBtn  = `<button class="btn btn-outline-danger ml-2" onClick="app.loadModalAction(${key},'workinscription')"><i class="fad fa-file-upload"></i> Inscrever trabalho</button>`;
                                app.user[`congress_${oCongress.id}`] = {
                                    submitedWorkId: oWork.id
                                }
                            } else if(worked.indexOf(oWork.id) > -1){
                                submitWorkBtn  = `<button class="btn btn-danger ml-2" onClick="app.loadWorkStatus('${oWork.id}')"><i class="fad fa-file-upload"></i> Trabalho Inscrito</button>`;
                            }
                        } 
                    }
                }

                if(inscripted.indexOf(oCongress.id) > -1){
                    scheduleLinks = `<button class="btn btn-info ml-2" onClick="app.loadScheduleListt('${oCongress.id}', 'regionalListModal')"><i class="fad fa-calendar-check"></i> Programação</button>`;
                }

                
            }else if( oCongress.type == 'nacional' ){
                document.querySelector('.type-picker').style.display = 'block'
                document.querySelector('#inscriptionWorkAlumn').style.display = 'none'
                if(typeof(oCongress.works) == 'object' && oCongress.works instanceof Array && oCongress.works.length > 0){
                    for (let index = 0; index < oCongress.works.length; index++) {
                        const oWork = oCongress.works[index];
                        // console.log(worked, oWork.id)
                        oWork.congressType = 'nacional';
                        app.lists.works.push(oWork)
                        if( worked.indexOf(oWork.id) < 0 ){
                            // submitWorkBtn  = `<button class="btn btn-outline-danger ml-2" onClick="app.loadModalActionDiff('${oCongress.id}','workinscription')"><i class="fad fa-file-upload"></i> Inscrever trabalho</button>`;
                            // break;
                        } else if(worked.indexOf(oWork.id) > -1){
                            submitWorkBtn  = `<button class="btn btn-danger ml-2" onClick="app.loadWorkStatus('${oWork.id}')"><i class="fad fa-file-upload"></i> Trabalho Inscrito</button>`;
                            break;
                        } else if(worked.indexOf(app.user.id) > -1){
                            submitWorkBtn  = `<button class="btn btn-danger ml-2" onClick="app.loadWorkStatus('${oWork.id}')"><i class="fad fa-file-upload"></i> Trabalho Inscrito</button>`;
                            break;
                        }
                    }
                } else {
                    //console.log('Tamo Aew')
                    // submitWorkBtn  = `<button class="btn btn-outline-danger ml-2" onClick="app.loadModalActionDiff('${oCongress.id}','workinscription')"><i class="fad fa-file-upload"></i> Inscrever trabalho</button>`;
                }
                
                if(inscripted.indexOf(oCongress.id) > -1){
                    bookSubmition = `<button class="btn btn-info ml-2" onClick="app.loadBookList('${oCongress.id}', 'bookListModal')"><i class="fad fa-books-medical"></i> Publicom</button>`;
                }
                 
            }
            

            if(inscripted.indexOf(oCongress.id) > -1){
                
                var today = new Date();
                var eventDay = new Date(oCongress.dateEnd);
                
                console.log( "Já passou o evento?", today > eventDay)

                let congressUrl = `https://${oCongress.url}.uhub.live/view/core/account-creation.html`;
                if(oCongress.type == 'nacional' && today <= eventDay){
                    inscriptionBtn = `<a class="btn btn-success ml-2" href="${congressUrl}?access=${app.getData('token')}"><i class="fad fa-plus-octagon"></i> Inscrito</a>`;
                } else {
                    inscriptionBtn = `<button class="btn btn-success ml-2" onclick="app.inscriptionAction('${oCongress.id}')"><i class="fad fa-plus-octagon"></i> Inscrito</button>`;
                    if(oCongress.type == 'nacional'){
                        inscriptionBtn += `<a class="btn btn-primary ml-2" href="${congressUrl}?access=${app.getData('token')}"><i class="fad fa-door-open"></i> Evento</a>`;    
                    }
                }
            } else {
                submitWorkBtn = '';
            }
            
            if(typeof(avaliatored[oCongress.id]) == 'object' && avaliatored[oCongress.id].status == 'ok'){
                avaliationBtn = `<button class="btn btn-warning ml-2" onclick="app.loadWorksForAvaliation('${oCongress.id}')"><i class="fad fa-clipboard-list-check"></i> Avaliar Trabalhos</button>`;
            } else if(typeof(avaliatored[oCongress.id]) == 'object' && avaliatored[oCongress.id].status == 'waiting'){
                avaliationBtn = `<button class="btn btn-warning ml-2" ><i class="fad fa-clipboard-list-check"></i> Avaliador Cadastrado aguardando liberação</button>`;
            }

            if(typeof(cordinatored[oCongress.id]) == 'object'){
                cordinationBtn = `<button class="btn btn-primary ml-2" onclick="app.loadWorksForCordination('${oCongress.id}')"><i class="fas fa-user-cog"></i> Coordenação</button>`;
            }

            // //console.log(oCongress)

            var text = oCongress.resume;
            var regex = /(<([^>]+)>)/ig;
            text = text.replace(regex,"");
            $('#congress').append(`
                <div class="col-md-12">
                    <div class="card flex-md-row mb-4 shadow-sm h-md-250">
                        <div class="card-body d-flex flex-column align-items-start">
                            <strong class="d-inline-block mb-2 text-primary">Evento</strong>
                            <h3 class="mb-0">
                                ${oCongress.name}
                            </h3>
                            <div class="mb-1 text-muted">${new Date(oCongress.date).toLocaleString()}</div>
                            <p class="card-text mb-auto">${text.slice(0,105)}...</p>
                            <div class="d-flex flex-row multiline-mobile">
                                <button class="btn btn-dark first-btn" onClick="app.loadModalCongress(${key})"><i class="fad fa-angle-double-right"></i> Ler mais</button>
                                ${inscriptionBtn}
                                ${submitWorkBtn}
                                ${avaliationBtn}
                                ${cordinationBtn}
                                ${bookSubmition}
                                ${scheduleLinks}
                            </div>
                        </div>
                        <div class="card-img-right flex-auto d-none d-lg-block" style="width: 200px; height: 200px; background: url(${oCongress.topImageUrl}) center center / cover no-repeat" alt="Card image cap"></div>
                    </div>
                </div>
            `)
        }
    }
}

app.bindLoadCongresses = function(){
    $('#congress').html('');
    const  queryStringObj = { 
        host: window.location.host,
        access_token: app.getData('token')
    }
    app.client.request(undefined,endpoint+'/api/Authenticateds/Institution', 'GET', queryStringObj, undefined, function(statusCode,responsePayload){
        if(statusCode == 200 && responsePayload){
            var oIns = responsePayload;
            if(typeof(oIns.congresses) == 'object' && oIns.congresses instanceof Array && oIns.congresses.length > 0){
                app.institution = oIns;
                app.loadEventsIns();
            } else {
                $('#congress').append(`<h3>No momento não há eventos ativos.</h3>`)
            }
            $('[data-toggle="tooltip"]').tooltip()
            $('.page-hover').fadeOut();
        } 
    })
}

app.loadByClass = function(){
    // Get the current page from the body class
    var bodyClasses = document.querySelector("body").classList;
    var primaryClass = typeof(bodyClasses[0]) == 'string' ? bodyClasses[0] : false;

    if(primaryClass == "accountHome"){

        document.querySelector('#certificateCongressman').addEventListener('click', function() {  
            localStorage.setItem('targetCert','congressmanCert')
            localStorage.setItem('congressCert',this.getAttribute('congress'))
            localStorage.removeItem('workName');
            localStorage.removeItem('categoryName');
            location.href = "certificate.html"
        })
        
        document.querySelector('#certificateAvaliator').addEventListener('click', function() {  
            localStorage.setItem('targetCert','avaliatorCert')
            localStorage.setItem('congressCert',this.getAttribute('congress'))
            localStorage.removeItem('workName');
            localStorage.removeItem('categoryName');
            location.href = "certificate.html"
        })
        
        document.querySelector('#certificateCordinator').addEventListener('click', function() {  
            localStorage.setItem('targetCert','coordinatorCert')
            localStorage.setItem('congressCert',this.getAttribute('congress'))
            localStorage.removeItem('workName');
            localStorage.removeItem('categoryName');
            location.href = "certificate.html"
        })

        var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
        

        var tel = document.querySelector('#phone');
        VMasker(tel).maskPattern(telMask[0]);
        tel.addEventListener('input', app.inputHandler.bind(undefined, telMask, 14), false);

        var mobile = document.querySelector('#mobile');
        VMasker(mobile).maskPattern(telMask[0]);
        mobile.addEventListener('input', app.inputHandler.bind(undefined, telMask, 14), false);

        var professional_phone = document.querySelector('#professional_phone');
        VMasker(professional_phone).maskPattern(telMask[0]);
        professional_phone.addEventListener('input', app.inputHandler.bind(undefined, telMask, 14), false);

        

        var docMask = ['999.999.999-999'];
        var doc = document.querySelector('#cpf');
        VMasker(doc).maskPattern(docMask[0]);
        doc.addEventListener('input', app.inputHandler.bind(undefined, docMask, 14), false);

        var docRg = ['99.999.999-999999'];
        var doc = document.querySelector('#rg');
        VMasker(doc).maskPattern(docRg[0]);
        doc.addEventListener('input', app.inputHandler.bind(undefined, docRg, 14), false)

        const updateMessage = app.getFromQuery('upd');

        if(updateMessage == 'data' && document.querySelector('.information-about-update')){
            document.querySelector('.information-about-update').style.display = 'block'
            document.querySelector('.content-notice').innerHTML = `<i class="fad fa-check-circle"></i> Dados atualizados com sucesso.`
        }

        if(updateMessage == 'evn' && document.querySelector('.information-about-update')){
            document.querySelector('.information-about-update').style.display = 'block'
            document.querySelector('.content-notice').innerHTML = `<i class="fad fa-check-circle"></i> Sua inscrição foi realizada com sucesso, para verificar sua inscrição basta clicar no menu eventos abaixo e verificar se você está inscrito.`
        }

        var points = 0;
        document.querySelector('.user-picture').src = typeof(app.user.photoURL) == 'string' && app.user.photoURL.length > 0 ? app.user.photoURL : 'http://ssl.gstatic.com/accounts/ui/avatar_2x.png';
        document.querySelector('.user-name').innerText = app.user.displayName;

        document.querySelector('#name').value = app.user.displayName;
        document.querySelector('#mobile').value = app.user.phoneNumber;
        document.querySelector('#email').value = app.user.email;

        setTimeout(function(){


            const oQueryString = {
                access_token: app.getData('token')
            }
          
            app.client.request(undefined,endpoint+'/api/Authenticateds/User','GET',oQueryString,undefined,function(statusCode,responsePayload){
                if(statusCode == 200 && responsePayload){
                    $('.page-hover').fadeOut();

                    app.user = responsePayload;
                    // HERE WE LOAD ALL DATA FROM USER
                    const date = new Date(app.user.birthday);

                    document.querySelector('.user-name').innerText = app.user.name;
                    

                    document.querySelector('#name').value = app.user.name || '';
                    document.querySelector('#gender').value = app.user.gender || '';
                    document.querySelector('#birthday').value = `${date.getFullYear().toString()}-${(date.getMonth() + 1).toString().padStart(2, 0)}-${(date.getDate() + 1).toString().padStart(2, 0)}` || '';
                    document.querySelector('#cpf').value = app.user.cpf || '';
                    document.querySelector('#rg').value = app.user.rg || '';
                    document.querySelector('#phone').value = app.user.phone || '';
                    document.querySelector('#mobile').value = app.user.mobile || '';
                    document.querySelector('#email').value = app.user.email || '';
                    document.querySelector('#address').value = app.user.address || '';
                    document.querySelector('#address_number').value = app.user.address_number || '';
                    document.querySelector('#address_complement').value = app.user.address_complement || '';
                    document.querySelector('#address_neighborhood').value = app.user.address_neighborhood || '';
                    document.querySelector('#address_city').value = app.user.address_city || '';
                    document.querySelector('#address_state').value = app.user.address_state || '';
                    document.querySelector('#address_country').value = app.user.address_country || '';
                    document.querySelector('#postal_code').value = app.user.postal_code || '';
                    document.querySelector('#role').value = app.user.role || '';
                    document.querySelector('#institution_abreviation').value = app.user.institution_abreviation || '';
                    document.querySelector('#professional_phone').value = app.user.professional_phone || '';
                    document.querySelector('#professional_address').value = app.user.professional_address || '';
                    document.querySelector('#professional_address_complement').value = app.user.professional_address_complement || '';
                    document.querySelector('#professional_address_neighborhood').value = app.user.professional_address_neighborhood || '';
                    document.querySelector('#professional_address_city').value = app.user.professional_address_city || '';
                    document.querySelector('#professional_address_state').value = app.user.professional_address_state || '';
                    document.querySelector('#professional_address_country').value = app.user.professional_address_country || '';
                    document.querySelector('#professional_postal_code').value = app.user.professional_postal_code || '';
                    document.querySelector('#title').value = app.user.title || '';
                    document.querySelector('#degree_institution').value = app.user.degree_institution || '';
                    document.querySelector('#degree_course').value = app.user.degree_course || '';
                    document.querySelector('#year_conclusion_degree').value = app.user.year_conclusion_degree || '';
                    document.querySelector('#specialization_institution').value = app.user.specialization_institution || ''; 
                    document.querySelector('#specialization_course').value = app.user.specialization_course || ''; 
                    document.querySelector('#year_conclusion_specialization').value = app.user.year_conclusion_specialization || ''; 
                    document.querySelector('#master_institution').value = app.user.master_institution || ''; 
                    document.querySelector('#master_course').value = app.user.master_course || ''; 
                    document.querySelector('#year_conclusion_master').value = app.user.year_conclusion_master || ''; 
                    document.querySelector('#doc_institution').value = app.user.doc_institution || ''; 
                    document.querySelector('#doc_course').value = app.user.doc_course || ''; 
                    document.querySelector('#year_conclusion_doc').value = app.user.year_conclusion_doc || ''; 
                    document.querySelector('#phd_institution').value = app.user.phd_institution || ''; 
                    document.querySelector('#phd_course').value = app.user.phd_course || ''; 
                    document.querySelector('#year_conclusion_phd').value = app.user.year_conclusion_phd || ''; 

                    points += app.user.name ? 1 : 0;
                    points += app.user.birthday ? 1 : 0;
                    points += app.user.cpf ? 1 : 0;
                    points += app.user.phone ? 1 : 0;
                    points += app.user.mobile ? 1 : 0;
                    points += app.user.email ? 1 : 0;
                    points += app.user.address ? 1 : 0;
                    points += app.user.address_number ? 1 : 0;
                    points += app.user.address_complement ? 1 : 0;
                    points += app.user.address_neighborhood ? 1 : 0;
                    points += app.user.address_city ? 1 : 0;
                    points += app.user.address_state ? 1 : 0;
                    points += app.user.address_country ? 1 : 0;
                    points += app.user.postal_code ? 1 : 0;

                    if(points >= 13){
                        document.querySelector('.payme-btn').addEventListener('click', function() {     

                            $('.page-hover').fadeIn();
                            
                            var checkout = new PagarMeCheckout.Checkout({
                                encryption_key: app.encryption_key,
                                success: function(data) {
                                    const  payloadObj = { 
                                        access_token: app.getData('token'),
                                        host: window.location.host,
                                        ...data
                                    }
                            
                                    app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Membership', 'POST', undefined, payloadObj, function(statusCode, responsePayload){
                                        if(statusCode == 200 && responsePayload){
                                            //console.log(responsePayload, 'HEY LOLO')
                                            if(responsePayload.status == 'paid'){
                                                app.setData('assInstMem', true)
                                                document.querySelector('.information-about-member').style.display = 'none';
                                                Swal.fire(
                                                    'Sucesso',
                                                    'O seu pagamento foi processado com sucesso.',
                                                    'success'
                                                  )
                                            } 
                                            
                                            if(responsePayload.type == 'boleto'){
                                                var boleto_url = responsePayload.boleto_url;
                                                var boleto_barcode = responsePayload.boleto_barcode;
                                                document.querySelector('.barcode-copy').innerText = boleto_barcode;
                                                document.querySelector('.pdf-file').href = boleto_url;
                                                $('#applicationModal').modal('show');
                                            }

                                            if(responsePayload.type == 'pix'){
                                                document.getElementById('qrcode').innerHTML = '';
                                                document.querySelector('.pixcode-copy').innerText = '';
                                                $('#qrcode').qrcode(responsePayload.tx.pix_qr_code);
                                                $('#pixModal').modal('show')
                                                document.querySelector('.pixcode-copy').innerText = responsePayload.tx.pix_qr_code;
                                                
                                            }
                                        } 
                                        $('.page-hover').fadeOut();
                                    })
                                },
                                error: function(err) {
                                    //console.log(err);
                                },
                                close: function() {
                                    //console.log('The modal has been closed.');
                                }
                            });             
                            
                
                            const  queryStringObj = { 
                                host: window.location.host,
                                access_token: app.getData('token')
                            }
                    
                            app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Membership', 'GET', queryStringObj, undefined, function(statusCode, responsePayload){
                                if(statusCode == 200 && responsePayload.length){
                                    
                                    app.institution.membership = responsePayload;
                
                                    const aPhoneNumbers = new Array();
                                    aPhoneNumbers.push(`+55${app.user.mobile.replace(/[^0-9]/g, "")}`);
                
                                    const customerData = {
                                        name: app.user.name,
                                        email: app.user.email,
                                        country: "br",
                                        external_id: app.user.email,
                                        documents: [{
                                            type: "cpf",
                                            number: app.user.cpf.replace(/[^0-9]/g, "")
                                        }],
                                        type: "individual",
                                        phone_numbers: aPhoneNumbers
                                    };
                
                                    const billingData = {
                                        name: app.user.name,
                                        address: {
                                            country: "br",
                                            state: app.user.address_state,
                                            city: app.user.address_city,
                                            neighborhood: app.user.address_neighborhood,
                                            complementary: app.user.address_complement,
                                            street: app.user.address,
                                            street_number: app.user.address_number,
                                            zipcode: app.user.postal_code.replace(/\D/gm,""),
                                        }
                                    }
                

                                    var iAmountPrice = 0;
                                    var aItems = [];
                                    var aPixItems = [];

                                    for (let index = 0; index < app.institution.membership.length; index++) {
                                        const oProduct = app.institution.membership[index];
                                        const oProductTemplate = {
                                            id: oProduct.id,
                                            title: oProduct.name,
                                            unit_price: oProduct.price, 
                                            quantity: 1,
                                            tangible: 'false'
                                        }
                                        iAmountPrice += oProduct.price
                                        aItems.push(oProductTemplate)
                                        aPixItems.push({name: oProduct.name, value: `${oProduct.price}`})
                                    }
                                    var date = new Date();
                                    date.setDate(date.getDate() + 1);

                                    var pixExpirationDate = `${date.getFullYear()}-${(`0`+ ( date.getMonth() + 1 ) ).slice(-2)}-${(`0`+date.getDate()).slice(-2)}`;
                                    //console.log(aPixItems)
                                    const  pagarmeTemp = {
                                        amount: iAmountPrice,
                                        customerData: 'false',
                                        createToken: 'true',
                                        paymentMethods: 'credit_card,boleto,pix',
                                        pix_expiration_date: pixExpirationDate,
                                        pix_additional_fields: aPixItems,
                                        boletoDiscountPercentage: 0,
                                        items: aItems,
                                        customer: customerData,
                                        billing: billingData
                                    };
                                    //console.log(pagarmeTemp)
                                    checkout.open(pagarmeTemp)
                                } else {
                                    Swal.fire(
                                        'Importante',
                                        'Você já possui um pedido de pagamento, verifique o seu pagamento no seu extrato.',
                                        'info'
                                        )
                                    app.bindLoadHistory();
                                    $('#myTab a[href="#history"]').tab('show')
                                    $('.page-hover').fadeOut();

                                }
                            })
                            
                        });
                    } else {
                        document.querySelector('.payme-btn').addEventListener('click', function() {
                            Swal.fire(
                                'Importante',
                                'Para realizar o pagamento da sua mensalidade, você primeiro precisa preencher os dados necessários em seu cadastro.',
                                'info'
                              )
                            $('#myTab a[href="#profile"]').tab('show')
                            document.querySelector('.formInfo').style.display = 'block'
                        })
                    }
                }
            })
    
            oQueryString.host = window.location.host;
    
            app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Product','GET',oQueryString,undefined,function(statusCode,responsePayload){
                if(statusCode == 200){
                    var d = new Date(responsePayload.dateend);
                    var dateString = `${d.getUTCDate() < 10 ? '0'+d.getUTCDate() : d.getUTCDate()}/${(d.getUTCMonth() + 1) < 10 ? '0'+(d.getUTCMonth() + 1) : (d.getUTCMonth() + 1) }/${d.getUTCFullYear()}`
                    document.querySelector('#date-string').innerText = dateString;
                }
            })


        },3500)
        
    
        $('[data-toggle="offcanvas"]').on('click', function () {
          $('.offcanvas-collapse').toggleClass('open')
        })
        

        document.querySelector('.history-btn').addEventListener('click', function(){
            $('.page-hover').fadeIn();

            app.bindLoadHistory();
        })

        document.querySelector('.congress-btn').addEventListener('click', function(){
            $('.page-hover').fadeIn();

            app.bindLoadCongresses();
        })
    }

    if(primaryClass == "accountUser"){
        
    }


}

app.toPdf = function(filename) {
    html2canvas(document.getElementById("certificate"), {
      dpi: 300,
      onrendered: function onrendered(canvas) {
        var imgData = canvas.toDataURL('image/png');
        var doc = new jspdf.jsPDF('l', 'mm', "a4");
        doc.addImage(imgData, 'PNG', 0, 0);
        doc.save(filename + '.pdf');
      },
      logging: true,
      allowTaint: true
    });
}

// Page load certificate
app.loadCertificate = function(eventData){

    app.lists.ies = {}

    for (let index = 0; index < eventData.oCongress.iesList.length; index++) {
        const ies = eventData.oCongress.iesList[index];
        app.lists.ies[ies.name] = ies;
    }

    let regions = [], categs = [];

    if(eventData.type == 'avaliatorCert'){
        for (let index = 0; index < eventData.oCustomer.avaliations.length; index++) {
            const oAvaliation = eventData.oCustomer.avaliations[index];
            if(typeof(oAvaliation.work) == 'object' && oAvaliation.work.congressId == eventData.oCongress.id){
                regions.push(app.lists.ies[oAvaliation.work.ies].region);
                categs.push(oAvaliation.work.category)
            }
        }
    }

    if(eventData.type == 'coordinatorCert'){
        for (let index = 0; index < eventData.oCustomer.cordinators.length; index++) {
            const oCordinator = eventData.oCustomer.cordinators[index];
            if(oCordinator.congressId == eventData.oCongress.id){
                categs = oCordinator.categs
            }
        }
    } 

    const targetCert = localStorage.getItem('targetCert');

    regions = regions.unique();
    categs = categs.unique();

    eventData.oCongress[targetCert] = eventData.oCongress[targetCert].replace('{{region}}',regions.join(","))
    eventData.oCongress[targetCert] = eventData.oCongress[targetCert].replace('{{categs}}',categs.join(","))

    var workName = localStorage.getItem('workName');
    
    if(typeof(workName) == 'string'){
        eventData.oCongress[targetCert] = eventData.oCongress[targetCert].replace('{{target}}',workName);
    }
    
    var categoryName = localStorage.getItem('categoryName');
    if(typeof(categoryName) == 'string'){
        eventData.oCongress[targetCert] = eventData.oCongress[targetCert].replace('{{categ}}',categoryName);
    }


    console.log('Carregando Certificado');

    var student = eventData.oCustomer.name;
    var course = eventData.oCongress.name;
    var date = new Date(eventData.oCongress.date);
    document.getElementById('cert-holder').innerText = student;
    document.getElementById('cert-course').innerText = "\"" + course + "\",";
    document.getElementById('cert-details').innerHTML = typeof eventData.oCongress[targetCert] == 'string' ? eventData.oCongress[targetCert] : "";
    document.getElementById('date').innerHTML = (date.getUTCDate() < 10 ? '0' + date.getUTCDate() : date.getUTCDate()) + "/" + (date.getUTCMonth() + 1 < 10 ? '0' + (date.getUTCMonth() + 1) : date.getUTCMonth()) + "/" + date.getUTCFullYear();
    document.getElementById('host').innerText = window.location.host;
    var certificateName = "Certificado-" + course.replaceAll(/ /g, '_') + "-" + student.replaceAll(/ /g, '_');

    function toDataURL(url, callback) {
      var xhr = new XMLHttpRequest();

      xhr.onload = function () {
        var reader = new FileReader();

        reader.onloadend = function () {
          callback(reader.result);
        };

        reader.readAsDataURL(xhr.response);
      };

      xhr.open('GET', url, true);
      xhr.responseType = 'blob';
      xhr.send();
    }

    if (typeof eventData.oCongress.certImageUrl == 'string') {
      toDataURL(eventData.oCongress.certImageUrl, function (dataUrl) {
        var img = document.getElementById('backgroundImage');
        img.src = dataUrl;
      });
    }

    var certBtn = document.getElementById("cert-btn");
    certBtn.setAttribute("onclick", "app.toPdf(\"" + certificateName + "\")"); //cert-detail
    
}
  
app.verifyMember = function(){
    var assInstMem = app.getData('assInstMem');
    
    if(!assInstMem){
        const  queryStringObj = { 
            host: window.location.host,
            access_token: app.getData('token')
        }
        app.client.request(undefined,endpoint+'/api/Authenticateds/Institution/Member', 'GET', queryStringObj, undefined, function(statusCode,responsePayload){
            if(statusCode == 200){
                if(responsePayload.active){
                    var style = document.createElement('style');
                    style.type = 'text/css';
                    style.innerHTML = '.information-about-member { display: none; }';
                    document.getElementsByTagName('head')[0].appendChild(style);
                    
                    app.setData('membership', JSON.stringify(responsePayload))
                    app.setData('assInstMem', responsePayload.active)
                    app.user.membership = responsePayload; 
                }
            } else {
                app.setData('membership', false);
                app.setData('assInstMem', false);
            }
        })
    } else if(assInstMem == 'true'){
        var style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = '.information-about-member { display: none; }';
        document.getElementsByTagName('head')[0].appendChild(style);
    }

}

// Bind the forms
app.bindForms = function(){
   
    if(document.querySelector("form")){
      var allForms = document.querySelectorAll("form");
      
      for(var i = 0; i < allForms.length; i++){
       
        allForms[i].addEventListener("submit", function(e){
  
          // Stop it from submitting
          e.preventDefault();
          var formId = this.id;
          var path = this.action;
          var method = this.method.toUpperCase();
  
          // // Hide the error message (if it's currently shown due to a previous error)
          document.querySelector("#"+formId+" .formError").style.display = 'none';
  
          
          // Set the host always into forms for admin recognize from where its the request
          document.querySelector("#"+formId+" #eventHost").value = window.location.host;
            
          // Hide the success message (if it's currently shown due to a previous error)
          if(document.querySelector("#"+formId+" .formSuccess")){
            document.querySelector("#"+formId+" .formSuccess").style.display = 'none';
          }
  
          if(formId == 'inscriptionWorkAlumn'){
            if(typeof(app.ckeditor.resumeWork) == 'object'){
                document.getElementById('resumeWork').innerText = app.ckeditor.resumeWork.getData();
            }
            if(typeof(app.ckeditor.studyObject) == 'object'){
                document.getElementById('studyObject').innerText = app.ckeditor.studyObject.getData();
            }
            if(typeof(app.ckeditor.researchRealized) == 'object'){
                document.getElementById('researchRealized').innerText = app.ckeditor.researchRealized.getData();
            }
            if(typeof(app.ckeditor.productionDescription) == 'object'){
                document.getElementById('productionDescription').innerText = app.ckeditor.productionDescription.getData();
            }
            
          }

          if(typeof(app.ckeditor.descriptionAvaliationWorkAvaliator) == 'object' && document.getElementById('descriptionAvaliationWorkAvaliator')){
            document.getElementById('descriptionAvaliationWorkAvaliator').innerText = app.ckeditor.descriptionAvaliationWorkAvaliator.getData();
          }

          var valueText = '';

          if(typeof(app.ckeditor.bookResume) == 'object' && document.getElementById('bookResume')){
            document.getElementById('bookResume').innerText = app.ckeditor.bookResume.getData();
          }

          if(document.querySelector('.button-imgload-submit') && document.querySelector('.button-imgload-submit-loader')){
            document.querySelector('.button-imgload-submit').style.display = 'none';
            document.querySelector('.button-imgload-submit-loader').style.display = 'block';
          }
  
          // Turn the inputs into a payload
          var leload = {};
          var payload = {};
          var elements = this.elements;
          for(var i = 0; i < elements.length; i++){
            if(elements[i].type !== 'submit'){
              // Determine class of element and set value accordingly
              var classOfElement = typeof(elements[i].classList.value) == 'string' && elements[i].classList.value.length > 0 ? elements[i].classList.value : '';
              var valueOfElement = elements[i].type == 'checkbox' && classOfElement.indexOf('multiselect') == -1 ? elements[i].checked : classOfElement.indexOf('intval') == -1 ? elements[i].value : parseInt(elements[i].value);
              var elementIsChecked = elements[i].checked;
              // Override the method of the form if the input's name is _method
              var nameOfElement = elements[i].name;
              if(nameOfElement == '_method'){
                method = valueOfElement;
              } else {
                // Create an payload field named "method" if the elements name is actually httpmethod
                if(nameOfElement == 'httpmethod'){
                  nameOfElement = 'method';
                }
                // Create an payload field named "id" if the elements name is actually uid
                if(nameOfElement == 'uid'){
                  nameOfElement = 'id';
                }
                // If the element has the class "multiselect" add its value(s) as array elements
                if(classOfElement.indexOf('multiselect') > -1){
                  if(elementIsChecked){
                    payload[nameOfElement] = typeof(payload[nameOfElement]) == 'object' && payload[nameOfElement] instanceof Array ? payload[nameOfElement] : [];
                    payload[nameOfElement].push(valueOfElement);
                  }
                } else if(classOfElement.indexOf('form-check-input') > -1){
                    if(elementIsChecked){
                        payload[nameOfElement] = valueOfElement;
                    }
                } else {
                  payload[nameOfElement] = valueOfElement;
                }

                
              }
            }
          }
  
          // If the method is DELETE, the payload should be a queryStringObject instead
          var queryStringObject = method == 'DELETE' ? payload : {};
          if(app.user){
            app.user.token = app.getData('token');
            if(app.user.token)
            {
                queryStringObject['access_token'] = app.user.token;
            }
          }

          if(typeof(app.ckeditor.descriptionAvaliationWorkCordinator) == 'object' && document.getElementById('descriptionAvaliationWorkCordinator')){
            valueText = app.ckeditor.descriptionAvaliationWorkCordinator.getData();
            document.getElementById('descriptionAvaliationWorkCordinator').innerText = valueText;
            payload["descriptionAvaliationWorkCordinator"] = valueText;
          }
         
          // Call the API
          app.client.request(undefined,path,method,queryStringObject,payload,function(statusCode,responsePayload){
            // Display an error on the form if needed
            if(statusCode !== 200){
               app.formResponseErrorProcessor(formId, payload, statusCode, responsePayload)
            } else {
              // If successful, send to form response processor
              app.formResponseProcessor(formId,payload,responsePayload);
            }
  
            if(document.querySelector('.button-imgload-submit') && document.querySelector('.button-imgload-submit-loader')){
                document.querySelector('.button-imgload-submit').style.display = 'block';
                document.querySelector('.button-imgload-submit-loader').style.display = 'none';
            }

          });
        });
      }
    }
};

app.logUserOut = function(){
    window.localStorage.clear();
    window.location.href = '/';
}

app.formResponseErrorProcessor = function(formId, payload, statusCode, responsePayload){

    switch(statusCode){
        case 401:
            //console.log(responsePayload)
            // Try to get the error from the api, or set a default error message
            var error = typeof(responsePayload.Error) == 'string' ? responsePayload.Error : '<i class="fad fa-exclamation-triangle"></i> Ops essa IES já possui uma indicação para essa categoria.';

            // Set the formError field with the error text
            document.querySelector("#"+formId+" .formError").innerHTML = error;

            // Show (unhide) the form error field on the form
            document.querySelector("#"+formId+" .formError").style.display = 'block';
            // document.querySelector(`#${formId} .formError`).classList.remove('alert-warning')
            document.querySelector(`#${formId} .formError`).classList.add('alert-danger')

            $(`#${formId}Modal`).animate({ scrollTop: 0 }, "slow");
            break;

        case 403:
            app.logUserOut();
            break;

        case 405:
            //console.log(responsePayload)
            // Try to get the error from the api, or set a default error message
            var error = typeof(responsePayload.Error) == 'string' ? responsePayload.Error : '<i class="fad fa-exclamation-triangle"></i> Ops essa trabalho já foi finalizado';

            // Set the formError field with the error text
            document.querySelector("#"+formId+" .formError").innerHTML = error;

            // Show (unhide) the form error field on the form
            document.querySelector("#"+formId+" .formError").style.display = 'block';
            // document.querySelector(`#${formId} .formError`).classList.remove('alert-warning')
            document.querySelector(`#${formId} .formError`).classList.add('alert-danger')

            $(`#${formId}Modal`).animate({ scrollTop: 0 }, "slow");
            break;

        default:
            // Try to get the error from the api, or set a default error message
            var error = typeof(responsePayload.Error) == 'string' ? responsePayload.Error : '<i class="fad fa-info-circle"></i> Houve um erro, por favor tente novamente, caso o erro persista entre em contato com o suporte técnigo: contato@uhub.team.';

            // Set the formError field with the error text
            document.querySelector("#"+formId+" .formError").innerHTML = error;

            // Show (unhide) the form error field on the form
            document.querySelector("#"+formId+" .formError").style.display = 'block';
            break;
    }
}

app.formResponseProcessor = function(formId,payload,responsePayload){
    document.querySelector("#"+formId+" .formSuccess").style.display = 'block';
    if(document.querySelector('.formInfo')){
        document.querySelector('.formInfo').style.display = 'none'
    }
    
    if(formId == 'updateDataInformation'){
        window.scrollTo(0,0)
        document.location = '/pages/account-home.html?upd=data';
    }

    if(formId == 'institutionCongressWork'){
        document.querySelectorAll("#"+formId+" input").forEach(e => {
            //console.log(e)
            // e.disabled = true;
        });
        // document.getElementById(formId).reset()
    }

    if(formId == 'inscriptionWorkAlumn'){
        window.scrollTo(0,0);
        Swal.fire('Uhuu','O seu trabalho foi submetido com sucesso. Agora é só esperar a revisão e avaliação.','success')
            .then(e => {
                window.location.reload();
            })
    }

    if(formId == 'workAvaliationAvaliator'){
        Swal.fire('Uhuu','A sua avaliação foi submetida com sucesso.','success')
        .then(e => {
            $('.page-hover').fadeIn();
            $('.page-hover').css('zIndex',9999)
            $(`#${formId}Modal`).modal('hide');
            var workId = $(`#${formId} #congressIdWorkAvaliator`).val();
            app.bindLoadCongresses();
            setTimeout(function(){
                app.loadWorksForAvaliation(workId);
                $('.page-hover').css('zIndex',2)
            },2500)
        })
    }
    
    if(formId == 'workCordinationCordinator'){
        Swal.fire('Uhuu','A sua avaliação foi concluida com sucesso.','success')
        .then(e => {
            $('.page-hover').fadeIn();
            $(`#${formId}Modal`).modal('hide');
            $('.page-hover').css('zIndex',9999)
            var workId = $(`#${formId} #congressIdWorkCordinator`).val();
            app.bindLoadCongresses();
            setTimeout(function(){
                app.loadWorksForCordination(workId)
                $('.page-hover').css('zIndex',2)
            },2500)
        })   
    }

    if(formId == 'bookSubmition'){
        Swal.fire('Uhuu','A sua proposta de lançamento de livro foi concluida com sucesso.','success')
        .then(e => {
            $(`#${formId}Modal`).modal('hide');
            document.getElementById(formId).reset()
            window.location.reload();
        })   
    }
    
}

app.checkFrontTemplate = function(){
    var appLogo = app.getData('applicationLogo');
    var appName = app.getData('applicationName');
    var contactMail = app.getData('contactMail');
    var contactPhone = app.getData('contactPhone');
    if(appLogo && appName){
        if(document.querySelector('.client-name')){
            document.querySelector('.client-logo').src = appLogo;
        }
        if(document.querySelector('.client-name')){
            document.querySelector('.client-name').innerText = appName;
        }
        if(document.querySelector('.logo-thumbnail')){
            document.querySelector('.logo-thumbnail').src = appLogo;
        }
        if(document.querySelector('.contactMail')){
            document.querySelector('.contactMail').innerText = contactMail;
        }
        if(document.querySelector('.contactPhone')){
            document.querySelector('.contactPhone').innerText = contactPhone;
        }
    } else {
        const  queryStringObj = { 
            host: window.location.host
        }
        app.client.request(undefined,endpoint+'/api/Publics/Institution', 'GET', queryStringObj, undefined, function(statusCode,responsePayload){
            if(statusCode == 200){
                appLogo = responsePayload.logo;
                appName = responsePayload.name;
                contactMail = responsePayload.contact_email;
                contactPhone = responsePayload.contact_phone;
               
                app.setData('applicationLogo', appLogo);
                app.setData('applicationName', appName);
                app.setData('contactMail' ,contactMail);
                app.setData('contactPhone', contactPhone);
               
                if(document.querySelector('.client-name')){
                    document.querySelector('.client-logo').src = appLogo;
                }
                if(document.querySelector('.client-name')){
                    document.querySelector('.client-name').innerText = appName;
                }
                if(document.querySelector('.logo-thumbnail')){
                    document.querySelector('.logo-thumbnail').src = appLogo;
                }
                if(document.querySelector('.contactMail')){
                    document.querySelector('.contactMail').innerText = contactMail;
                }
                if(document.querySelector('.contactPhone')){
                    document.querySelector('.contactPhone').innerText = contactPhone;
                }
            } 
        })
    }

}

// Authentication, Validatation and core sutff.
app.validateFrbAuth = function(){
    var displayName = app.user.displayName;
    var email = app.user.email;
    var emailVerified = app.user.emailVerified;
    var photoURL = app.user.photoURL;
    var uid = app.user.uid;
    var phoneNumber = app.user.phoneNumber;
    var providerData = app.user.providerData;
    app.user.getIdToken().then(function(accessToken) {
        const requestOb = { 
            name: displayName,
            email: email,
            emailVerified: emailVerified,
            phoneNumber: phoneNumber,
            picture: photoURL,
            uid: uid,
            accessToken: accessToken,
            front: window.location.host
        }
        app.client.request(undefined,endpoint+'/api/Authenticateds/authenticate','POST',undefined,requestOb,function(statusCode,responsePayload){
            if(statusCode == 200){
                app.setData('token',responsePayload.id);
                app.setData('uid',responsePayload.userId);
            } else {
                window.location = '/?rsn=not-autorized';
            }
        })
    });
}

app.checkAuthentication = function(){
    var isAuthenticated = app.getData('token');
    if(!isAuthenticated)
        app.validateFrbAuth();
    return true;
}

app.loadConfiguration = function(){
    // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    var firebaseConfig = {
        apiKey: "AIzaSyCCjMIuaOfDNygqU1rGOOyA8h8mST07taE",
        authDomain: "uhubclub.firebaseapp.com",
        databaseURL: "https://uhubclub-default-rtdb.firebaseio.com",
        projectId: "uhubclub",
        storageBucket: "uhubclub.appspot.com",
        messagingSenderId: "1090303585021",
        appId: "1:1090303585021:web:a44056d54ce3ce099dac94",
        measurementId: "G-0500E63LTN"
    };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    // Set analytics to collect data from application
    firebase.analytics();
}

app.initTranslate = function() {
    setTimeout(function() {
        var dl = 'pt-BR';
        var bl = navigator.language;
        var google = 'Google';
        var facebook = 'Facebook';
        var email = 'password';
        bl === 'en-US' || bl === 'en-us' ? runlang(bl) : runlang(dl);

        function translate(text, provider) {
            var container = $('.firebaseui-idp-' + provider.toLowerCase() + ' .firebaseui-idp-text-long');
            if(provider === 'password'){
                provider = 'email';
                container.text(text+' '+provider);
            } else{
                container.text(text+' '+provider);
            }
        }

        function translate2(html, provider) {
            var container = $('.firebaseui-card-footer.firebaseui-provider-sign-in-footer');
            container.html(html)
        }

        function runlang(lang) {
            //console.log(lang)
            if (lang === 'en-US' || lang === 'en-us') {
                var sign_in = 'Sign in with';
                translate( sign_in, google );
                translate( sign_in, facebook );
                translate( sign_in, email );
            } else {
                var sign_in = 'Entrar com';
                translate( sign_in, google );
                translate( sign_in, facebook );
                translate( sign_in, email );
                translate2( `<p class="firebaseui-tos firebaseui-tospp-full-message">Ao continuar, você indica que aceita nossos <a href="/legal/terms-of-use.html" class="firebaseui-link firebaseui-tos-link" target="_blank">Termos de Serviço</a> e <a href="/legal/privacy-policy.html" class="firebaseui-link firebaseui-pp-link" target="_blank">Política de Privacidade</a></p>`,'.firebaseui-provider-sign-in-footer')
            }
        }
    }, 1500); 
}

app.startAuthentication = function(){

     // FirebaseUI config.
     var uiConfig = {
        signInSuccessUrl: 'pages/account-home.html',
        signInOptions: [
            // Leave the lines as is for the providers you want to offer your users.
            firebase.auth.GoogleAuthProvider.PROVIDER_ID,
            firebase.auth.EmailAuthProvider.PROVIDER_ID
        ],
        // tosUrl and privacyPolicyUrl accept either url string or a callback
        // function.
        // Terms of service url/callback.
        tosUrl: 'legal/terms-of-use.html',
        // Privacy policy url/callback.
        privacyPolicyUrl: function() {
        window.location.assign('legal/privacy-policy.html');
        }
    };

    // Initialize the FirebaseUI Widget using Firebase.
    var ui = new firebaseui.auth.AuthUI(firebase.auth());
    // The start method will wait until the DOM is loaded.
    ui.start('#firebaseui-auth-container', uiConfig);
    app.initTranslate();
}

app.checkIsLoggedIn = function(){
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            app.user = {};
            app.user = user;
            app.checkFrontTemplate();
            app.checkAuthentication();
            app.loadByClass();
            setTimeout(function(){
                app.verifyMember();
            },2500)
        } else {
            localStorage.removeItem('token')
            localStorage.removeItem('assInstMem')
            window.location = '/';
        }
    }, function(error) {
        //console.log(error);
    });
}

app.loadPageOnData = function(){

    
    // Get the current page from the body class
    var bodyClasses = document.querySelector("body").classList;
    var primaryClass = typeof(bodyClasses[0]) == 'string' ? bodyClasses[0] : false;

    if(primaryClass == 'index'){

        
        localStorage.removeItem('token');
        localStorage.removeItem('assInstMem');
        localStorage.removeItem('applicationLogo');
        localStorage.removeItem('applicationName');
        localStorage.removeItem('contactMail');
        localStorage.removeItem('contactPhone');


        if(typeof(app.user) == 'object' && typeof(app.user.token) == 'string' && app.user.token.length > 0){
            delete app.user.token;
        }

        const oQueryString = {
            host: window.location.host
        }

        app.startAuthentication();
        setTimeout(function(){
            app.checkFrontTemplate()
        },300)
    }

    if(primaryClass != 'index' && primaryClass != 'institutionCongress' && primaryClass != 'institutionCongressIndication' && primaryClass != 'eventCertificate'){
        app.checkIsLoggedIn();
    }

   
    if(primaryClass == 'eventCertificate'){

        const oQueryString = {
            access_token: localStorage.getItem('token'),
            congressId: localStorage.getItem('congressCert'),
            type: localStorage.getItem('targetCert')
        }
      
        app.client.request(undefined,endpoint+'/api/Authenticateds/Certificate','GET',oQueryString,undefined,function(statusCode,responsePayload){
            if(statusCode == 200 && responsePayload){
                app.loadCertificate(responsePayload);
                $('#setPreference').fadeOut(1000);
            }
        })
    }

    if(primaryClass == 'institutionCongresses'){
        const  queryStringObj = { 
            host: window.location.host
        }
        app.client.request(undefined,endpoint+'/api/Publics/Institution', 'GET', queryStringObj, undefined, function(statusCode,responsePayload){
            if(statusCode == 200 && responsePayload){
                var oIns = responsePayload;
                document.getElementById('application-name').innerHTML = `
                    <img src="${responsePayload.logomark}" height="50px"/>
                `
                if(typeof(oIns.congresses) == 'object' && oIns.congresses instanceof Array && oIns.congresses.length > 0){
                    oIns.congresses.forEach( (oCongress,key) => {
                        var text = oCongress.resume;
                        var regex = /(<([^>]+)>)/ig;
                        text = text.replace(regex,"");
                        if(key == 0){
                            document.querySelector('.home-bg-image').style.background = `url(${oCongress.topImageUrl}) center center / cover no-repeat`;
                            document.querySelector('.home-title').innerText = oCongress.name;
                            document.querySelector('.home-resume-short').innerText = `${text.slice(0,240)}...`;
                            // document.querySelector('.home-link-more').src = `/event.html?congressNode=${oCongress.id}`;
                            document.getElementById('home-link-more').href = `/event.html?congressNode=${oCongress.id}`;

                        } else {
                            $('.congress-collection').append(`
                                <div class="col-md-6">
                                    <div class="card flex-md-row mb-4 shadow-sm h-md-250">
                                        <div class="card-body d-flex flex-column align-items-start">
                                            <strong class="d-inline-block mb-2 text-primary">Evento</strong>
                                            <h3 class="mb-0">
                                                <a class="text-dark" href="/event.html?congressNode=${oCongress.id}">${oCongress.name}</a>
                                            </h3>
                                            <div class="mb-1 text-muted">${new Date(oCongress.date).toLocaleString()}</div>
                                            <p class="card-text mb-auto">${text.slice(0,37)}...</p>
                                            <a href="/event.html?congressNode=${oCongress.id}"><i class="fad fa-angle-double-right"></i> Ver evento</a>
                                        </div>
                                        <div class="card-img-right flex-auto d-none d-lg-block" style="width: 200px; height: 250px; background: url(${oCongress.topImageUrl}) center center / cover no-repeat" alt="Card image cap"></div>
                                    </div>
                                </div>
                            `)
                            //console.log(oCongress, regex, key)
                        }
                    })
                } else {
                    //console.log(
                        // 'ZERAR TUDO'
                    // )
                }
            } 
        })
    }

    if(primaryClass == 'institutionCongress'){
        var congressNode = app.getFromQuery('congressNode');
        
        const  queryStringObj = { 
            host: window.location.host
        }
        app.client.request(undefined,endpoint+'/api/Publics/Institution', 'GET', queryStringObj, undefined, function(statusCode,responsePayload){
            if(statusCode == 200 && responsePayload){
                var oIns = responsePayload;

                document.querySelector('.phone').innerText = oIns.contact_phone;

                document.querySelector('.email').innerText = oIns.contact_email;

                document.getElementById('application-name').innerHTML = `
                    <img src="${responsePayload.logomark}" height="50px"/>
                `

                let count = 0;
                if(typeof(oIns.congresses) == 'object' && oIns.congresses instanceof Array && oIns.congresses.length > 0){
                    oIns.congresses.forEach( (oCongress,key) => {
                        var text = oCongress.resume;
                        var regex = /(<([^>]+)>)/ig;
                        text = text.replace(regex,"");
                        if(oCongress.id == congressNode){
                            document.querySelector('.home-bg-image').style.background = `url(${oCongress.topImageUrl}) center center / cover no-repeat`;
                            document.querySelector('.home-title').innerText = oCongress.name;
                            document.querySelector('.home-resume-long').innerHTML = oCongress.resume;
                        } else {
                            count++;
                            $('.event-calendar').append(`
                                <li>
                                    <div class="row">
                                        <div class="card flex-md-row mb-4 shadow-sm h-md-250">
                                            <div class="card-body d-flex flex-column align-items-start">
                                                <strong class="d-inline-block mb-2 text-primary">Evento</strong>
                                                <h3 class="mb-0">
                                                    <a class="text-dark" href="/event.html?congressNode=${oCongress.id}">${oCongress.name}</a>
                                                </h3>
                                                <div class="mb-1 text-muted">${new Date(oCongress.date).toLocaleString()}</div>
                                                <p class="card-text mb-auto">${text.slice(0,37)}...</p>
                                                <a href="/event.html?congressNode=${oCongress.id}"><i class="fad fa-angle-double-right"></i> Ver evento</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            `)
                            //console.log(oCongress, regex, key)
                        }
                    })
                } else {
                    //console.log(
                    //     'ZERAR TUDO'
                    // )
                }

                if(count == 0){
                    document.querySelector('.event-calendar').innerHTML = `<li><i>Esta organização não possui outros eventos agendados</i></li>`
                }
            } 
        })
    }

    if(primaryClass == 'institutionCongressIndication'){
        var congressNode = app.getFromQuery('node');
        
        const  queryStringObj = { 
            host: window.location.host
        }
        app.client.request(undefined,endpoint+'/api/Publics/Institution', 'GET', queryStringObj, undefined, function(statusCode,responsePayload){
            if(statusCode == 200 && responsePayload){
                var oIns = responsePayload;

                document.querySelector('.phone').innerText = oIns.contact_phone;

                document.querySelector('.email').innerText = oIns.contact_email;

                document.getElementById('application-name').innerHTML = `
                    <img src="${responsePayload.logomark}" height="50px"/>
                `
                if(typeof(oIns.congresses) == 'object' && oIns.congresses instanceof Array && oIns.congresses.length > 0){
                    oIns.congresses.forEach( (oCongress,key) => {
                        var text = oCongress.resume;
                        var regex = /(<([^>]+)>)/ig;
                        text = text.replace(regex,"");
                        if(oCongress.id == congressNode){
                            //console.log(oCongress)

                            if(typeof(oCongress.categories) == 'object' && oCongress.categories instanceof Array && oCongress.categories.length > 0){
                                var categorySelect = document.getElementById('category');
                                for (let index = 0; index < oCongress.categories.length; index++) {
                                    const oCategory = oCongress.categories[index];
                                    categorySelect.options[categorySelect.options.length] = new Option(oCategory, oCategory);
                                }
                            }
                
                            if(typeof(oCongress.iesList) == 'object' && oCongress.iesList instanceof Array && oCongress.iesList.length > 0){
                                var ieSelect = document.getElementById('ies');
                                for (let index = 0; index < oCongress.iesList.length; index++) {
                                    const ies = oCongress.iesList[index];
                                    ieSelect.options[ieSelect.options.length] = new Option(ies.name, ies.name);
                                }
                            }


                            document.querySelector('.home-bg-image').style.background = `url(${oCongress.topImageUrl}) center center / cover no-repeat`;
                            document.querySelector('.home-title').innerText = oCongress.name;
                            document.querySelector('#eventHost').value = window.location.host;
                            document.getElementById('congressId').value = oCongress.id;
                            var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
                            var mobile = document.querySelector('#mobile');
                            VMasker(mobile).maskPattern(telMask[0]);
                            mobile.addEventListener('input', app.inputHandler.bind(undefined, telMask, 14), false);
                            var docMask = ['999.999.999-99'];
                            var doc = document.querySelector('#cpf');
                            VMasker(doc).maskPattern(docMask[0]);
                            doc.addEventListener('input', app.inputHandler.bind(undefined, docMask, 14), false)
                            var docLeader = document.querySelector('#leaderDoc');
                            VMasker(docLeader).maskPattern(docMask[0]);
                            docLeader.addEventListener('input', app.inputHandler.bind(undefined, docMask, 14), false)
                        } 
                    })
                } else {
                    //console.log(
                    //     'ZERAR TUDO'
                    // )
                }
            } 
        })
    }

}

// AJAX Client (for RESTful API)
app.client = {};

// Interface for making API calls
app.client.request = function(headers,path,method,queryStringObject,payload,callback){

    // Set defaults
    headers = typeof(headers) == 'object' && headers !== null ? headers : {};
    path = typeof(path) == 'string' ? path : '/';
    method = typeof(method) == 'string' && ['POST','GET','PUT','DELETE'].indexOf(method.toUpperCase()) > -1 ? method.toUpperCase() : 'GET';
    queryStringObject = typeof(queryStringObject) == 'object' && queryStringObject !== null ? queryStringObject : {};
    payload = typeof(payload) == 'object' && payload !== null ? payload : {};
    callback = typeof(callback) == 'function' ? callback : false;
  
    // For each query string parameter sent, add it to the path
    var requestUrl = path+'?';
    var counter = 0;
    for(var queryKey in queryStringObject){
       if(queryStringObject.hasOwnProperty(queryKey)){
         counter++;
         // If at least one query string parameter has already been added, preprend new ones with an ampersand
         if(counter > 1){
           requestUrl+='&';
         }
         // Add the key and value
         requestUrl+=queryKey+'='+queryStringObject[queryKey];
       }
    }
  
    // Form the http request as a JSON type
    var xhr = new XMLHttpRequest();
    xhr.open(method, requestUrl, true);
    xhr.setRequestHeader("Content-type", "application/json");
  
    // For each header sent, add it to the request
    for(var headerKey in headers){
       if(headers.hasOwnProperty(headerKey)){
         xhr.setRequestHeader(headerKey, headers[headerKey]);
       }
    }
  
    // If there is a current session token set, add that as a header
    if(app.session.token){
      xhr.setRequestHeader("acces_token", app.session.token);
    }
  
    // When the request comes back, handle the response
    xhr.onreadystatechange = function() {
        if(xhr.readyState == XMLHttpRequest.DONE) {
          var statusCode = xhr.status;
          var responseReturned = xhr.responseText;
  
          // Callback if requested
          if(callback){
            try{
              var parsedResponse = JSON.parse(responseReturned);
              callback(statusCode,parsedResponse);
            } catch(e){
              callback(statusCode,false);
            }
  
          }
        }
    }
  
    // Send the payload as JSON
    var payloadString = JSON.stringify(payload);
    xhr.send(payloadString);
  
};

app.start = function(){
    app.loadConfiguration();
    app.loadPageOnData();
    // Bind all form submissions
    app.bindForms()
}

app.start()

// Helpers

app.setData = function(key, value){
    localStorage.setItem(key, value);
    return true;
}

app.getData = function(key){
    return localStorage.getItem(key);
}

app.eventFire = function(el, etype){
    if (el.fireEvent) {
      el.fireEvent('on' + etype);
    } else {
      var evObj = document.createEvent('Events');
      evObj.initEvent(etype, true, false);
      el.dispatchEvent(evObj);
    }
}

app.inputHandler = function(masks, max, event) {
	var c = event.target;
	var v = c.value.replace(/\D/g, '');
	var m = c.value.length > max ? 1 : 0;
	VMasker(c).unMask();
	VMasker(c).maskPattern(masks[m]);
	c.value = VMasker.toPattern(v, masks[m]);
}

app.loadPixPayment = function(data){
    document.getElementById('qrcode').innerHTML = '';
    document.querySelector('.pixcode-copy').innerText = '';
    $('#qrcode').qrcode(data);
    $('#pixModal').modal('show')
    document.querySelector('.pixcode-copy').innerText = data;
}